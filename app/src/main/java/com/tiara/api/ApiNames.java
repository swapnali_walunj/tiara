package com.tiara.api;

/**
 * Created by itrs-203 on 12/22/17.
 */

public class ApiNames {

    /**
     * Login
     */
    public static final String SALES_LOGIN = "LoginService/SalesmanLogin";
    public static final String SALESMAN="Salesman";
    public static final String CUSTOMER_LOGIN = "LoginService/CustomerLogin";
    public static final String CUSTOMER="Customer";
    public static final String LIVE_STOCK="LiveStock/GetDetails";
    public static final String PROFILE="CustomerProfile/Profile";
    public static final String LABOUR_CHARGE="Labourcharge/GetDetails";
    public static final String FILLGST="CustomerProfile/FillGSTState";
    public static final String PROFILE_UPDATE="CustomerProfile/Update";
    public static final String LIVE_STOCK_DESIGN="LiveStock/GetDetailsByID";
    public static final String OLD_STOCK="OldStock/GetDetails";
    public static final String GET_COLOR_DETAILS="LiveStock/GetColorDetails";
    public static final String GET_SIZE_DETAILS="LiveStock/GetSizeDetails ";
    public static final String GET_MORE_DETAILS="LiveStock/ViewMoreDetails";
    public static final String GET_SEARCH_MORE_DETAILS="LiveStock/SearchColourSizeWise";
    public static final String GET_MORE_DETAILS_CATALOG="OldStock/ViewMoreDesign";
    public static final String CATALOG_DESIGN="OldStock/GetDetailsByID";
    public static final String LIVE_STOCK_ORDER="LiveStock/order";
    public static final String OLD_STOCK_ORDER="OldStock/order";
    public static final String LIVESTOCK_FILLRATE="LiveStock/FillRate";
    public static final String LIVESTOCK_FILLALLCUSTOMER="LiveStock/FillAllCustomer";
    public static final String LIVESTOCK_FILLADDRES= "LiveStock/FillAddress";
    public static final String LIVESTOCK_FILLSALESWISECUSTOMER="LiveStock/FillSalesWiseCustomer";
    public static final String OLDSTOCK_FILLSALESWISECUSTOMER="OldStock/FillCustomer";
    public static final String LIVESTOCK_SAVE="LiveStock/Save";
    public static final String LIVESTOCK_SAVEALL="LiveStock/Saveallvalues";
    public static final String OLDSTOCK_FILLRATE="OldStock/FillRate";
    public static final String OLDSTOCK_FILLADDRES= "OldStock/FillAddress";
    public static final String OLDSTOCK_SAVE="OldStock/Save";
    public static final String OLDSTOCK_SAVEALL="OldStock/Saveallvalues";
    public static final String LABOURCHARGE_OTP="Labourcharge/SendSMS";
    public static final String DELETE_ORDER="PendingOrder/DeleteById";



    public static final String PENDING_ORDER="PendingOrder/Customer";
    public static final String PENDING_ORDER_DETAILS="PendingOrder/ViewById";
    public static final String OLD_PENDING_ORDER="OldPendingorder/Customer";
    public static final String OLD_PENDING_ORDER_DETAILS="OldPendingorder/ViewById";
    public static final String APPROVED_ORDER="Approvedorder/Customer";
    public static final String APPROVED_ORDER_DETAILS="Approvedorder/ViewByID";
    public static final String CHANGE_PASSWORD="ChangePassword/Customer";

    public static final String LIVE_STOCK_SEARCH="LiveStock/SearchCategory";
    public static final String MY_CUSTOMER="MyCustomer/FillGrid";

    public static final String PENDING_ORDER_SALESMAN="PendingOrder/Salesman";
    public static final String OLD_PENDING_ORDER_SALESMAN="OldPendingorder/Salesman";
    public static final String APPROVED_ORDER_SALESMAN="Approvedorder/Salesman";















}
