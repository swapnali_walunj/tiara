package com.tiara.api;


import com.tiara.ui.model.Response.AddressResponse;
import com.tiara.ui.model.Response.ApprovedOrderDetails;
import com.tiara.ui.model.Response.ApprovedOrderResponse;
import com.tiara.ui.model.Response.ColorResponse;
import com.tiara.ui.model.Response.CommonErrorResponse;
import com.tiara.ui.model.Response.CustomerListResponse;
import com.tiara.ui.model.Response.CustomerLoginRes;
import com.tiara.ui.model.Response.FILLGSTRes;
import com.tiara.ui.model.Response.LabourChargeOTPResponse;
import com.tiara.ui.model.Response.LabourChargeResponse;
import com.tiara.ui.model.Response.LiveStockDesignRes;
import com.tiara.ui.model.Response.LiveStockOrderResponse;
import com.tiara.ui.model.Response.LiveStockRes;
import com.tiara.ui.model.Response.LiveStockSaveAllResponse;
import com.tiara.ui.model.Response.LiveStockSaveResponse;
import com.tiara.ui.model.Response.MyCustomerResponse;
import com.tiara.ui.model.Response.OldPendingOrderDetails;
import com.tiara.ui.model.Response.OldPendingOrderResponse;
import com.tiara.ui.model.Response.OldStockOrderResponse;
import com.tiara.ui.model.Response.PendingOrderDetails;
import com.tiara.ui.model.Response.PendingOrderResponse;
import com.tiara.ui.model.Response.ProfileResponse;
import com.tiara.ui.model.Response.RateResponse;
import com.tiara.ui.model.Response.SalesManLoginRes;
import com.tiara.ui.model.Response.SizeResponse;
import com.tiara.ui.model.Response.ViewMoreDesignResponse;
import com.tiara.utils.Constants;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Completable;
import rx.Observable;

/**
 * Created by swapnali on 20/10/18.
 */

public interface NetworkService {


    @GET(ApiNames.SALES_LOGIN + "/{username}/" +  "{password}/" +ApiNames.SALESMAN)
    Observable<SalesManLoginRes> getSalesmanLogin(@Path("username") String Username, @Path("password") String Password);

    @GET(ApiNames.CUSTOMER_LOGIN + "/{username}/" +  "{password}/" +ApiNames.CUSTOMER)
    Observable<CustomerLoginRes> getCustomerLogin(@Path("username") String Username, @Path("password") String Password);

    @GET(ApiNames.LIVE_STOCK /*+ "/{id}/"*/ +  "/{offset}" )
    Observable<LiveStockRes> getLiveStock(/*@Path("id") int id,*/ @Path("offset") int offset);

    @GET(ApiNames.OLD_STOCK /*+ "/{id}/"*/ +  "/{offset}" )
    Observable<LiveStockRes> getoldStock(/*@Path("id") int id,*/ @Path("offset") int offset);

    @GET(ApiNames.FILLGST)
    Observable<FILLGSTRes> getGst();



    @GET(ApiNames.PROFILE + "/{id}")
    Observable<ProfileResponse> getProfileDetails(@Path("id") int id);
    @GET(ApiNames.LABOUR_CHARGE + "/{id}/" +  "{offset}" )
    Observable<LabourChargeResponse> getLabourCharge(@Path("id") int id, @Path("offset") int offset);

    @GET(ApiNames.PROFILE_UPDATE + "/{Cust_Name}/" +  "{Cust_Email}/" +  "{Cust_ContactNo}/"+  "{Cust_ID}/"+  "{Cust_ContPerson}/"+ "{Cust_Mobile}/"+  "{Cust_VatTin}/"+  "{Cust_CstTin}/"+  "{Cust_SalesPID}/"+  "{Cust_Password}/"+  "{Cust_Logo}/"+ "{Cust_GSTNo}/"+  "{Cust_StateId}/"+  "{IsOTP}/"+  "{Cust_ApprovEmail}")
    Observable<CommonErrorResponse> updateProfile(@Path("Cust_Name") String Cust_Name, @Path("Cust_Email") String Cust_Email,
                                                  @Path("Cust_ContactNo") String Cust_ContactNo, @Path("Cust_ID") String Cust_ID,
                                                  @Path("Cust_ContPerson") String Cust_ContPerson, @Path("Cust_Mobile") String Cust_Mobile,
                                                  @Path("Cust_SalesPID") Integer Cust_SalesPID, @Path("Cust_Password") String Cust_Password,
                                                  @Path("Cust_Logo") String Cust_Logo, @Path("Cust_GSTNo") String Cust_GSTNo,
                                                  @Path("Cust_StateId") Integer Cust_StateId, @Path("IsOTP") Boolean IsOTP,
                                                  @Path("Cust_ApprovEmail") String Cust_ApprovEmail);

    @GET(ApiNames.LIVE_STOCK_DESIGN + "/{designid}/" +  "{companyid}" )
    Observable<LiveStockDesignRes> getLiveStockDesign(@Path("designid") int designid, @Path("companyid") int companyid);


    @GET(ApiNames.GET_SIZE_DETAILS)
    Observable<SizeResponse> getSize();

    @GET(ApiNames.GET_COLOR_DETAILS)
    Observable<ColorResponse> getColor();


    @GET(ApiNames.GET_MORE_DETAILS + "/{design_catid}/" +  "{designid}" )
    Observable<ViewMoreDesignResponse> getViewMoreDesign(@Path("design_catid") int id, @Path("designid") int offset);

    @GET(ApiNames.GET_SEARCH_MORE_DETAILS  + "/{colorvalue}/" + "{sizevalue}/" + "{design_catid}/" +  "{designid}" )
    Observable<LiveStockDesignRes> getLiveStockDesignBySearch(@Path("colorvalue") String colorvalue, @Path("sizevalue") String sizevalue,@Path("design_catid") int design_catid, @Path("designid") int designid);

    @GET(ApiNames.CATALOG_DESIGN + "/{designid}" )
    Observable<LiveStockDesignRes> getCatalogDesign(@Path("designid") int designid/*, @Path("companyid") int companyid*/);

    @GET(ApiNames.GET_MORE_DETAILS_CATALOG + "/{design_catid}/" +  "{designid}" )
    Observable<ViewMoreDesignResponse> getCatalogViewMoreDesign(@Path("design_catid") int id, @Path("designid") int offset);


    @GET(ApiNames.LIVE_STOCK_ORDER + "/{design_catid}/" +  "{designid}" )
    Observable<LiveStockOrderResponse> getLiveStockOrder(@Path("design_catid") int id, @Path("designid") int offset);

    @GET(ApiNames.OLD_STOCK_ORDER + "/{design_catid}/" +  "{designid}" )
    Observable<OldStockOrderResponse> getOldStockOrder(@Path("design_catid") int id, @Path("designid") int offset);


    @GET(ApiNames.LIVESTOCK_FILLADDRES + "/{Cust_ID}")
    Observable<AddressResponse> getAddress(@Path("Cust_ID") int Cust_ID);

    @POST(ApiNames.LIVESTOCK_SAVE  + "/{Purchase_Date}/" + "{Cust_id}/" + "{Address_id}")
    Observable<LiveStockSaveResponse> getSaveLiveStock(@Path("Purchase_Date") String Purchase_Date, @Path("Cust_id") int Cust_id, @Path("Address_id") int Address_id);

    @POST(ApiNames.LIVESTOCK_SAVEALL  + "/{STOID}/" + "{Rate}/" + "{CatID}/" +  "{DesignID}/"  + "{ColourID}/" + "{SizeID}/" +  "{Qty}/"+  "{Company_ID}")
    Observable<LiveStockSaveAllResponse> getSaveLiveStockAll(@Path("STOID") int STOID,
                                                             @Path("Rate") double Rate,
                                                             @Path("CatID") int CatID,
                                                             @Path("DesignID") int DesignID,
                                                             @Path("ColourID") int ColourID,
                                                             @Path("SizeID") int SizeID,
                                                             @Path("Qty") int Qty,
                                                             @Path("Company_ID") int Company_ID);


    @GET(ApiNames.LIVESTOCK_FILLRATE + "/{Company_id}")
    Observable<RateResponse> getRate(@Path("Company_id") int Company_id);


    @GET(ApiNames.OLDSTOCK_FILLADDRES + "/{Cust_ID}")
    Observable<AddressResponse> getOldAddress(@Path("Cust_ID") int Cust_ID);

    @POST(ApiNames.OLDSTOCK_SAVE  + "/{Purchase_Date}/" + "{Cust_id}/" + "{Address_id}")
    Observable<LiveStockSaveResponse> getSaveOldStock(@Path("Purchase_Date") String Purchase_Date, @Path("Cust_id") int Cust_id, @Path("Address_id") int Address_id);

    @POST(ApiNames.OLDSTOCK_SAVEALL  + "/{STOID}/" + "{Rate}/" + "{CatID}/" +  "{DesignID}/"  + "{ColourID}/" + "{SizeID}/" +  "{Weight}/"+  "{Qty}/"+  "{Company_ID}/" +"{Old_Days}")
    Observable<LiveStockSaveAllResponse> getSaveOldStockAll(@Path("STOID") int STOID,
                                                             @Path("Rate") double Rate,
                                                             @Path("CatID") int CatID,
                                                             @Path("DesignID") int DesignID,
                                                             @Path("ColourID") int ColourID,
                                                             @Path("SizeID") int SizeID,
                                                             @Path("Weight") double Weight,
                                                             @Path("Qty") int Qty,
                                                             @Path("Company_ID") int Company_ID,
                                                            @Path("Old_Days") int Old_Days
                                                            );


    @GET(ApiNames.OLDSTOCK_FILLRATE + "/{Company_id}")
    Observable<RateResponse> getOldRate(@Path("Company_id") int Company_id);

    @GET(ApiNames.LABOURCHARGE_OTP + "/{id}")
    Observable<LabourChargeOTPResponse> getLabourChargeOtp(@Path("id") String id);


    @GET(ApiNames.PENDING_ORDER + "/{Cust_ID}"+"/{Company_id}"+ "/{offset}" )
    Observable<PendingOrderResponse> getpendingorder( @Path("Cust_ID") int Cust_ID,@Path("Company_id") int Company_id,@Path("offset") int offset);

    @GET(ApiNames.OLD_PENDING_ORDER  + "/{Cust_ID}"+"/{Company_id}"+ "/{offset}" )
    Observable<OldPendingOrderResponse> getoldpendingorder( @Path("Cust_ID") int Cust_ID,@Path("Company_id") int Company_id,@Path("offset") int offset);


    @GET(ApiNames.APPROVED_ORDER  + "/{Cust_ID}"+"/{Company_id}"+ "/{offset}" )
    Observable<ApprovedOrderResponse> getapprovedorder( @Path("Cust_ID") int Cust_ID,@Path("Company_id") int Company_id,@Path("offset") int offset);


    @GET(ApiNames.PENDING_ORDER_DETAILS + "/{id}" )
    Observable<PendingOrderDetails> getpendingorderdetails(@Path("id") int id);

    @GET(ApiNames.OLD_PENDING_ORDER_DETAILS + "/{id}" )
    Observable<OldPendingOrderDetails> getoldpendingorderdetails(@Path("id") int id);


    @GET(ApiNames.APPROVED_ORDER_DETAILS + "/{id}"  )
    Observable<ApprovedOrderDetails> getapprovedorderdetails(@Path("id") int id);


    @POST(ApiNames.CHANGE_PASSWORD + "/{designid}/" +  "{companyid}" )
    Observable<CommonErrorResponse> getChangePassword(@Path("designid") int designid, @Path("companyid") String companyid);


    @GET(ApiNames.LIVE_STOCK_SEARCH +  "/{offset}" + "/{id}" )
    Observable<LiveStockRes> getSearchLiveStock( @Path("offset") int offset,@Path("id") String id);

    @GET(ApiNames.DELETE_ORDER + "/{id}")
    Observable<CommonErrorResponse> getDeleteOrder(@Path("id") int id);

    @GET(ApiNames.MY_CUSTOMER  + "/{Cust_ID}"+"/{offset}" )
    Observable<MyCustomerResponse> getmycustomer(@Path("Cust_ID") int Cust_ID, @Path("offset") int offset);


    @GET(ApiNames.PENDING_ORDER_SALESMAN + "/{Cust_ID}"+"/{Company_id}"+ "/{offset}" )
    Observable<PendingOrderResponse> getpendingordersalesman( @Path("Cust_ID") int Cust_ID,@Path("Company_id") int Company_id,@Path("offset") int offset);

    @GET(ApiNames.OLD_PENDING_ORDER_SALESMAN  + "/{Cust_ID}"+"/{Company_id}"+ "/{offset}" )
    Observable<OldPendingOrderResponse> getoldpendingordersalesman( @Path("Cust_ID") int Cust_ID,@Path("Company_id") int Company_id,@Path("offset") int offset);


    @GET(ApiNames.APPROVED_ORDER_SALESMAN  + "/{Cust_ID}"+"/{Company_id}"+ "/{offset}" )
    Observable<ApprovedOrderResponse> getapprovedordersalesman( @Path("Cust_ID") int Cust_ID,@Path("Company_id") int Company_id,@Path("offset") int offset);

    @GET(ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER + "/{Cust_ID}"+"/{Company_id}")
    Observable<CustomerListResponse> getcustomerList(@Path("Cust_ID") int Cust_ID, @Path("Company_id") int Company_id);

    @GET(ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER + "/{Cust_ID}"+"/{Company_id}")
    Observable<CustomerListResponse> getcustomerListold(@Path("Cust_ID") int Cust_ID, @Path("Company_id") int Company_id);




}
