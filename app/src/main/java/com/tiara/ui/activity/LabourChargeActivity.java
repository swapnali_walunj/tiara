package com.tiara.ui.activity;


import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityLabourChargeBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.LabourChargeAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.Response.CustomerLoginRes;
import com.tiara.ui.model.Response.LabourChargeOTPResponse;
import com.tiara.ui.model.Response.LabourChargeResponse;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.CUSTOMER_RESPONSE;
import static com.tiara.utils.Constants.LOAD_MORE_DELAY;
import static com.tiara.utils.Constants.MOBILE_NO;

public class LabourChargeActivity extends BaseActivity implements OnLoadMoreListener {
    ActivityLabourChargeBinding binding;
    CommonPresenterImpl presenter;
    LabourChargeAdapter adapter;
    List<LabourChargeResponse.Datum> datalist=new ArrayList<>();
    boolean isLoadMore = false;
    int offset=0;
    LabourChargeResponse response;
    LinearLayoutManager layoutManager;
    String otp="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_labour_charge, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        binding.commonToolbarLayout.toolbarTitle.setText("Labour Charge");
        layoutManager = new LinearLayoutManager(this);
        customNavigationViewAdapter.showSelected(2, false,-1);
        presenter=new CommonPresenterImpl(this);
        setUpRecyclerView(binding.fragmentRecyclerCommon.recyclerCommon,new LinearLayoutManager(this));

        if(!TextUtils.isEmpty(getPreferenceInstance().getValue(CUSTOMER_RESPONSE)))
        {

            String res=getPreferenceInstance().getValue(CUSTOMER_RESPONSE);
            CustomerLoginRes customerLoginRes = new Gson().fromJson(res, CustomerLoginRes.class);
            if(customerLoginRes.getData().get(0).getIsOTP())
            {
                binding.llOtp.setVisibility(View.VISIBLE);
                binding.llData.setVisibility(View.GONE);

            }else{
                callApi(offset);
          }
        }

        binding.tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

 binding.tvSendOtp.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View view) {
         showProgressDialog();
         callOtp();
     }
 });

        binding.tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(binding.edittextCompany.getText().toString()))
                {
                    if(binding.edittextCompany.getText().toString().equalsIgnoreCase(otp))
                    {
                        String res=getPreferenceInstance().getValue(CUSTOMER_RESPONSE);
                        CustomerLoginRes customerLoginRes = new Gson().fromJson(res, CustomerLoginRes.class);
                        customerLoginRes.getData().get(0).setIsOTP(false);
                        getPreferenceInstance().setValue(CUSTOMER_RESPONSE,res);
                        callApi(0);
                    }else{
                        showToast("Please enter valid OTP");
                    }

                }else{
                   showToast("Please enter Otp");
                }
            }
        });

        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                      binding.swipe.setRefreshing(false);
                    }
                }, 2000);
            }
        });



    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        super.onResponseSuccess(data, apiName);
        switch (apiName) {
            case ApiNames.LABOUR_CHARGE:
                LabourChargeResponse response = (LabourChargeResponse) data;
                    this.response=response;
                    showData(response);

                break;
            case ApiNames.LABOURCHARGE_OTP:
                hideProgressDialog();
                showToast("OTP Send on your mobile");
                LabourChargeOTPResponse labourChargeOTPResponse=(LabourChargeOTPResponse) data;
                otp=labourChargeOTPResponse.getData().get(0).getOTP();


        }
    }
    private void showData(final LabourChargeResponse response) {
     /*   if (response != null && response.getData() != null && response.getData().size() > 0) {

            if (adapter == null) {
                if (!isLoadMore) {
                    datalist = response.getData();
                    adapter = new LabourChargeAdapter(this, datalist, new OnItemClickListener() {
                        @Override
                        public void onClick(Object Item, View v) {

                        }
                    });
                    binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                    adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager,null);
                    adapter.setOnLoadMoreListener(this);
                } else {
                    hideOtherLoadMore();
                }
                switchVisibility(0);
            } else {
                if (!isLoadMore) {
                    datalist = response.getData();
                    adapter.refresh(datalist);
                    adapter.setLoaded();
                } else {
                    hideOtherLoadMore();
                }
                adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager,null);
                adapter.setOnLoadMoreListener(this);
                switchVisibility(0);
            }
        } else {
            if (!isLoadMore) {
                switchVisibility(1);
            }else {
                hideOtherLoadMore();
            }


        }*/
        if (response != null) {
            if (datalist != null && datalist.size() == 0 && response.getData() != null && response.getData().size() == 0) {
                adapter = new LabourChargeAdapter(this, datalist, new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {

                    }
                });
                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
              switchVisibility(0);
            } else {
                if (!isLoadMore) {
                    datalist = response.getData();
                    if (adapter == null) {
                        adapter = new LabourChargeAdapter(this, datalist, new OnItemClickListener() {
                            @Override
                            public void onClick(Object Item, View v) {

                            }
                        });
                        binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                    } else {
                        adapter.refresh(datalist);
                        adapter.setLoaded();
                    }
                    adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                    adapter.setOnLoadMoreListener(LabourChargeActivity.this);
                    switchVisibility(0);

                    if (datalist != null && datalist.size() == 0) {
                       switchVisibility(0);
                    }

                } else {
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() > 0) {
                                datalist.remove(datalist.size() - 1);
                            }
                            datalist.addAll(response.getData());
                            if (response.getData().size() == 0) {

                            }
                            isLoadMore = false;
                            if (adapter == null) {
                                adapter = new LabourChargeAdapter(LabourChargeActivity.this, datalist, new OnItemClickListener() {
                                    @Override
                                    public void onClick(Object Item, View v) {

                                    }
                                });
                                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                            } else {
                                adapter.refresh(datalist);
                                adapter.setLoaded();
                            }
                            adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                            adapter.setOnLoadMoreListener(LabourChargeActivity.this);
                            switchVisibility(0);
                        }
                    }, LOAD_MORE_DELAY);
                }
            }

        } else {
            switchVisibility(0);
        }

    }

    public void callOtp()
    {
        presenter.getLabourChargeOtp(isConnected,getPreferenceInstance().getValue(MOBILE_NO));

    }

    public void callApi(int offset)
    {
        binding.llOtp.setVisibility(View.GONE);
        binding.llData.setVisibility(View.VISIBLE);
            presenter.getLabourCharge(getCustUserId(),offset,isConnected);

    }

    @Override
    public void switchVisibility(int type) {
        binding.swipe.setRefreshing(false);
        binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
        switch (type) {
            case 0:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.VISIBLE);
                break;

            case 1:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;

            case 2:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.fragmentRecyclerCommon.emptyLayout.textEmpty.setText(getString(R.string.no_internet));
                binding.fragmentRecyclerCommon.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 3:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 4:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void onLoadMore(String type) {
        //if(!isLoadMore) {
             isLoadMore = true;
            LabourChargeResponse.Datum dataItem = new LabourChargeResponse.Datum();
            dataItem.setCatName("Loading");
            datalist.add(dataItem);
            adapter.refresh(datalist);
            offset=offset+1;
            callApi(offset);
       // }


    }
}
