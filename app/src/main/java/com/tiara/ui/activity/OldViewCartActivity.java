package com.tiara.ui.activity;

import android.app.DatePickerDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityViewCartBinding;
import com.tiara.db.DatabaseInitUtil;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.FinalCatalogOrderAdapter;
import com.tiara.ui.adapter.FinalOrderAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.AddressResponse;
import com.tiara.ui.model.Response.CustomerListResponse;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.LiveStockSaveResponse;
import com.tiara.ui.model.Response.OldStockOrder;
import com.tiara.ui.model.Response.RateResponse;
import com.tiara.utils.AppUtils;
import com.tiara.utils.Constants;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.tiara.utils.Constants.COMPANY_ID;
import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.USER_ID;

public class OldViewCartActivity extends BaseActivity {

    ActivityViewCartBinding binding;
    FinalCatalogOrderAdapter orderAdapter;

    private LiveData<List<OldStockOrder>> mObservableProduct;
    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final String dd_MM_yyyy = "dd-MM-yyyy";
    CommonPresenterImpl presenter;
    List<AddressResponse> brands = new ArrayList<>();
    String[] brandsArray;
    String[] custArray;
    HashMap<Integer, Integer> map_brands = new HashMap<Integer, Integer>();
    HashMap<Integer, Integer> map_cust = new HashMap<Integer, Integer>();
    int sopid = 0;
    double rate = 0;
    int addressid = 0;
    int custid = 0;
    String startDate = "";
    double totalwt=0.0;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_cart);
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.GONE);
        binding.llData.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarTitle.setText("Catalog View Cart");
        presenter = new CommonPresenterImpl(this);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        configureToolbar(binding.commonToolbarLayout.toolbar, R.drawable.abc_ic_ab_back_material_new, ContextCompat.getColor(this, R.color.white));
        setUpRecyclerView(binding.rvCart, new LinearLayoutManager(this));
       // presenter.getOldAddress(getPreferenceInstance().getIntValue(USER_ID, 0), isConnected);
        presenter.getOldRate(getPreferenceInstance().getIntValue(COMPANY_ID, 0), isConnected);
       // binding.edittextCustomerName.setText(getPreferenceInstance().getValue(Constants.USER_NAME));
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            custArray = new String[1];
            custArray[0]=getPreferenceInstance().getValue(Constants.USER_NAME);
            presenter.getOldAddress(getPreferenceInstance().getIntValue(USER_ID, 0), isConnected);
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.item_spinner_add_event, R.id.tv_name, custArray);
            adapter1.setDropDownViewResource(R.layout.item_spinner_multiline);
            binding.spinnerCustomer.setAdapter(adapter1);
        }else{
            presenter.getCustomerListold(getCustUserId(), getPreferenceInstance().getIntValue(COMPANY_ID, 0), isConnected);
        }
        binding.edittextDate.setText(getTodayDate());
        binding.edittextRate.setEnabled(false);
        binding.edittextRate.setEnabled(false);
       // binding.edittextCustomerName.setEnabled(false);
        binding.edittextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(OldViewCartActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                binding.edittextDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        getOfflineOrderListing();
        binding.tvSave.setText("Order Request");
        binding.tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*String name= binding.searchAutoComplete.getText().toString();
                if(!TextUtils.isEmpty(name) && brandsArray!=null &&  Arrays.asList(brandsArray).contains(name)) {
                    int pos = Arrays.asList(brandsArray).indexOf(name);
                    int addressid = map_brands.get(pos);*/

                if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
                    presenter.getSaveOldStock(convertDateFormat(binding.edittextDate.getText().toString().trim(), dd_MM_yyyy, yyyy_MM_dd), getPreferenceInstance().getIntValue(USER_ID, 0), addressid, isConnected);
                }else{
                    presenter.getSaveOldStock(convertDateFormat(binding.edittextDate.getText().toString().trim(), dd_MM_yyyy, yyyy_MM_dd), custid, addressid, isConnected);
                }
              //  presenter.getSaveOldStock(convertDateFormat(binding.edittextDate.getText().toString().trim(), dd_MM_yyyy, yyyy_MM_dd), getPreferenceInstance().getIntValue(USER_ID, 0), addressid, isConnected);
                /*}else{
                    showToast("Please select address");
                }*/

            }
        });

        binding.spinnerAddress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = Arrays.asList(brandsArray).indexOf(adapterView.getItemAtPosition(i).toString());
                addressid = map_brands.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.spinnerCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = Arrays.asList(custArray).indexOf(adapterView.getItemAtPosition(i).toString());
                if(!getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
                    custid = map_cust.get(pos);
                    presenter.getOldAddress(custid, isConnected);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    public static String convertDateFormat(String strLocalTime, String oldFormat, String newFormat) {
        String s = "";
        SimpleDateFormat LocalTimeFormatter = new SimpleDateFormat(oldFormat, Locale.getDefault());
        SimpleDateFormat TimeFormatter = new SimpleDateFormat(newFormat, Locale.getDefault());

        try {
            LocalTimeFormatter.setTimeZone(TimeZone.getDefault());
            // millisecond = LocalTimeFormatter.parse(strLocalTime).getTime();
            Date d = LocalTimeFormatter.parse(strLocalTime);
            TimeFormatter.setTimeZone(TimeZone.getDefault());
            s = TimeFormatter.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.OLDSTOCK_FILLADDRES:
                AddressResponse response = (AddressResponse) data;
                ArrayList<AddressResponse.Datum> searchArrayList = new ArrayList<AddressResponse.Datum>();
                searchArrayList.addAll(response.getData());
                binding.edittextAddress.setText(searchArrayList.get(0).getAddressAddress());
                addressid=response.getData().get(0).getAddressID();
                brandsArray = new String[response.getData().size()];
                for (int i = 0; i < searchArrayList.size(); i++) {
                    map_brands.put(i, searchArrayList.get(i).getAddressID());
                    brandsArray[i] = searchArrayList.get(i).getAddressAddress();
                }
              /*  List<String> list = new ArrayList<>(Arrays.asList(brandsArray));
                AutoCompleteAdapter adapter = new AutoCompleteAdapter(this, android.R.layout.simple_dropdown_item_1line, android.R.id.text1,list);
                binding.searchAutoComplete.setThreshold(1);
                binding.searchAutoComplete.setAdapter(adapter);
                adapter.notifyDataSetChanged();*/
                ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.item_spinner_add_event, R.id.tv_name, brandsArray);
                adapter1.setDropDownViewResource(R.layout.item_spinner_multiline);
                binding.spinnerAddress.setAdapter(adapter1);
                break;

            case ApiNames.OLDSTOCK_SAVE:
                LiveStockSaveResponse response1 = (LiveStockSaveResponse) data;
                List<OldStockOrder> liveStockOrders = orderAdapter.getData();
                OldStockOrder stockOrder = liveStockOrders.get(0);
                sopid = Integer.parseInt(response1.getData().get(0).getSTOID());
                /*if (!TextUtils.isEmpty(binding.edittextRate.getText().toString().trim())) {
                    rate = Integer.parseInt(binding.edittextRate.getText().toString().trim());
                }*/
                presenter.getSaveAlloldStock(Integer.parseInt(response1.getData().get(0).getSTOID()), rate, stockOrder.getCatID(), stockOrder.getDesignID(), stockOrder.getColourID(), stockOrder.getSizeID(),stockOrder.getDesignAvgwt(), stockOrder.getQty(), getPreferenceInstance().getIntValue(USER_ID, 0), stockOrder.getDesignAvgdays(), isConnected, 0);

                break;
            case ApiNames.OLDSTOCK_FILLRATE:
                RateResponse rateResponse = (RateResponse) data;
                rate=rateResponse.getData().get(0).getRateValue();
                binding.edittextRate.setText(rateResponse.getData().get(0).getRateValue() + "");

                break;

            case ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER:
                CustomerListResponse response2 = (CustomerListResponse) data;
                ArrayList<CustomerListResponse.Datum> CustomerArrayList = new ArrayList<CustomerListResponse.Datum>();
                CustomerArrayList.addAll(response2.getData());
                binding.edittextAddress.setText(CustomerArrayList.get(0).getCustName());
                custArray= new String[response2.getData().size()];
                custid=response2.getData().get(0).getCustID();
                for (int i = 0; i < CustomerArrayList.size(); i++) {
                    map_cust.put(i, CustomerArrayList.get(i).getCustID());
                    custArray[i] = CustomerArrayList.get(i).getCustName();
                }

                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.item_spinner_add_event, R.id.tv_name, custArray);
                adapter2.setDropDownViewResource(R.layout.item_spinner_multiline);
                binding.spinnerCustomer.setAdapter(adapter2);
                break;


        }
    }


    @Override
    public void onResponseSuccess(Object data, String apiName, int position, String grppostion) {
        switch (apiName) {
            case ApiNames.OLDSTOCK_SAVEALL:
                position++;
                if (position < orderAdapter.getData().size()) {
                    OldStockOrder stockOrder = orderAdapter.getData().get(position);
                    presenter.getSaveAlloldStock(sopid, 2, stockOrder.getCatID(), stockOrder.getDesignID(), stockOrder.getColourID(), stockOrder.getSizeID(),stockOrder.getDesignAvgwt(), stockOrder.getQty(), getPreferenceInstance().getIntValue(USER_ID, 0), stockOrder.getDesignAvgdays(), isConnected, position);

                } else {
                    DatabaseInitUtil.deleteAllCatalogStockOrder(appDatabase);
                    finish();
                    startActivity(new Intent(OldViewCartActivity.this,OldPendingOrderActivity.class));
                }
        }
    }

    synchronized void getOfflineOrderListing() {
        mObservableProduct = appDatabase.catalogOrderListingDao().loadOrderList();
        mObservableProduct.observe(this, new Observer<List<OldStockOrder>>() {
            @Override
            public void onChanged(@Nullable List<OldStockOrder> itelist) {
                if (itelist != null && itelist.size() > 0) {
                    if (orderAdapter == null) {
                        orderAdapter = new FinalCatalogOrderAdapter(OldViewCartActivity.this, itelist, new OnItemClickListener() {
                            @Override
                            public void onClick(Object Item, View v) {
                                OldStockOrder liveStockOrder = (OldStockOrder) Item;
                                DatabaseInitUtil.deleteCatalogOrderProductFromCart(appDatabase, liveStockOrder.getId());
                            }
                        });
                        binding.rvCart.setAdapter(orderAdapter);
                        for(int i=0;i< itelist.size();i++)
                        {
                            double wt=itelist.get(i).getDesignAvgwt()*itelist.get(i).getQty();
                            totalwt= totalwt+wt;
                        }
                        binding.tvEstWeight.setText(new DecimalFormat("##.##").format(totalwt)+"");
                        binding.tvDays.setText("30");

                    } else {
                        orderAdapter.updateAdapter(itelist);
                    }
                } else {
                    // switchVisibility(4, "");
                   // switchVisibility(3);
                    binding.emptyLayout.linearEmpty.setVisibility(View.VISIBLE);
                    binding.llData.setVisibility(View.GONE);
                }

            }
        });
    }


    public  String getTodayDate() {
        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            //c.add(Calendar.DAY_OF_YEAR, 1);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dd_MM_yyyy, Locale.getDefault());
            formattedDate = simpleDateFormat.format(c.getTime());
        } catch (Exception e) {
            formattedDate = "";
        }
        return formattedDate;
    }
}
