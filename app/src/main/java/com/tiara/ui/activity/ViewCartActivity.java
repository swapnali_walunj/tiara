package com.tiara.ui.activity;

import android.app.DatePickerDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityViewCartBinding;
import com.tiara.db.DatabaseInitUtil;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.AutoCompleteAdapter;
import com.tiara.ui.adapter.FinalOrderAdapter;
import com.tiara.ui.adapter.OrderAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.AddressResponse;
import com.tiara.ui.model.Response.CustomerListResponse;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.LiveStockSaveResponse;
import com.tiara.ui.model.Response.RateResponse;
import com.tiara.utils.AppUtils;
import com.tiara.utils.Constants;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.tiara.utils.Constants.COMPANY_ID;
import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.USER_ID;
import static com.tiara.utils.Constants.USER_NAME;

public class ViewCartActivity extends BaseActivity {

    ActivityViewCartBinding binding;
    FinalOrderAdapter orderAdapter;

    private LiveData<List<LiveStockOrder>> mObservableProduct;
    public static final String yyyy_MM_dd = "yyyy-MM-dd";
    public static final String dd_MM_yyyy = "dd-MM-yyyy";
    CommonPresenterImpl presenter;
    List<AddressResponse> brands = new ArrayList<>();
    String[] brandsArray;
    String[] custArray;
    HashMap<Integer, Integer> map_brands = new HashMap<Integer, Integer>();
    HashMap<Integer, Integer> map_cust = new HashMap<Integer, Integer>();
    int sopid = 0;
    double rate = 0;
    int addressid = 0;
    int custid = 0;
    String startDate = "";
    private int mYear, mMonth, mDay, mHour, mMinute;
    double totalwt=0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_cart);
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.GONE);
        binding.commonToolbarLayout.toolbarTitle.setText("Live Stock View Cart");
        presenter = new CommonPresenterImpl(this);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        configureToolbar(binding.commonToolbarLayout.toolbar, R.drawable.abc_ic_ab_back_material_new, ContextCompat.getColor(this, R.color.white));
        setUpRecyclerView(binding.rvCart, new LinearLayoutManager(this));

        presenter.getRate(getPreferenceInstance().getIntValue(COMPANY_ID, 0), isConnected);
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            custArray = new String[1];
            custArray[0]=getPreferenceInstance().getValue(Constants.USER_NAME);
            presenter.getAddress(getPreferenceInstance().getIntValue(USER_ID, 0), isConnected);
            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.item_spinner_add_event, R.id.tv_name, custArray);
            adapter1.setDropDownViewResource(R.layout.item_spinner_multiline);
            binding.spinnerCustomer.setAdapter(adapter1);
        }else{
            presenter.getCustomerList(getCustUserId(), getPreferenceInstance().getIntValue(COMPANY_ID, 0), isConnected);
        }
       // binding.edittextCustomerName.setText(getPreferenceInstance().getValue(Constants.USER_NAME));
        binding.edittextDate.setText(getTodayDate());
        binding.edittextRate.setEnabled(false);
        //binding.edittextCustomerName.setEnabled(false);

        binding.edittextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewCartActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                binding.edittextDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });
        getOfflineOrderListing();

        binding.tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showProgressDialog();
                if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
                    presenter.getSaveLiveStock(convertDateFormat(binding.edittextDate.getText().toString().trim(), dd_MM_yyyy, yyyy_MM_dd), getPreferenceInstance().getIntValue(USER_ID, 0), addressid, isConnected);
                }else{
                    presenter.getSaveLiveStock(convertDateFormat(binding.edittextDate.getText().toString().trim(), dd_MM_yyyy, yyyy_MM_dd), custid, addressid, isConnected);
                }


            }
        });

        binding.spinnerAddress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = Arrays.asList(brandsArray).indexOf(adapterView.getItemAtPosition(i).toString());
                addressid = map_brands.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.spinnerCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int pos = Arrays.asList(custArray).indexOf(adapterView.getItemAtPosition(i).toString());
                if(!getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
                    custid = map_cust.get(pos);
                    presenter.getAddress(custid, isConnected);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }


    public static String convertDateFormat(String strLocalTime, String oldFormat, String newFormat) {
        String s = "";
        SimpleDateFormat LocalTimeFormatter = new SimpleDateFormat(oldFormat, Locale.getDefault());
        SimpleDateFormat TimeFormatter = new SimpleDateFormat(newFormat, Locale.getDefault());

        try {
            LocalTimeFormatter.setTimeZone(TimeZone.getDefault());
            // millisecond = LocalTimeFormatter.parse(strLocalTime).getTime();
            Date d = LocalTimeFormatter.parse(strLocalTime);
            TimeFormatter.setTimeZone(TimeZone.getDefault());
            s = TimeFormatter.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.LIVESTOCK_FILLADDRES:
                AddressResponse response = (AddressResponse) data;
                ArrayList<AddressResponse.Datum> searchArrayList = new ArrayList<AddressResponse.Datum>();
                searchArrayList.addAll(response.getData());
                binding.edittextAddress.setText(searchArrayList.get(0).getAddressAddress());
                brandsArray = new String[response.getData().size()];
                addressid=response.getData().get(0).getAddressID();
                for (int i = 0; i < searchArrayList.size(); i++) {
                    map_brands.put(i, searchArrayList.get(i).getAddressID());
                    brandsArray[i] = searchArrayList.get(i).getAddressAddress();
                }

                ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, R.layout.item_spinner_add_event, R.id.tv_name, brandsArray);
                adapter1.setDropDownViewResource(R.layout.item_spinner_multiline);
                binding.spinnerAddress.setAdapter(adapter1);
                break;

            case ApiNames.LIVESTOCK_SAVE:
                LiveStockSaveResponse response1 = (LiveStockSaveResponse) data;
                List<LiveStockOrder> liveStockOrders = orderAdapter.getData();
                LiveStockOrder stockOrder = liveStockOrders.get(0);
                sopid = Integer.parseInt(response1.getData().get(0).getSTOID());
                presenter.getSaveAllLiveStock(Integer.parseInt(response1.getData().get(0).getSTOID()), rate, stockOrder.getCatID(), stockOrder.getDesignID(), stockOrder.getColourID(), stockOrder.getSizeID(), stockOrder.getQty(), getPreferenceInstance().getIntValue(COMPANY_ID, 0), isConnected, 0);

                break;
            case ApiNames.LIVESTOCK_FILLRATE:
                RateResponse rateResponse = (RateResponse) data;
                rate=rateResponse.getData().get(0).getRateValue();
                binding.edittextRate.setText(rateResponse.getData().get(0).getRateValue() + "");

                break;
            case ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER:
                CustomerListResponse response2 = (CustomerListResponse) data;
                ArrayList<CustomerListResponse.Datum> CustomerArrayList = new ArrayList<CustomerListResponse.Datum>();
                CustomerArrayList.addAll(response2.getData());
                binding.edittextAddress.setText(CustomerArrayList.get(0).getCustName());
                custArray= new String[response2.getData().size()];
                custid=response2.getData().get(0).getCustID();
                for (int i = 0; i < CustomerArrayList.size(); i++) {
                    map_cust.put(i, CustomerArrayList.get(i).getCustID());
                    custArray[i] = CustomerArrayList.get(i).getCustName();
                }

                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.item_spinner_add_event, R.id.tv_name, custArray);
                adapter2.setDropDownViewResource(R.layout.item_spinner_multiline);
                binding.spinnerCustomer.setAdapter(adapter2);
                break;


        }
    }


    @Override
    public void onResponseSuccess(Object data, String apiName, int position, String grppostion) {
        switch (apiName) {
            case ApiNames.LIVESTOCK_SAVEALL:
                position++;
                if (position < orderAdapter.getData().size()) {
                    LiveStockOrder stockOrder = orderAdapter.getData().get(position);
                    presenter.getSaveAllLiveStock(sopid, rate, stockOrder.getCatID(), stockOrder.getDesignID(), stockOrder.getColourID(), stockOrder.getSizeID(), stockOrder.getQty(), getPreferenceInstance().getIntValue(COMPANY_ID, 0), isConnected, position);
                } else {
                    hideProgressDialog();
                    DatabaseInitUtil.deleteAllLiveStockOrder(appDatabase);
                    finish();
                    startActivity(new Intent(ViewCartActivity.this,PendingOrderActivity.class));
                }
        }
    }

    synchronized void getOfflineOrderListing() {
        mObservableProduct = appDatabase.orderListingDao().loadOrderList();
        mObservableProduct.observe(this, new Observer<List<LiveStockOrder>>() {
            @Override
            public void onChanged(@Nullable List<LiveStockOrder> itelist) {
                if (itelist != null && itelist.size() > 0) {
                    if (orderAdapter == null) {
                        orderAdapter = new FinalOrderAdapter(ViewCartActivity.this, itelist, new OnItemClickListener() {
                            @Override
                            public void onClick(Object Item, View v) {
                                LiveStockOrder liveStockOrder = (LiveStockOrder) Item;
                                DatabaseInitUtil.deleteOrderProductFromCart(appDatabase, liveStockOrder.getId());
                            }
                        });
                        binding.rvCart.setAdapter(orderAdapter);
                        for(int i=0;i< itelist.size();i++)
                        {
                            double wt=itelist.get(i).getAvgWeight()*itelist.get(i).getQty();
                            totalwt= totalwt+wt;
                        }
                        binding.tvEstWeight.setText(new DecimalFormat("##.##").format(totalwt)+"");

                    } else {
                        orderAdapter.updateAdapter(itelist);
                    }
                } else {
                    // switchVisibility(4, "");
                    //switchVisibility(3);
                    binding.emptyLayout.linearEmpty.setVisibility(View.VISIBLE);
                    binding.llData.setVisibility(View.GONE);
                }

            }
        });
    }


    public  String getTodayDate() {
        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            //c.add(Calendar.DAY_OF_YEAR, 1);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dd_MM_yyyy, Locale.getDefault());
            formattedDate = simpleDateFormat.format(c.getTime());
        } catch (Exception e) {
            formattedDate = "";
        }
        return formattedDate;
    }
}
