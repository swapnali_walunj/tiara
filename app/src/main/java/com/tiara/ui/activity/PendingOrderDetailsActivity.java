package com.tiara.ui.activity;

import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityPendingOrderDetailsBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.PendingOrderDetailsAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.PendingOrderDetails;

public class PendingOrderDetailsActivity extends BaseActivity {

    ActivityPendingOrderDetailsBinding binding;
    CommonPresenterImpl presenter;
    PendingOrderDetailsAdapter  pendingOrderDetailsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pending_order_details);
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.GONE);
        binding.commonToolbarLayout.toolbarTitle.setText("View Pending Order Details");
        presenter = new CommonPresenterImpl(this);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        configureToolbar(binding.commonToolbarLayout.toolbar, R.drawable.abc_ic_ab_back_material_new, ContextCompat.getColor(this, R.color.white));
        setUpRecyclerView(binding.rvCart, new LinearLayoutManager(this));
        presenter.getPendingOrderDetails(isConnected,getIntent().getIntExtra("id",0));
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.PENDING_ORDER_DETAILS:
                PendingOrderDetails pendingOrderDetails=(PendingOrderDetails)data;
                setData(pendingOrderDetails);
                break;
        }
    }

    private void setData(final PendingOrderDetails pendingOrderDetails) {
        binding.edittextCustomerName.setText(pendingOrderDetails.getData().get(0).getCustName());
        binding.edittextAddress.setText(pendingOrderDetails.getData().get(0).getAddressAddress());
        binding.edittextDate.setText(pendingOrderDetails.getData().get(0).getSTODate());
        binding.edittextRate.setText(pendingOrderDetails.getData().get(0).getSTODRate()+"");

        pendingOrderDetailsAdapter=new PendingOrderDetailsAdapter(this, pendingOrderDetails.getData(), new OnItemClickListener() {
            @Override
            public void onClick(Object Item, View v) {
                int position= (int) Item;
                PendingOrderDetails.Datum datum= pendingOrderDetails.getData().get(position);
               presenter.getDeleteOrder(isConnected,datum.getPDID());
               pendingOrderDetails.getData().remove(position);
               pendingOrderDetailsAdapter.updateAdapter(pendingOrderDetails.getData());
            }
        });
        binding.rvCart.setAdapter(pendingOrderDetailsAdapter);


    }
}
