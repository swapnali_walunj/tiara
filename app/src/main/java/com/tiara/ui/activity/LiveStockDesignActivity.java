package com.tiara.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.jakewharton.rxbinding.view.RxView;
import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityLiveStockBinding;
import com.tiara.databinding.ActivityLiveStockDesignBinding;
import com.tiara.databinding.CustomDialogOrderLayoutBinding;
import com.tiara.db.DatabaseInitUtil;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.LiveStockDesignAdapter;
import com.tiara.ui.adapter.OrderAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.fragment.FilterFragment;
import com.tiara.ui.listener.OnFilterListener;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.NavigationModel;
import com.tiara.ui.model.Response.FilterCopy;
import com.tiara.ui.model.Response.LiveStockDesignRes;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.LiveStockOrderResponse;
import com.tiara.utils.AppUtils;
import com.tiara.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

import static com.tiara.utils.Constants.COMPANY_ID;
import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.USER_NAME;

public class LiveStockDesignActivity extends BaseActivity implements OnFilterListener {
    ActivityLiveStockDesignBinding binding;
    ArrayList<NavigationModel> navigationModels;
    FilterFragment mFilterFragment1;
    FragmentManager mFragmentManager;
    CommonPresenterImpl presenter;
    LiveStockDesignAdapter adapter;
    List<LiveStockDesignRes.Datum> datalist=new ArrayList<>();
    boolean isLoadMore = false;
    int offset=0;
    LiveStockDesignRes response;
    LinearLayoutManager layoutManager;
    int designid=0;
    FilterCopy filterCopy;
    OrderAdapter orderAdapter;
    private LiveData<List<LiveStockOrder>> mObservableProduct;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  DataBindingUtil.setContentView(this, R.layout.activity_live_stock_design);
       /* baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);*/
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarTitle.setText("Live Stock Design");
        configureToolbar(binding.commonToolbarLayout.toolbar, R.drawable.abc_ic_ab_back_material_new, ContextCompat.getColor(this, R.color.white));
        designid=getIntent().getIntExtra("designid",0);
        binding.commonToolbarLayout.toolbarImageSearch.setVisibility(View.VISIBLE);
        //binding.fab.setVisibility(View.VISIBLE);
        presenter=new CommonPresenterImpl(this);
        mFragmentManager = getSupportFragmentManager();
       // layoutManager = new LinearLayoutManager(this);
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            customNavigationViewAdapter.showSelected(3, false, -1);
        }else{
            customNavigationViewAdapter.showSelected(2, false, -1);
        }
         setUpRecyclerView(binding.rvFeeds,new LinearLayoutManager(this));
         callApi();
         getOfflineOrderListing();


        binding.commonToolbarLayout.toolbarImageSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFragmentManager.getBackStackEntryCount() == 0) {
                    mFilterFragment1 = new FilterFragment();
                    FragmentTransaction ft = mFragmentManager.beginTransaction();
                    ft.add(binding.frameAdoption.getId(), mFilterFragment1);
                    ft.addToBackStack(Constants.FILTER);
                    ft.commit();
                }
                binding.frameAdoption.setVisibility(View.VISIBLE);
                AppUtils.animateFromBottomtoTop(LiveStockDesignActivity.this, binding.frameAdoption, true);
            }
        });
        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.swipe.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        binding.commonToolbarLayout.toolbarImageSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LiveStockDesignActivity.this,ViewCartActivity.class));
            }
        });

        RxView.clicks(binding.fab).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {

                if (mFragmentManager.getBackStackEntryCount() == 0) {
                    mFilterFragment1 = new FilterFragment();
                    FragmentTransaction ft = mFragmentManager.beginTransaction();
                    ft.add(binding.frameAdoption.getId(), mFilterFragment1);
                    ft.addToBackStack(Constants.FILTER);
                    ft.commit();
                }
                binding.frameAdoption.setVisibility(View.VISIBLE);
                AppUtils.animateFromBottomtoTop(LiveStockDesignActivity.this, binding.frameAdoption, true);

           }
        });
    }
    synchronized void getOfflineOrderListing() {
        mObservableProduct = appDatabase.orderListingDao().loadOrderList();
        mObservableProduct.observe(this, new Observer<List<LiveStockOrder>>() {
            @Override
            public void onChanged(@Nullable List<LiveStockOrder> itelist) {
                if (itelist != null && itelist.size() > 0) {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.VISIBLE);
                    binding.commonToolbarLayout.tvCount.setText(itelist.size()+"");
                    }
                 else {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.GONE);
                }

            }
        });
    }
    public void callApi()
    {
        presenter.getLiveStockDesign(designid,getPreferenceInstance().getIntValue(COMPANY_ID,0),isConnected);

    }

    public void callSearchApi()
    {
        presenter.getViewMoreDesignBysearch(filterCopy.getColorid()==null?"":filterCopy.getColorid().startsWith(",") ? filterCopy.getColorid().substring(1) : filterCopy.getColorid(),filterCopy.getSizeid()==null?"":filterCopy.getSizeid().startsWith(",") ? filterCopy.getSizeid().substring(1) : filterCopy.getSizeid(),designid,getPreferenceInstance().getIntValue(COMPANY_ID,0),isConnected);

    }




    @Override
    public void onFilterApplied(FilterCopy filterCopy) {
        binding.frameAdoption.setVisibility(View.GONE);
        this.filterCopy = filterCopy;
//        binding.textFilter.setVisibility(View.VISIBLE);
        AppUtils.logMe("FILTER", new Gson().toJson(filterCopy));
        switchVisibility(4);
        if (datalist != null && datalist.size() > 0) {
            datalist.clear();
        }
        isLoadMore = false;
        callSearchApi();
        binding.frameAdoption.setVisibility(View.GONE);
        AppUtils.animateFromBottomtoTop(LiveStockDesignActivity.this, binding.frameAdoption, false);


    }

    @Override
    public void onFilterReset(FilterCopy filterCopy) {
        if (this.filterCopy != null) {
            this.filterCopy.clearFilter();
        }
        AppUtils.hideKeyboard(LiveStockDesignActivity.this);
        AppUtils.logMe("FILTER", new Gson().toJson(filterCopy));
        if (datalist != null && datalist.size() > 0) {
            datalist.clear();
        }
        if (adapter != null) {
            adapter = null;
        }
        isLoadMore = false;
        callApi();

     /*   mFragmentManager
                .beginTransaction()
                .remove(mFilterFragment1)
                .commit();
        mFragmentManager.popBackStack();*/
        for(int i = 0; i < mFragmentManager.getBackStackEntryCount(); ++i) {
            mFragmentManager.popBackStack();
        }

        binding.frameAdoption.setVisibility(View.GONE);
        AppUtils.animateFromBottomtoTop(LiveStockDesignActivity.this, binding.frameAdoption, false);
    }

    @Override
    public void onFilterHide() {
        binding.frameAdoption.setVisibility(View.GONE);
        AppUtils.animateFromBottomtoTop(LiveStockDesignActivity.this, binding.frameAdoption, false);
    }

    @Override
    public void onBackPressed() {
        if (binding.frameAdoption.getVisibility() == View.VISIBLE) {
            binding.frameAdoption.setVisibility(View.GONE);
            AppUtils.animateFromBottomtoTop(LiveStockDesignActivity.this, binding.frameAdoption, false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.LIVE_STOCK_DESIGN:
                LiveStockDesignRes response = (LiveStockDesignRes) data;
                this.response=response;
                showData(response);
                break;
            case ApiNames.GET_SEARCH_MORE_DETAILS:
                LiveStockDesignRes response1 = (LiveStockDesignRes) data;
                this.response=response1;
                showData(response1);
                break;
            case ApiNames.LIVE_STOCK_ORDER:
                LiveStockOrderResponse liveStockOrderResponse = (LiveStockOrderResponse) data;
                showOrderPopUp(LiveStockDesignActivity.this,liveStockOrderResponse.getData());

                break;
        }
    }
    private void showData(final LiveStockDesignRes response) {
             if (response != null) {
            if (response.getData() != null && response.getData().size() > 0) {
                switchVisibility(0);
                adapter = new LiveStockDesignAdapter(this, response.getData(), new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {
                        LiveStockDesignRes.Datum liDatum= (LiveStockDesignRes.Datum) Item;
                        presenter.getLiveStockOrder(liDatum.getDesignID(), getPreferenceInstance().getIntValue(COMPANY_ID,0),isConnected);
                        //showOrderPopUp(LiveStockDesignActivity.this,"");
                    }
                });
                binding.rvFeeds.setAdapter(adapter);

            }else{
                switchVisibility(3);
            }

        } else {
            switchVisibility(0);
        }

    }

    public void showOrderPopUp(final Activity context, final List<LiveStockOrder> data) {
        {

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            final Dialog mDialog = new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final CustomDialogOrderLayoutBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.custom_dialog_order_layout, null, false);
            mDialog.setContentView(binding.getRoot());
            lp.copyFrom(mDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            mDialog.getWindow().setAttributes(lp);
            binding.tvTotalWt.setVisibility(View.GONE);

            setUpRecyclerView(binding.recyclerCommon,new LinearLayoutManager(this));
             orderAdapter=new OrderAdapter(this, data,new OnItemClickListener() {
                @Override
                public void onClick(Object Item, View v) {

                }
            });
            binding.recyclerCommon.setAdapter(orderAdapter);
            mDialog.show();
            binding.tvCancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                }
            });
            binding.tvSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<LiveStockOrder> orders=new ArrayList<>();
                    for(int i=0;i< orderAdapter.getData().size();i++)
                    {
                        if(orderAdapter.getData().get(i).getQty()!=null && orderAdapter.getData().get(i).getQty()!=0)
                        {
                            orders.add(orderAdapter.getData().get(i));
                        }
                    }
                    DatabaseInitUtil.addOrderListAsyncTask(appDatabase,orders,false);
                    mDialog.dismiss();
                }
            });


        }


    }

    @Override
    public void switchVisibility(int type) {
        binding.swipe.setRefreshing(false);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        switch (type) {
            case 0:
                binding.emptyLayout.linearEmpty.setVisibility(View.GONE);
                binding.progressLayout.progressBar.setVisibility(View.GONE);
                binding.swipe.setVisibility(View.VISIBLE);
                break;

            case 1:
                binding.emptyLayout.linearEmpty.setVisibility(View.VISIBLE);
                binding.swipe.setVisibility(View.GONE);
                break;

            case 2:
                binding.emptyLayout.linearEmpty.setVisibility(View.VISIBLE);
                binding.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.emptyLayout.textEmpty.setText(getString(R.string.no_internet));
                binding.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.swipe.setVisibility(View.GONE);
                break;
            case 3:
                binding.emptyLayout.linearEmpty.setVisibility(View.VISIBLE);
                binding.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.emptyLayout.textEmpty.setText(getString(R.string.no_match_found));
                binding.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.swipe.setVisibility(View.GONE);
                break;
            case 4:
                binding.emptyLayout.linearEmpty.setVisibility(View.GONE);
                binding.swipe.setVisibility(View.GONE);
                binding.progressLayout.progressBar.setVisibility(View.VISIBLE);
                break;

        }
    }


}
