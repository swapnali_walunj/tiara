package com.tiara.ui.activity;

import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityImagePagerBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.SizeAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.fragment.ImageFragment;
import com.tiara.ui.model.Response.SizeResponse;
import com.tiara.ui.model.Response.ViewMoreDesignResponse;
import com.tiara.utils.Constants;

import java.util.ArrayList;

public class ImagePagerActivity extends BaseActivity {

    ArrayList<ViewMoreDesignResponse.Datum> list;
    int position=0;
    private ActivityImagePagerBinding binding;
    CommonPresenterImpl presenter;
    int companyid,designid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_pager);
        presenter=new CommonPresenterImpl(this);
        companyid=getIntent().getIntExtra("companyid",0);
        designid=getIntent().getIntExtra("designid",0);
        if(getIntent().getBooleanExtra("isMore",false)) {
            presenter.getViewMoreDesign(companyid, designid, isConnected);
        }else{
            list=new ArrayList<>();
            list.add(new ViewMoreDesignResponse.Datum(0,"",0,"",getIntent().getStringExtra("imagePath"),""));
            position = getIntent().getIntExtra(Constants.POSITION_KEY,0);

            if(list !=null) {
                binding.classviewPager.setDrawingCacheEnabled(true);
                binding.classviewPager.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                binding.classviewPager.setAdapter(new ProductPageAdapter(
                        getSupportFragmentManager(), list));
            }
            binding.classviewPager.setCurrentItem(position);
        }



    }
    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.GET_MORE_DETAILS:
                ViewMoreDesignResponse viewMoreDesignResponse= (ViewMoreDesignResponse) data;
                list = (ArrayList<ViewMoreDesignResponse.Datum>) viewMoreDesignResponse.getData();
                position = getIntent().getIntExtra(Constants.POSITION_KEY,0);

                if(list !=null) {
                    binding.classviewPager.setDrawingCacheEnabled(true);
                    binding.classviewPager.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                    binding.classviewPager.setAdapter(new ProductPageAdapter(
                            getSupportFragmentManager(), list));
                }
                binding.classviewPager.setCurrentItem(position);
                break;
        }
    }



    private class ProductPageAdapter extends FragmentPagerAdapter {
        ArrayList<ViewMoreDesignResponse.Datum> list;

        public ProductPageAdapter(FragmentManager fm, ArrayList<ViewMoreDesignResponse.Datum> list) {
            super(fm);
            this.list = list;
        }

        @Override
        public Fragment getItem(int pos) {
            return new ImageFragment().newInstance(list.get(pos).getImagePath(), pos, getCount());
        }

        @Override
        public int getCount() {
            return list.size();
        }

    }
}