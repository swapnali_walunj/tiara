package com.tiara.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityOldPendingOrderBinding;
import com.tiara.databinding.ActivityPendingOrderBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.OldPendingOrderAdapter;
import com.tiara.ui.adapter.PendingOrderAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.fragment.FilterFragment;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.NavigationModel;
import com.tiara.ui.model.Response.OldPendingOrderResponse;
import com.tiara.ui.model.Response.PendingOrderResponse;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.COMPANY_ID;
import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.LOAD_MORE_DELAY;

public class OldPendingOrderActivity extends BaseActivity implements OnLoadMoreListener {
    ActivityOldPendingOrderBinding binding;
    ArrayList<NavigationModel> navigationModels;
    FilterFragment mFilterFragment1;
    FragmentManager mFragmentManager;
    CommonPresenterImpl presenter;
    OldPendingOrderAdapter adapter;
    List<OldPendingOrderResponse.Datum> datalist=new ArrayList<>();
    boolean isLoadMore = false;
    int offset=0;
    OldPendingOrderResponse response;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_old_pending_order, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        binding.commonToolbarLayout.toolbarTitle.setText("Old Pending Order");
        presenter=new CommonPresenterImpl(this);
        mFragmentManager = getSupportFragmentManager();
        layoutManager = new LinearLayoutManager(this);
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            customNavigationViewAdapter.showSelected(7, false, -1);
        }else{
            customNavigationViewAdapter.showSelected(6, false, -1);
        }
        setUpRecyclerView(binding.fragmentRecyclerCommon.recyclerCommon,new LinearLayoutManager(this));
        callApi(0);


        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.swipe.setRefreshing(false);
                    }
                }, 2000);
            }
        });


    }
    public void callApi(int offset)
    {
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            presenter.getOldPendingOrder(getCustUserId(), getPreferenceInstance().getIntValue(COMPANY_ID, 0), offset, isConnected);
        }else{
            presenter.getOldPendingOrdersaleman(getCustUserId(), getPreferenceInstance().getIntValue(COMPANY_ID, 0), offset, isConnected);
        }

    }






    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.OLD_PENDING_ORDER:
                OldPendingOrderResponse response = (OldPendingOrderResponse) data;
                this.response=response;
                showData(response);
                break;
            case ApiNames.OLD_PENDING_ORDER_SALESMAN:
                OldPendingOrderResponse response1 = (OldPendingOrderResponse) data;
                this.response=response1;
                showData(response1);
                break;
        }
    }
    private void showData(final OldPendingOrderResponse response) {
        if (response != null) {
            if (datalist != null && datalist.size() == 0 && response.getData() != null && response.getData().size() == 0) {
                /*adapter = new OldPendingOrderAdapter(this, datalist, new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {
                        OldPendingOrderResponse.Datum datum =(OldPendingOrderResponse.Datum)Item;
                        Intent intent=new Intent(OldPendingOrderActivity.this,OldPendingOrderDetailsActivity.class);
                        intent.putExtra("id",datum.getID());
                        startActivity(intent);
                    }
                });
                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);*/
                switchVisibility(1);
            } else {
                if (!isLoadMore) {
                    datalist = response.getData();
                    if (adapter == null) {
                        adapter = new OldPendingOrderAdapter(this, datalist, new OnItemClickListener() {
                            @Override
                            public void onClick(Object Item, View v) {
                                OldPendingOrderResponse.Datum datum =(OldPendingOrderResponse.Datum)Item;
                                Intent intent=new Intent(OldPendingOrderActivity.this,OldPendingOrderDetailsActivity.class);
                                intent.putExtra("id",datum.getID());
                                startActivity(intent);
                            }
                        });
                        binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                    } else {
                        adapter.refresh(datalist);
                        adapter.setLoaded();
                    }
                    adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                    adapter.setOnLoadMoreListener(OldPendingOrderActivity.this);
                    switchVisibility(0);

                    if (datalist != null && datalist.size() == 0) {
                        switchVisibility(0);
                    }

                } else {
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() > 0) {
                                datalist.remove(datalist.size() - 1);
                            }
                            datalist.addAll(response.getData());
                            if (response.getData().size() == 0) {

                            }
                            isLoadMore = false;
                            if (adapter == null) {
                                adapter = new OldPendingOrderAdapter(OldPendingOrderActivity.this, datalist, new OnItemClickListener() {
                                    @Override
                                    public void onClick(Object Item, View v) {
                                        OldPendingOrderResponse.Datum datum =(OldPendingOrderResponse.Datum)Item;
                                        Intent intent=new Intent(OldPendingOrderActivity.this,OldPendingOrderDetailsActivity.class);
                                        intent.putExtra("id",datum.getID());
                                        startActivity(intent);
                                    }
                                });
                                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                            } else {
                                adapter.refresh(datalist);
                                adapter.setLoaded();
                            }
                            adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                            adapter.setOnLoadMoreListener(OldPendingOrderActivity.this);
                            switchVisibility(0);
                        }
                    }, LOAD_MORE_DELAY);
                }
            }

        } else {
            switchVisibility(0);
        }

    }

    @Override
    public void switchVisibility(int type) {
        binding.swipe.setRefreshing(false);
        binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
        switch (type) {
            case 0:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.VISIBLE);
                break;

            case 1:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.emptyLayout.linearEmpty.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;

            case 2:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.fragmentRecyclerCommon.emptyLayout.textEmpty.setText(getString(R.string.no_internet));
                binding.fragmentRecyclerCommon.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 3:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 4:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void onLoadMore(String type) {
        //if(!isLoadMore) {
        isLoadMore = true;
        OldPendingOrderResponse.Datum dataItem = new OldPendingOrderResponse.Datum();
        dataItem.setCustName("Loading");
        datalist.add(dataItem);
        adapter.refresh(datalist);
        offset=offset+1;
        callApi(offset);
        // }


    }
}
