package com.tiara.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.tiara.R;
import com.tiara.databinding.ActivityDashBoardBinding;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.adapter.NavDashBoardPagerAdapter;
import com.tiara.ui.helper.NavDashBoardPagerList;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.utils.AppUtils;
import com.tiara.utils.Constants;

import java.util.ArrayList;

public class DashBoardActivity extends BaseActivity {

    ActivityDashBoardBinding binding;
    ArrayList<NavDashBoardPagerList> navDashBoardPagerLists;
    NavDashBoardPagerAdapter navDashBoardPagerAdapter;
    public String moduleData = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_dash_board, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        customNavigationViewAdapter.showSelected(0, false,-1);
        //customNavigationViewAdapter.showSelected(0, false,0);

        //mCommonPresenter = new CommonPresenterImpl(this);

        navDashBoardPagerLists = new ArrayList<>();
        navDashBoardPagerLists.add(new NavDashBoardPagerList("Live Stock", "100", R.drawable.logog));
        navDashBoardPagerLists.add(new NavDashBoardPagerList("Catalog",         "100", R.drawable.logog));
        navDashBoardPagerLists.add(new NavDashBoardPagerList("Pending Order", "100", R.drawable.logog));
        navDashBoardPagerLists.add(new NavDashBoardPagerList("Old Pending Order", "100", R.drawable.logog));





        initDashBoardToolbar();
        binding.commonToolbarLayout.toolbar.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.searchToolbar.searchtoolbar.setVisibility(View.GONE);
        setInitToolbar();

        binding.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashBoardActivity.this, LiveStockActivity.class));
            }
        });

        binding.cardview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashBoardActivity.this, CatelogActivity.class));
            }
        });
        binding.cardview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(DashBoardActivity.this, PendingOrderActivity.class));
            }
        });
        binding.cardview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashBoardActivity.this, OldPendingOrderActivity.class));
            }
        });





    }



    private void initDashBoardToolbar() {
        binding.tvUserName.setText("Welcome "+getPreferenceInstance().getValue(Constants.USER_NAME));
        binding.tvUserName.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.rlHeading.setVisibility(View.GONE);
        binding.commonToolbarLayout.rlHeadingSort.setVisibility(View.GONE);


    }

    @Override
    public void onBackPressed() {
        AppUtils.showAlertDialog("Exit", "Are you sure you want to Exit ?", "Yes", "No", DashBoardActivity.this, new OnItemClickListener() {
            @Override
            public void onClick(Object Item, View v) {
                //getlogout();
                System.exit(2);
            }
        });
    }

    private void setInitToolbar() {

        binding.toolbarLayout.setTitle(" ");
    }








    @Override
    public void onResponseSuccess(Object data, String apiName) {
            super.onResponseSuccess(data, apiName);
    }

    @Override
    public void onResponseSuccess(Object data, String apiName, int position, String grppostion) {

    }

    @Override
    public void onResponseFailure(String msg, String apiName, int position, String grppostion) {

    }

    @Override
    public void onResponseSuccess(Object data, String apiName, String searchKey) {
        super.onResponseSuccess(data, apiName, searchKey);

    }



    @Override
    public void onResume() {
        super.onResume();
    }




}