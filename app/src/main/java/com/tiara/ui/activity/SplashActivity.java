package com.tiara.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.tiara.R;
import com.tiara.databinding.ActivitySplashBinding;
import com.tiara.ui.base.BaseActivity;

import static com.tiara.utils.Constants.USER_ID;
import static com.tiara.utils.Constants.USER_NAME;


public class SplashActivity extends BaseActivity implements Animation.AnimationListener{
    Animation splashAnim;
    int SPLASH_SCREEN_TIMEOUT = 3000;
    ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_splash);
        splashAnim = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        splashAnim.setAnimationListener(SplashActivity.this);
        binding.llSplash.startAnimation(splashAnim);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

        if (animation == splashAnim) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                    overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
                   if(TextUtils.isEmpty(getPreferenceInstance().getValue(USER_NAME))) {
                       startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                   }else{
                       startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                       startActivity(new Intent(SplashActivity.this, LiveStockActivity.class));
                   }

                }
            }, SPLASH_SCREEN_TIMEOUT);
        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
