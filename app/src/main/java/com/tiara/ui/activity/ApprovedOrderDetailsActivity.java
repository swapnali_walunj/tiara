package com.tiara.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityApprovedOrderDetailBinding;
import com.tiara.databinding.ActivityPendingOrderDetailsBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.ApprovedOrderDetailsAdapter;
import com.tiara.ui.adapter.PendingOrderDetailsAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.ApprovedOrderDetails;
import com.tiara.ui.model.Response.PendingOrderDetails;

import java.text.DecimalFormat;

public class ApprovedOrderDetailsActivity extends BaseActivity {

    ActivityApprovedOrderDetailBinding binding;
    CommonPresenterImpl presenter;
    double granttotal=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_approved_order_detail);
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.GONE);
        binding.commonToolbarLayout.toolbarTitle.setText("View Approved Order Details");
        presenter = new CommonPresenterImpl(this);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        configureToolbar(binding.commonToolbarLayout.toolbar, R.drawable.abc_ic_ab_back_material_new, ContextCompat.getColor(this, R.color.white));
        setUpRecyclerView(binding.rvCart, new LinearLayoutManager(this));
        presenter.getApprovedOrderDetails(isConnected,getIntent().getIntExtra("id",0));
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.APPROVED_ORDER_DETAILS:
                ApprovedOrderDetails pendingOrderDetails=(ApprovedOrderDetails)data;
                setData(pendingOrderDetails);
                break;
        }
    }

    private void setData(ApprovedOrderDetails pendingOrderDetails) {
        binding.edittextCustomerName.setText(pendingOrderDetails.getData().get(0).getCustName());
        binding.edittextAddress.setText(pendingOrderDetails.getData().get(0).getAddressAddress());
        binding.edittextDate.setText(pendingOrderDetails.getData().get(0).getSODate());
        binding.edittextRate.setVisibility(View.GONE);

        for(int i=0;i< pendingOrderDetails.getData().size();i++)
        {
            granttotal= granttotal+pendingOrderDetails.getData().get(i).getLbltotalamt();
        }
        binding.tvGrantTotal.setText(" ₹ "+new DecimalFormat("##.##").format(granttotal)+"");

        ApprovedOrderDetailsAdapter pendingOrderDetailsAdapter=new ApprovedOrderDetailsAdapter(this, pendingOrderDetails.getData(), new OnItemClickListener() {
            @Override
            public void onClick(Object Item, View v) {

            }
        });
        binding.rvCart.setAdapter(pendingOrderDetailsAdapter);


    }
}
