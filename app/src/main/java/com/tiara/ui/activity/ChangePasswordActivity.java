package com.tiara.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityChangePasswordBinding;
import com.tiara.db.DatabaseInitUtil;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.base.BaseActivity;
import com.tiara.utils.AppUtils;

import static com.tiara.utils.Constants.IS_CUSTOMER;

public class ChangePasswordActivity extends BaseActivity {
    ActivityChangePasswordBinding binding;
    CommonPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_change_password, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        binding.commonToolbarLayout.toolbarTitle.setText("Change Password");
        presenter = new CommonPresenterImpl(this);
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            customNavigationViewAdapter.showSelected(8, false, -1);
        }else{
            customNavigationViewAdapter.showSelected(7, false, -1);
        }

        binding.tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(isValid())
            {
                presenter.getChangePassword(getCustUserId(),binding.edittextNew.getText().toString(),isConnected);
            }
            }
        });
        binding.tvCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              finish();
            }
        });
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.CHANGE_PASSWORD:
                AppUtils.toastMe(ChangePasswordActivity.this,"Password Changed successfully");
                getPreferenceInstance().clear();
                DatabaseInitUtil.deleteAllTables(appDatabase);
                Intent in = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
                finish();
                break;
        }
    }

    boolean isValid()
    {
        boolean isValid=true;
        if(TextUtils.isEmpty(binding.edittextOld.getText().toString()))
        {
            AppUtils.toastMe(ChangePasswordActivity.this,"Please enter old password");
            isValid=false;
        }
        if(TextUtils.isEmpty(binding.edittextNew.getText().toString()))
        {
            AppUtils.toastMe(ChangePasswordActivity.this,"Please enter new password");
            isValid=false;
        }
        if(TextUtils.isEmpty(binding.edittextNewRenter.getText().toString()))
        {
            AppUtils.toastMe(ChangePasswordActivity.this,"Please re-enter new password");
            isValid=false;
        }
        if(!binding.edittextNew.getText().toString().equalsIgnoreCase(binding.edittextNewRenter.getText().toString()))
        {
            AppUtils.toastMe(ChangePasswordActivity.this,"New password and re-enter password should be same");
            isValid=false;
        }
        return isValid;
    }
}
