package com.tiara.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityLoginBinding;
import com.tiara.mvp.PresenterImpl.LoginPresenterImpl;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.base.NavigationDrawerActivity;
import com.tiara.ui.model.Response.CustomerLoginRes;
import com.tiara.ui.model.Response.SalesManLoginRes;

import static com.tiara.utils.Constants.COMPANY_ID;
import static com.tiara.utils.Constants.COMPANY_NAME;
import static com.tiara.utils.Constants.CUSTOMER_RESPONSE;
import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.MOBILE_NO;
import static com.tiara.utils.Constants.SALES_RESPONSE;
import static com.tiara.utils.Constants.USER_ID;
import static com.tiara.utils.Constants.USER_NAME;
import static com.tiara.utils.Constants.USER_PIC;

public class LoginActivity extends BaseActivity {
    ActivityLoginBinding binding;
    LoginPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_login);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_login);
        presenter = new LoginPresenterImpl(this);
        binding.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(presenter.validateRegistration(binding.edittextEmail.getText().toString(),binding.edittextPassword.getText().toString())) {
                   if (binding.spType.getSelectedItemPosition() == 0) {
                       presenter.getLoginCustomer(isConnected, binding.edittextEmail.getText().toString(), binding.edittextPassword.getText().toString());
                   } else {
                       presenter.getLoginSalesMan(isConnected, binding.edittextEmail.getText().toString(), binding.edittextPassword.getText().toString());
                   }
               }
            }
        });
    }
    @Override
    public void onResponseSuccess(Object data, String apiName) {
        super.onResponseSuccess(data, apiName);
        switch (apiName) {
            case ApiNames.SALES_LOGIN:
                SalesManLoginRes response = (SalesManLoginRes) data;
                getPreferenceInstance().setValue(CUSTOMER_RESPONSE,"");
                getPreferenceInstance().setValue(IS_CUSTOMER,false);
                getPreferenceInstance().setValue(SALES_RESPONSE,true);
                getPreferenceInstance().setValue(USER_ID,response.getData().get(0).getSalesPID());
                getPreferenceInstance().setValue(COMPANY_ID,response.getData().get(0).getCompanyID());
                getPreferenceInstance().setValue(COMPANY_NAME,response.getData().get(0).getCompanyName());
                getPreferenceInstance().setValue(USER_NAME,response.getData().get(0).getSalesPName());
                getPreferenceInstance().setValue(SALES_RESPONSE, new Gson().toJson(response));
                startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                startActivity(new Intent(LoginActivity.this, LiveStockActivity.class));
                finish();
                break;
            case ApiNames.CUSTOMER_LOGIN:
                CustomerLoginRes response1 = (CustomerLoginRes) data;
                getPreferenceInstance().setValue(CUSTOMER_RESPONSE,new Gson().toJson(response1));
                getPreferenceInstance().setValue(USER_ID,response1.getData().get(0).getCustID());
                getPreferenceInstance().setValue(COMPANY_ID,response1.getData().get(0).getCompanyID());
                getPreferenceInstance().setValue(MOBILE_NO,response1.getData().get(0).getCustMobile());
                getPreferenceInstance().setValue(USER_NAME,response1.getData().get(0).getCustName());
                getPreferenceInstance().setValue(USER_PIC,response1.getData().get(0).getCustLogo());
                getPreferenceInstance().setValue(SALES_RESPONSE,false);
                getPreferenceInstance().setValue(IS_CUSTOMER,true);
                startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                startActivity(new Intent(LoginActivity.this, LiveStockActivity.class));
                finish();
                break;
        }
    }
}
