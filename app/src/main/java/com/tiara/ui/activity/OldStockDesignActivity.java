package com.tiara.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.jakewharton.rxbinding.view.RxView;
import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityLiveStockDesignBinding;
import com.tiara.databinding.CustomDialogOrderLayoutBinding;
import com.tiara.db.DatabaseInitUtil;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.CatalogOrderAdapter;
import com.tiara.ui.adapter.OldStockDesignAdapter;
import com.tiara.ui.adapter.OrderAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.fragment.FilterFragment;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.NavigationModel;
import com.tiara.ui.model.Response.FilterCopy;
import com.tiara.ui.model.Response.LiveStockDesignRes;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.LiveStockOrderResponse;
import com.tiara.ui.model.Response.OldStockOrder;
import com.tiara.ui.model.Response.OldStockOrderResponse;
import com.tiara.utils.AppUtils;
import com.tiara.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

import static com.tiara.utils.Constants.COMPANY_ID;
import static com.tiara.utils.Constants.IS_CUSTOMER;

public class OldStockDesignActivity extends BaseActivity  {
    ActivityLiveStockDesignBinding binding;
    ArrayList<NavigationModel> navigationModels;
    FilterFragment mFilterFragment1;
    FragmentManager mFragmentManager;
    CommonPresenterImpl presenter;
    OldStockDesignAdapter adapter;
    List<LiveStockDesignRes.Datum> datalist=new ArrayList<>();
    boolean isLoadMore = false;
    int offset=0;
    LiveStockDesignRes response;
    LinearLayoutManager layoutManager;
    int designid=0;
    FilterCopy filterCopy;
     CatalogOrderAdapter orderAdapter;
    private LiveData<List<OldStockOrder>> mObservableProduct;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  DataBindingUtil.setContentView(this, R.layout.activity_live_stock_design);
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarTitle.setText("Old Stock Design");
        configureToolbar(binding.commonToolbarLayout.toolbar, R.drawable.abc_ic_ab_back_material_new, ContextCompat.getColor(this, R.color.white));
        designid=getIntent().getIntExtra("designid",0);
        binding.fab.setVisibility(View.GONE);
        presenter=new CommonPresenterImpl(this);
        mFragmentManager = getSupportFragmentManager();
       // layoutManager = new LinearLayoutManager(this);
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            customNavigationViewAdapter.showSelected(4, false, -1);
        }else{
            customNavigationViewAdapter.showSelected(3, false, -1);
        }
         setUpRecyclerView(binding.rvFeeds,new LinearLayoutManager(this));
         getOfflineOrderListing();
         callApi();

        binding.commonToolbarLayout.toolbarImageSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OldStockDesignActivity.this,OldViewCartActivity.class));
            }
        });

        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.swipe.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        RxView.clicks(binding.fab).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {

                if (mFragmentManager.getBackStackEntryCount() == 0) {
                    mFilterFragment1 = new FilterFragment();
                    FragmentTransaction ft = mFragmentManager.beginTransaction();
                    ft.add(binding.frameAdoption.getId(), mFilterFragment1);
                    ft.addToBackStack(Constants.FILTER);
                    ft.commit();
                }
                binding.frameAdoption.setVisibility(View.VISIBLE);
                AppUtils.animateFromBottomtoTop(OldStockDesignActivity.this, binding.frameAdoption, true);

           }
        });
    }
    public void callApi()
    {
        presenter.getCatalogDesign(designid,getPreferenceInstance().getIntValue(COMPANY_ID,0),isConnected);

    }

    @Override
    public void onBackPressed() {
        if (binding.frameAdoption.getVisibility() == View.VISIBLE) {
            binding.frameAdoption.setVisibility(View.GONE);
            AppUtils.animateFromBottomtoTop(OldStockDesignActivity.this, binding.frameAdoption, false);
        } else {
            super.onBackPressed();
        }
    }

    synchronized void getOfflineOrderListing() {
        mObservableProduct = appDatabase.catalogOrderListingDao().loadOrderList();
        mObservableProduct.observe(this, new Observer<List<OldStockOrder>>() {
            @Override
            public void onChanged(@Nullable List<OldStockOrder> itelist) {
                if (itelist != null && itelist.size() > 0) {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.VISIBLE);
                    binding.commonToolbarLayout.tvCount.setText(itelist.size()+"");
                } else {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.GONE);
                }



            }
        });
    }
    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.CATALOG_DESIGN:
                LiveStockDesignRes response = (LiveStockDesignRes) data;
                this.response=response;
                showData(response);
                break;
            case ApiNames.OLD_STOCK_ORDER:
                OldStockOrderResponse liveStockOrderResponse = (OldStockOrderResponse) data;
                showOrderPopUp(OldStockDesignActivity.this,liveStockOrderResponse.getData());

                break;

        }
    }
    private void showData(final LiveStockDesignRes response) {
             if (response != null) {
            if (response.getData() != null && response.getData().size() > 0) {
                switchVisibility(0);
                adapter = new OldStockDesignAdapter(this, response.getData(),designid, new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {
                        //showOrderPopUp(OldStockDesignActivity.this,"");
                        LiveStockDesignRes.Datum liDatum= (LiveStockDesignRes.Datum) Item;
                        presenter.getOldStockOrder(liDatum.getDesignID(), getPreferenceInstance().getIntValue(COMPANY_ID,0),isConnected);
                    }
                });
                binding.rvFeeds.setAdapter(adapter);

            }

        } else {
            switchVisibility(0);
        }

    }

    public void showOrderPopUp(final Activity context,final List<OldStockOrder> data) {
        {

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            final Dialog mDialog = new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            final CustomDialogOrderLayoutBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.custom_dialog_order_layout, null, false);
            mDialog.setContentView(binding.getRoot());
            lp.copyFrom(mDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            mDialog.getWindow().setAttributes(lp);
            binding.tvTitle.setText("Order Request");
            binding.tvStock.setVisibility(View.GONE);
            binding.vStock.setVisibility(View.GONE);
            setUpRecyclerView(binding.recyclerCommon,new LinearLayoutManager(this));
            orderAdapter=new CatalogOrderAdapter(this, data,new OnItemClickListener() {
                @Override
                public void onClick(Object Item, View v) {

                }
            });
            binding.recyclerCommon.setAdapter(orderAdapter);
            mDialog.show();
            binding.tvCancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog.dismiss();
                }
            });
            binding.tvSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<OldStockOrder> orders=new ArrayList<>();
                    for(int i=0;i< orderAdapter.getData().size();i++)
                    {
                        if(orderAdapter.getData().get(i).getQty()!=null && orderAdapter.getData().get(i).getQty()!=0)
                        {
                            orders.add(orderAdapter.getData().get(i));
                        }
                    }
                    DatabaseInitUtil.addCatalogOrderListAsyncTask(appDatabase,orders,false);
                    mDialog.dismiss();
                }
            });


        }


    }

    @Override
    public void switchVisibility(int type) {
        binding.swipe.setRefreshing(false);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        switch (type) {
            case 0:
                binding.emptyLayout.container.setVisibility(View.GONE);
                binding.progressLayout.progressBar.setVisibility(View.GONE);
                binding.rvFeeds.setVisibility(View.VISIBLE);
                break;

            case 1:
                binding.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.rvFeeds.setVisibility(View.GONE);
                break;

            case 2:
                binding.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.emptyLayout.textEmpty.setText(getString(R.string.no_internet));
                binding.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.rvFeeds.setVisibility(View.GONE);
                break;
            case 3:
                binding.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.rvFeeds.setVisibility(View.GONE);
                break;
            case 4:
                binding.emptyLayout.container.setVisibility(View.GONE);
                binding.rvFeeds.setVisibility(View.GONE);
                binding.progressLayout.progressBar.setVisibility(View.VISIBLE);
                break;

        }
    }


}
