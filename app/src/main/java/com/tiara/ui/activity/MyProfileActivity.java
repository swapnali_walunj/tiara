package com.tiara.ui.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityMyProfileBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.Interface.SetImagePreviewInterface;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.fragment.CameraAndGalleryBottomSheetDialogFragment;
import com.tiara.ui.model.Response.LabourChargeResponse;
import com.tiara.ui.model.Response.ProfileResponse;
import com.tiara.utils.AppUtils;
import com.tiara.utils.ImageOrientationChecker;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MyProfileActivity extends BaseActivity implements SetImagePreviewInterface {
    private static final String POST_FIELD ="POST_FIELD" ;
    ActivityMyProfileBinding binding;
    public static final String TEMP_PHOTO_FILE_NAME = "Image.jpg";
    public static final int REQUEST_CODE_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE = 0x3;
    protected Dialog dialog;
    String base64;
    File uploadfile;
    CommonPresenterImpl presenter;
    ProfileResponse response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_my_profile, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        customNavigationViewAdapter.showSelected(1, false,-1);
        binding.commonToolbarLayout.toolbarTitle.setText("My Profile");
        presenter = new CommonPresenterImpl(this);

       showProgressDialog();
       presenter.getProfileDetails(isConnected,getCustUserId());
       binding.tvCancle.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               finish();
           }
       });
       binding.tvUpdate.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               new AsyncTask<Void, Void, Void>() {
                   @Override
                   protected Void doInBackground(Void... voids) {
                       uploadFile(uploadfile);
                       return null;
                   }
               }.execute();

              presenter.updateProfile(isConnected,response.getData().get(0));
           }
       });
        binding.linearImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetDialogFragment bottomSheetDialogFragment = (BottomSheetDialogFragment) CameraAndGalleryBottomSheetDialogFragment.newInstance(MyProfileActivity.this);
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag()); }
        });
    }
    @Override
    public void onResponseSuccess(Object data, String apiName) {
        super.onResponseSuccess(data, apiName);
        switch (apiName) {
            case ApiNames.PROFILE:
                 response = (ProfileResponse) data;
                hideProgressDialog();
                showData(response);
                break;
        }
    }
    public void showData(ProfileResponse response)
    {
        if(response!=null) {
            binding.edittextMobile.setText(response.getData().get(0).getCustMobile());
            binding.edittextCompany.setText(response.getData().get(0).getCustName());
            binding.edittextEmail.setText(response.getData().get(0).getCustEmail());
            binding.edittextContact.setText(response.getData().get(0).getCustContPerson());
            binding.edittextGst.setText(response.getData().get(0).getCustGSTNo());
            binding.edittextState.setText(response.getData().get(0).getState_Name());
            binding.edittextApprove.setText(response.getData().get(0).getCustApprovEmail());

        }
    }

    @Override
    public void getImagePreviewCamera(File file) {
        try {
            Bitmap originalBitmap = BitmapFactory.decodeFile(file.getPath());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPurgeable = true;
            final int maxSize = 1080;
            int outWidth;
            int outHeight;
            int inWidth = originalBitmap.getWidth();
            int inHeight = originalBitmap.getHeight();
            if (inWidth > inHeight) {
                outWidth = maxSize;
                outHeight = (inHeight * maxSize) / inWidth;
            } else {
                outHeight = maxSize;
                outWidth = (inWidth * maxSize) / inHeight;
            }
            Bitmap processedImage = Bitmap.createScaledBitmap(originalBitmap, outWidth, outHeight, true);
            Bitmap orientedImage = ImageOrientationChecker.changeOrientation(file.getPath(), processedImage);

            processedImage = orientedImage;

            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            processedImage.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] bitmapdata = bos.toByteArray();


            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();


            base64 = Base64.encodeToString(bitmapdata, Base64.NO_WRAP);
            binding.imageAdd.setImageBitmap(orientedImage);
            uploadfile=file;
            binding.llAddPhoto.setVisibility(View.GONE);
            AppUtils.logMe("base64", base64);
        } catch (Exception e) {
            AppUtils.logMe("Exception camera", e.toString());
        }
    }

    @SuppressLint("LongLogTag")
    private String uploadFile(File file) {
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //final File sourceFile = new File(file);
        String serverResponseMessage = null;
        String responce = null;
        if (!file.isFile()) {

           /* dialog.dismiss();*/

            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(), "File not found !", Toast.LENGTH_LONG).show();
                }
            });

            return "no file";
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(file.getPath());
                URL url = new URL("https://b2b.tiarasilver.com/Image/Customer/");
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty(POST_FIELD, file.getName());
                dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"" + POST_FIELD + "\";filename="
                        + file.getName() + lineEnd);
                dos.writeBytes(lineEnd);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                int serverResponseCode = conn.getResponseCode();
                serverResponseMessage = conn.getResponseMessage();
                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);
                if (serverResponseCode <= 200) {

                    runOnUiThread(new Runnable() {
                        public void run() {

                            Toast.makeText(MyProfileActivity.this, "File Upload Complete.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                fileInputStream.close();
                dos.flush();
                dos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (MalformedURLException ex) {
               // dialog.dismiss();
                ex.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {

                        Toast.makeText(MyProfileActivity.this, "MalformedURLException",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (IOException e) {
               // dialog.dismiss();
                e.printStackTrace();

                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(MyProfileActivity.this, "Got Exception : see logcat ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Upload file to server Exception", "Exception : "
                        + e.getMessage(), e);
            }
        }
        //dialog.dismiss();
        return responce;
    }

}
