package com.tiara.ui.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityMyCustomerBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.mvp.View.SearchProcessView;
import com.tiara.ui.adapter.MyCustomerAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.NavigationModel;
import com.tiara.ui.model.Response.MyCustomerResponse;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.LOAD_MORE_DELAY;

public class MyCustomerActivity extends BaseActivity implements OnLoadMoreListener {
    ActivityMyCustomerBinding binding;
    ArrayList<NavigationModel> navigationModels;
    CommonPresenterImpl presenter;
    List<MyCustomerResponse.Datum> datalist=new ArrayList<>();
    boolean isLoadMore = false;
    int offset=0;
    MyCustomerAdapter adapter;
    MyCustomerResponse response;
    LinearLayoutManager layoutManager;

    boolean searchApplied = false;
    String searchFor = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_my_customer, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        binding.commonToolbarLayout.toolbarTitle.setText("My Customer");
        presenter=new CommonPresenterImpl(this);
        layoutManager = new LinearLayoutManager(this);
        customNavigationViewAdapter.showSelected(1, false,-1);
        setUpRecyclerView(binding.fragmentRecyclerCommon.recyclerCommon,new LinearLayoutManager(this));
        callApi(0);

       /* binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarImageSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_dark_gray));
        binding.commonToolbarLayout.toolbarImageSearch.setVisibility(View.VISIBLE);*/



    }
    public void callApi(int offset)
    {
        presenter.getMyCustomer(getCustUserId(),offset,isConnected);

    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.MY_CUSTOMER:
                MyCustomerResponse response = (MyCustomerResponse) data;
                this.response=response;
                showData(response);
                break;
        }
    }
    private void showData(final MyCustomerResponse response) {
        if (response != null) {
            if (datalist != null && datalist.size() == 0 && response.getData() != null && response.getData().size() == 0) {
                adapter = new MyCustomerAdapter(this, datalist, new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {

                    }
                });
                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                // switchVisibility(0);
            } else {
                if (!isLoadMore) {
                    datalist = response.getData();
                    if (adapter == null) {
                        adapter = new MyCustomerAdapter(this, datalist, new OnItemClickListener() {
                            @Override
                            public void onClick(Object Item, View v) {

                            }
                        });
                        binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                    } else {
                        adapter.refresh(datalist);
                        adapter.setLoaded();
                    }
                    adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                    adapter.setOnLoadMoreListener(MyCustomerActivity.this);
                    switchVisibility(0);

                    if (datalist != null && datalist.size() == 0) {
                        switchVisibility(0);
                    }

                } else {
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() > 0) {
                                datalist.remove(datalist.size() - 1);
                            }
                            datalist.addAll(response.getData());
                            if (response.getData().size() == 0) {

                            }
                            isLoadMore = false;
                            if (adapter == null) {
                                adapter = new MyCustomerAdapter(MyCustomerActivity.this, datalist, new OnItemClickListener() {
                                    @Override
                                    public void onClick(Object Item, View v) {

                                    }
                                });
                                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                            } else {
                                adapter.refresh(datalist);
                                adapter.setLoaded();
                            }
                            adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                            adapter.setOnLoadMoreListener(MyCustomerActivity.this);
                            switchVisibility(0);
                        }
                    }, LOAD_MORE_DELAY);
                }
            }

        } else {
            switchVisibility(0);
        }

    }

    @Override
    public void switchVisibility(int type) {
        binding.swipe.setRefreshing(false);
        binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
        switch (type) {
            case 0:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.VISIBLE);
                break;

            case 1:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;

            case 2:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.fragmentRecyclerCommon.emptyLayout.textEmpty.setText(getString(R.string.no_internet));
                binding.fragmentRecyclerCommon.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 3:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 4:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.VISIBLE);
                break;

        }
    }


    @Override
    public void onLoadMore(String type) {
        if(!isLoadMore) {
            isLoadMore = true;
            MyCustomerResponse.Datum dataItem = new MyCustomerResponse.Datum();
            dataItem.setCustAddress("Loading");
            datalist.add(dataItem);
            adapter.refresh(datalist);
            offset=offset+1;
            callApi(offset);
        }


    }


}