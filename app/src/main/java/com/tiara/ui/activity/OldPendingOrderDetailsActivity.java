package com.tiara.ui.activity;

import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityOldPendingOrderDetailsBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.OldPendingOrderDetailsAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.OldPendingOrderDetails;

import java.text.DecimalFormat;


public class OldPendingOrderDetailsActivity extends BaseActivity {

    ActivityOldPendingOrderDetailsBinding binding;
    CommonPresenterImpl presenter;
    double granttotal=0.0,totalwt=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_old_pending_order_details);
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.GONE);
        binding.commonToolbarLayout.toolbarTitle.setText("View Old Pending Order Details");
        presenter = new CommonPresenterImpl(this);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        configureToolbar(binding.commonToolbarLayout.toolbar, R.drawable.abc_ic_ab_back_material_new, ContextCompat.getColor(this, R.color.white));
        setUpRecyclerView(binding.rvCart, new LinearLayoutManager(this));
        presenter.getOldPendingOrderDetails(isConnected,getIntent().getIntExtra("id",0));
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.OLD_PENDING_ORDER_DETAILS:
                OldPendingOrderDetails pendingOrderDetails=(OldPendingOrderDetails)data;
                setData(pendingOrderDetails);
                break;
        }
    }

    private void setData(OldPendingOrderDetails pendingOrderDetails) {
        binding.edittextCustomerName.setText(pendingOrderDetails.getData().get(0).getCustName());
        binding.edittextAddress.setText(pendingOrderDetails.getData().get(0).getAddressAddress());
        binding.edittextDate.setText(pendingOrderDetails.getData().get(0).getOldODate());
        //binding.edittextRate.setText(pendingOrderDetails.getData().get(0).()+"");
        for(int i=0;i< pendingOrderDetails.getData().size();i++)
        {
            granttotal= granttotal+pendingOrderDetails.getData().get(i).getTotal();
        }
        binding.tvGrantTotal.setText(" ₹ "+new DecimalFormat("##.##").format(granttotal)+"");
        for(int i=0;i< pendingOrderDetails.getData().size();i++)
        {
            double wt=pendingOrderDetails.getData().get(i).getOldWeight()* pendingOrderDetails.getData().get(i).getOldQty();
            totalwt= totalwt+wt;
        }
        binding.tvTotalWt.setText(new DecimalFormat("##.##").format(totalwt)+"");
        binding.tvDays.setText(pendingOrderDetails.getData().get(0).getOldDays()+"");
        OldPendingOrderDetailsAdapter pendingOrderDetailsAdapter=new OldPendingOrderDetailsAdapter(this, pendingOrderDetails.getData(), new OnItemClickListener() {
            @Override
            public void onClick(Object Item, View v) {

            }
        });
        binding.rvCart.setAdapter(pendingOrderDetailsAdapter);


    }
}