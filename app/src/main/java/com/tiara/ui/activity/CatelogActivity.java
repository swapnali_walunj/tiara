package com.tiara.ui.activity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityCatelogBinding;
import com.tiara.db.DatabaseInitUtil;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.mvp.PresenterImpl.SearchPresenterImpl;
import com.tiara.mvp.View.SearchProcessView;
import com.tiara.ui.adapter.FinalCatalogOrderAdapter;
import com.tiara.ui.adapter.LiveStockAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.fragment.FilterFragment;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.NavigationModel;
import com.tiara.ui.model.Response.LiveStockRes;
import com.tiara.ui.model.Response.OldStockOrder;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.LOAD_MORE_DELAY;

public class CatelogActivity extends BaseActivity implements OnLoadMoreListener, SearchProcessView {
    ActivityCatelogBinding binding;
    ArrayList<NavigationModel> navigationModels;
    FilterFragment mFilterFragment1;
    FragmentManager mFragmentManager;
    CommonPresenterImpl presenter;
    LiveStockAdapter adapter;
    List<LiveStockRes.Datum> datalist=new ArrayList<>();
    boolean isLoadMore = false;
    int offset=0;
    LiveStockRes response;
    LinearLayoutManager layoutManager;
    private LiveData<List<OldStockOrder>> mObservableProduct;
    boolean searchApplied = false;
    String searchFor = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_catelog, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        binding.commonToolbarLayout.toolbarTitle.setText("Catalog");
        presenter=new CommonPresenterImpl(this);
        mFragmentManager = getSupportFragmentManager();
        layoutManager = new LinearLayoutManager(this);
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            customNavigationViewAdapter.showSelected(4, false, -1);
        }else{
            customNavigationViewAdapter.showSelected(3, false, -1);
        }
        setUpRecyclerView(binding.fragmentRecyclerCommon.recyclerCommon,new LinearLayoutManager(this));
        callApi(0);
        getOfflineOrderListing();
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarImageSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_dark_gray));
        binding.commonToolbarLayout.toolbarImageSearch.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarImageSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CatelogActivity.this,OldViewCartActivity.class));
            }
        });
        binding.commonToolbarLayout.toolbarImageSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.commonToolbarLayout.toolbar.setVisibility(View.GONE);
                binding.commonSearchLayout.cardSearchLayout.setVisibility(View.VISIBLE);
                initSearchView();
            }
        });
        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.swipe.setRefreshing(false);
                    }
                }, 2000);
            }
        });


    }
    public void callApi(int offset)
    {
        presenter.getCatelog(getCustUserId(),offset,isConnected);

    }



    synchronized void getOfflineOrderListing() {
        mObservableProduct = appDatabase.catalogOrderListingDao().loadOrderList();
        mObservableProduct.observe(this, new Observer<List<OldStockOrder>>() {
            @Override
            public void onChanged(@Nullable List<OldStockOrder> itelist) {
                if (itelist != null && itelist.size() > 0) {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.VISIBLE);
                    binding.commonToolbarLayout.tvCount.setText(itelist.size()+"");
                    } else {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.GONE);
                    }



            }
        });
    }


    @Override
    public void onBackPressed() {
            super.onBackPressed();

    }
    @Override
    public void onResponseSuccess(Object data, String apiName, String searchKey) {
        switch (apiName) {
            case ApiNames.LIVE_STOCK_SEARCH:
                LiveStockRes response = (LiveStockRes) data;
                this.response=response;
                showData(response);
                break;
        }
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.OLD_STOCK:
                LiveStockRes response = (LiveStockRes) data;
                this.response=response;
                showData(response);
                break;
        }
    }
    private void showData(final LiveStockRes response) {
        if (response != null) {
            if (datalist != null && datalist.size() == 0 && response.getData() != null && response.getData().size() == 0) {
                adapter = new LiveStockAdapter(this, datalist, new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {
                        LiveStockRes.Datum liveStockRes= (LiveStockRes.Datum) Item;
                        Intent i=new Intent(CatelogActivity.this,OldStockDesignActivity.class);
                        i.putExtra("designid",liveStockRes.getID());
                        startActivity(i);
                    }
                });
                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                // switchVisibility(0);
            } else {
                if (!isLoadMore) {
                    datalist = response.getData();
                    if (adapter == null) {
                        adapter = new LiveStockAdapter(this, datalist, new OnItemClickListener() {
                            @Override
                            public void onClick(Object Item, View v) {
                                LiveStockRes.Datum liveStockRes= (LiveStockRes.Datum) Item;
                                Intent i=new Intent(CatelogActivity.this,OldStockDesignActivity.class);
                                i.putExtra("designid",liveStockRes.getID());
                                startActivity(i);
                            }
                        });
                        binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                    } else {
                        adapter.refresh(datalist);
                        adapter.setLoaded();
                    }
                    adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                    adapter.setOnLoadMoreListener(CatelogActivity.this);
                    switchVisibility(0);

                    if (datalist != null && datalist.size() == 0) {
                        switchVisibility(0);
                    }

                } else {
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() > 0) {
                                datalist.remove(datalist.size() - 1);
                            }
                            datalist.addAll(response.getData());
                            if (response.getData().size() == 0) {

                            }
                            isLoadMore = false;
                            if (adapter == null) {
                                adapter = new LiveStockAdapter(CatelogActivity.this, datalist, new OnItemClickListener() {
                                    @Override
                                    public void onClick(Object Item, View v) {
                                        LiveStockRes.Datum liveStockRes= (LiveStockRes.Datum) Item;
                                        Intent i=new Intent(CatelogActivity.this,OldStockDesignActivity.class);
                                        i.putExtra("designid",liveStockRes.getID());
                                        startActivity(i);
                                    }
                                });
                                binding.fragmentRecyclerCommon.recyclerCommon.setAdapter(adapter);
                            } else {
                                adapter.refresh(datalist);
                                adapter.setLoaded();
                            }
                            adapter.setUpScrollListener(binding.fragmentRecyclerCommon.recyclerCommon, layoutManager);
                            adapter.setOnLoadMoreListener(CatelogActivity.this);
                            switchVisibility(0);
                        }
                    }, LOAD_MORE_DELAY);
                }
            }

        } else {
            switchVisibility(0);
        }

    }

    @Override
    public void switchVisibility(int type) {
        binding.swipe.setRefreshing(false);
        binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
        switch (type) {
            case 0:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.VISIBLE);
                break;

            case 1:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;

            case 2:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.fragmentRecyclerCommon.emptyLayout.textEmpty.setText(getString(R.string.no_internet));
                binding.fragmentRecyclerCommon.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 3:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                break;
            case 4:
                binding.fragmentRecyclerCommon.emptyLayout.container.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.recyclerCommon.setVisibility(View.GONE);
                binding.fragmentRecyclerCommon.progressLayout.progressBar.setVisibility(View.VISIBLE);
                break;

        }
    }
    public void callSearchApi(String str,int offset)
    {
        presenter.getSearchLiveStock(offset,str,isConnected);

    }

    @Override
    public void onLoadMore(String type) {
        if(!isLoadMore) {
        isLoadMore = true;
        LiveStockRes.Datum dataItem = new LiveStockRes.Datum();
        dataItem.setCatName("Loading");
        datalist.add(dataItem);
        adapter.refresh(datalist);
        offset=offset+1;
        callApi(offset);
        }


    }
    private void initSearchView() {
        new SearchPresenterImpl(this, this).getSearchKeywordDetails(binding.commonSearchLayout.editSearch, "");
        binding.commonSearchLayout.editSearch.setHint("Search name");
        binding.commonSearchLayout.imageClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(binding.commonSearchLayout.editSearch.getText())) {
                    binding.commonSearchLayout.editSearch.setText("");
                    searchFor="";
                    searchApplied=false;
                    offset=0;
                    callApi(0);
                    binding.commonToolbarLayout.toolbar.setVisibility(View.VISIBLE);
                    binding.commonSearchLayout.cardSearchLayout.setVisibility(View.GONE);
                }
            }
        });
    }
    @Override
    public void onSearchStarted(String search) {
        if (datalist != null) {
            datalist.clear();
        }
        isLoadMore = false;
        searchApplied = true;
        searchFor = search;
        offset=0;
        if (isConnected) {
            callSearchApi(search,0);
        } else {
            switchVisibility(2);
        }
    }

    @Override
    public void onSearchCompleted(List<Object> listCategories) {
        if (datalist != null) {
            datalist.clear();
        }
//        if (adapter != null) {
//            adapter = null;
//        }
        searchApplied = false;
        searchFor = "";
        isLoadMore = false;
        offset=0;
        if (isConnected) {
            callSearchApi("",0);
        } else {
            switchVisibility(2);
        }
    }

    @Override
    public void onMenuItemActionCollapse(Toolbar searchToolbar, Toolbar mainToolbar) {
        searchToolbar.setVisibility(View.GONE);
        mainToolbar.setVisibility(View.VISIBLE);
        isLoadMore = false;
        searchApplied = false;
        searchFor = "";
        if (isConnected) {
            callApi(0);
        } else{
            switchVisibility(2);
        }
    }
}

