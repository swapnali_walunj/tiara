package com.tiara.ui.activity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.jakewharton.rxbinding.view.RxView;
import com.tiara.api.ApiNames;
import com.tiara.databinding.ActivityLiveStockBinding;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.mvp.PresenterImpl.SearchPresenterImpl;
import com.tiara.mvp.View.SearchProcessView;
import com.tiara.ui.adapter.LiveStockAdapter;
import com.tiara.ui.base.BaseActivity;
import com.tiara.R;
import com.tiara.ui.fragment.FilterFragment;
import com.tiara.ui.listener.OnFilterListener;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.NavigationModel;
import com.tiara.ui.model.Response.FilterCopy;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.LiveStockRes;
import com.tiara.utils.AppUtils;
import com.tiara.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.LOAD_MORE_DELAY;

public class LiveStockActivity extends BaseActivity implements OnFilterListener,OnLoadMoreListener, SearchProcessView {
    ActivityLiveStockBinding binding;
    ArrayList<NavigationModel> navigationModels;
    FilterFragment mFilterFragment1;
    FragmentManager mFragmentManager;
    CommonPresenterImpl presenter;
    LiveStockAdapter adapter;
    List<LiveStockRes.Datum> datalist=new ArrayList<>();
    boolean isLoadMore = false;
    int offset=0;
    LiveStockRes response;
    LinearLayoutManager layoutManager;
    private LiveData<List<LiveStockOrder>> mObservableProduct;
    public boolean isSearchOpen = false;
    boolean searchApplied = false;
    String searchFor = "";

    private void initSearchView() {
        new SearchPresenterImpl(this, this).getSearchKeywordDetails(binding.commonSearchLayout.editSearch, "");
        binding.commonSearchLayout.editSearch.setHint("Search name");
        binding.commonSearchLayout.imageClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(binding.commonSearchLayout.editSearch.getText())) {
                    binding.commonSearchLayout.editSearch.setText("");
                    searchFor="";
                    searchApplied=false;
                    offset=0;
                    callApi(0);
                    binding.commonToolbarLayout.toolbar.setVisibility(View.VISIBLE);
                    binding.commonSearchLayout.cardSearchLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_live_stock, null, false);
        baseNavigationDrawerBinding.appBarNav.containerLayout.addView(binding.getRoot(), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        setSupportActionBar(binding.commonToolbarLayout.toolbar);
        initNavigationDrawer(binding.commonToolbarLayout.toolbar);
        binding.commonToolbarLayout.toolbarTitle.setText("Live Stock");
        binding.commonToolbarLayout.toolbarImageSort.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarImageSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_dark_gray));
        binding.commonToolbarLayout.toolbarImageSearch.setVisibility(View.VISIBLE);
        presenter=new CommonPresenterImpl(this);
        mFragmentManager = getSupportFragmentManager();
        layoutManager = new LinearLayoutManager(this);
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            customNavigationViewAdapter.showSelected(3, false, -1);
        }else{
            customNavigationViewAdapter.showSelected(2, false, -1);
        }
         setUpRecyclerView(binding.rvFeeds,new LinearLayoutManager(this));
         callApi(0);
         getOfflineOrderListing();
       // initSearchView();

        binding.commonToolbarLayout.toolbarImageSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LiveStockActivity.this,ViewCartActivity.class));
            }
        });


        binding.swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.swipe.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        binding.commonToolbarLayout.toolbarImageSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.commonToolbarLayout.toolbar.setVisibility(View.GONE);
                binding.commonSearchLayout.cardSearchLayout.setVisibility(View.VISIBLE);
                initSearchView();
            }
        });

        RxView.clicks(binding.fab).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {

                if (mFragmentManager.getBackStackEntryCount() == 0) {
                    mFilterFragment1 = new FilterFragment();
                    FragmentTransaction ft = mFragmentManager.beginTransaction();
                    ft.add(binding.frameAdoption.getId(), mFilterFragment1);
                    ft.addToBackStack(Constants.FILTER);
                    ft.commit();
                }
                binding.frameAdoption.setVisibility(View.VISIBLE);
                AppUtils.animateFromBottomtoTop(LiveStockActivity.this, binding.frameAdoption, true);

//                BottomSheetDialogFragment bottomSheetDialogFragment = new FilterFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString(FILTER_DATA, new Gson().toJson(filterCopy == null ? new FilterCopy() : filterCopy));
//                bundle.putBoolean(IS_FILTER, true);
//                bottomSheetDialogFragment.setArguments(bundle);
//                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });
    }
    synchronized void getOfflineOrderListing() {
        mObservableProduct = appDatabase.orderListingDao().loadOrderList();
        mObservableProduct.observe(this, new Observer<List<LiveStockOrder>>() {
            @Override
            public void onChanged(@Nullable List<LiveStockOrder> itelist) {
                if (itelist != null && itelist.size() > 0) {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.VISIBLE);
                    binding.commonToolbarLayout.tvCount.setText(itelist.size()+"");
                }
                else {
                    binding.commonToolbarLayout.tvCount.setVisibility(View.GONE);
                }

            }
        });
    }
    public void callApi(int offset)
    {
        presenter.getLiveStock(getCustUserId(),offset,isConnected);

    }


    public void callSearchApi(String str,int offset)
    {
        presenter.getSearchLiveStock(offset,str,isConnected);

    }

    @Override
    public void onResponseFailure(String msg) {
        super.onResponseFailure(msg);
        if(datalist!= null && datalist.size()==0) {
            switchVisibility(3);
        }
    }

    @Override
    public void onResponseSuccess(Object data, String apiName, String searchKey) {
        switch (apiName) {
            case ApiNames.LIVE_STOCK_SEARCH:
                LiveStockRes response = (LiveStockRes) data;
                this.response=response;
                showData(response);
                break;
        }
    }

    @Override
    public void onFilterApplied(FilterCopy filterCopy) {
        binding.frameAdoption.setVisibility(View.GONE);
        AppUtils.animateFromBottomtoTop(LiveStockActivity.this, binding.frameAdoption, false);
    }

    @Override
    public void onFilterReset(FilterCopy filterCopy) {
        binding.frameAdoption.setVisibility(View.GONE);
        AppUtils.animateFromBottomtoTop(LiveStockActivity.this, binding.frameAdoption, false);
    }

    @Override
    public void onFilterHide() {
        binding.frameAdoption.setVisibility(View.GONE);
        AppUtils.animateFromBottomtoTop(LiveStockActivity.this, binding.frameAdoption, false);
    }

    @Override
    public void onBackPressed() {
        if (binding.frameAdoption.getVisibility() == View.VISIBLE) {
            binding.frameAdoption.setVisibility(View.GONE);
            AppUtils.animateFromBottomtoTop(LiveStockActivity.this, binding.frameAdoption, false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.LIVE_STOCK:
                LiveStockRes response = (LiveStockRes) data;
                this.response=response;
                showData(response);
                break;
        }
    }
    private void showData(final LiveStockRes response) {
             if (response != null) {
            if (datalist != null && datalist.size() == 0 && response.getData() != null && response.getData().size() == 0) {
                adapter = new LiveStockAdapter(this, datalist, new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {
                        LiveStockRes.Datum liveStockRes= (LiveStockRes.Datum) Item;
                        Intent i=new Intent(LiveStockActivity.this,LiveStockDesignActivity.class);
                        i.putExtra("designid",liveStockRes.getID());
                        startActivity(i);
                    }
                });
                binding.rvFeeds.setAdapter(adapter);
                // switchVisibility(0);
            } else {
                if (!isLoadMore) {
                    datalist = response.getData();
                    if (adapter == null) {
                        adapter = new LiveStockAdapter(this, datalist, new OnItemClickListener() {
                            @Override
                            public void onClick(Object Item, View v) {
                                LiveStockRes.Datum liveStockRes= (LiveStockRes.Datum) Item;
                             Intent i=new Intent(LiveStockActivity.this,LiveStockDesignActivity.class);
                             i.putExtra("designid",liveStockRes.getID());
                             startActivity(i);
                            }
                        });
                        binding.rvFeeds.setAdapter(adapter);
                    } else {
                        adapter.refresh(datalist);
                        adapter.setLoaded();
                    }
                    adapter.setUpScrollListener(binding.rvFeeds, layoutManager);
                    adapter.setOnLoadMoreListener(LiveStockActivity.this);
                    switchVisibility(0);

                    if (datalist != null && datalist.size() == 0) {
                        switchVisibility(0);
                    }

                } else {
                    new android.os.Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() > 0) {
                                datalist.remove(datalist.size() - 1);
                            }
                            datalist.addAll(response.getData());
                            if (response.getData().size() == 0) {

                            }
                            isLoadMore = false;
                            if (adapter == null) {
                                adapter = new LiveStockAdapter(LiveStockActivity.this, datalist, new OnItemClickListener() {
                                    @Override
                                    public void onClick(Object Item, View v) {
                                        LiveStockRes.Datum liveStockRes= (LiveStockRes.Datum) Item;
                                        Intent i=new Intent(LiveStockActivity.this,LiveStockDesignActivity.class);
                                        i.putExtra("designid",liveStockRes.getID());
                                        startActivity(i);
                                    }
                                });
                                binding.rvFeeds.setAdapter(adapter);
                            } else {
                                adapter.refresh(datalist);
                                adapter.setLoaded();
                            }
                            adapter.setUpScrollListener(binding.rvFeeds, layoutManager);
                            adapter.setOnLoadMoreListener(LiveStockActivity.this);
                            switchVisibility(0);
                        }
                    }, LOAD_MORE_DELAY);
                }
            }

        } else {
            switchVisibility(0);
        }

    }

    @Override
    public void switchVisibility(int type) {
        binding.swipe.setRefreshing(false);
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        switch (type) {
            case 0:
                binding.emptyLayout.container.setVisibility(View.GONE);
                binding.progressLayout.progressBar.setVisibility(View.GONE);
                binding.swipe.setVisibility(View.VISIBLE);
                break;

            case 1:
                binding.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.swipe.setVisibility(View.GONE);
                break;

            case 2:
                binding.emptyLayout.container.setVisibility(View.VISIBLE);
                binding.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.emptyLayout.textEmpty.setText(getString(R.string.no_internet));
                binding.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.swipe.setVisibility(View.GONE);
                break;
            case 3:
                binding.emptyLayout.linearEmpty.setVisibility(View.VISIBLE);
                binding.emptyLayout.imageEmpty.setImageResource(R.drawable.internet);
                binding.emptyLayout.textEmpty.setText(getString(R.string.no_match_found));
                binding.emptyLayout.imageRefresh.setVisibility(View.VISIBLE);
                binding.swipe.setVisibility(View.GONE);
                break;
            case 4:
                binding.emptyLayout.container.setVisibility(View.GONE);
                binding.swipe.setVisibility(View.GONE);
                binding.progressLayout.progressBar.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public void onLoadMore(String type) {
        //if(!isLoadMore) {
        isLoadMore = true;
        LiveStockRes.Datum dataItem = new LiveStockRes.Datum();
        dataItem.setCatName("Loading");
        datalist.add(dataItem);
        adapter.refresh(datalist);
        offset=offset+1;
        if(searchApplied)
        {
            callSearchApi(searchFor,offset);
        }else {
            callApi(offset);
        }
        // }


    }

    @Override
    public void onSearchStarted(String search) {
        if (datalist != null) {
            datalist.clear();
        }
        isLoadMore = false;
        searchApplied = true;
        searchFor = search;
        offset=0;
        if (isConnected) {
            callSearchApi(search,0);
        } else {
            switchVisibility(2);
        }
    }

    @Override
    public void onSearchCompleted(List<Object> listCategories) {
        if (datalist != null) {
            datalist.clear();
        }
//        if (adapter != null) {
//            adapter = null;
//        }
        searchApplied = false;
        searchFor = "";
        isLoadMore = false;
         offset=0;
        if (isConnected) {
            callSearchApi("",0);
        } else {
            switchVisibility(2);
        }
    }

    @Override
    public void onMenuItemActionCollapse(Toolbar searchToolbar, Toolbar mainToolbar) {
        searchToolbar.setVisibility(View.GONE);
        mainToolbar.setVisibility(View.VISIBLE);
        isLoadMore = false;
        searchApplied = false;
        searchFor = "";
        if (isConnected) {
            callApi(0);
        } else{
            switchVisibility(2);
        }
    }



}
