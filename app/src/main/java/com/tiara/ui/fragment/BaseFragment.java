package com.tiara.ui.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;


import com.tiara.dependancy.DaggerDependancyInjection;
import com.tiara.mvp.PresenterImpl.ConnectionPresenterImpl;
import com.tiara.mvp.View.BaseView;
import com.tiara.mvp.View.ConnectionView;
import com.tiara.ui.base.AppController;
import com.tiara.ui.model.Response.CommonErrorResponse;
import com.tiara.utils.AppUtils;
import com.tiara.utils.PreferenceUtils;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;
import com.zplesac.connectionbuddy.models.ConnectivityState;

import javax.inject.Inject;


/**
 * Created by itrs-203 on 11/14/17.
 */

public abstract class BaseFragment extends Fragment implements BaseView, ConnectionView {

    public boolean isConnected = false;
    ConnectionPresenterImpl connectionPresenter;
    private ProgressDialog progressDialog;
        public String moduleData = "";

    @Inject
    public PreferenceUtils mPreferenceUtils;

    // Returns the callback for Swipe To Refresh.
    public abstract void onSectionRefresh();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        connectionPresenter = new ConnectionPresenterImpl(this);
        connectionPresenter.init(savedInstanceState != null);

    }



    public void setUpRecyclerView(RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showToast(String Message) {

    }

    @Override
    public void showToast(int id) {

    }


    @Override
    public void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = AppUtils.showNonDismissibleProgressDialog(getActivity());
            } else if (!progressDialog.isShowing()) {
                progressDialog = AppUtils.showNonDismissibleProgressDialog(getActivity());
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void showCancelableProgressDialog() {
        if (progressDialog == null) {
            progressDialog = AppUtils.showProgressDialog(getActivity());
        } else if (!progressDialog.isShowing()) {
            progressDialog = AppUtils.showNonDismissibleProgressDialog(getActivity());
        }
    }

    @Override
    public void hideProgressDialog() {
        AppUtils.dismissProgressDialog(progressDialog);
    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {

    }



    @Override
    public void onResponseFailure(String msg) {

    }


    @Override
    public void onErrorResponse(Object data, String apiName) {
        try {
            CommonErrorResponse response = (CommonErrorResponse) data;
            if (response != null) {
                if (response.getMessage() != null && response.getMessage().length() > 0) {
                    showToast(response.getMessage());
                }

                if (response.getCode() == 401) {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void switchVisibility(int type) {

    }

    @Override
    public void switchVisibility(int type, String msg) {

    }

    @Override
    public void initViews(boolean isConnected) {
        this.isConnected = isConnected;
    }

    @Override
    public void onConnectionChangeEvent(ConnectivityEvent event) {
        isConnected = event.getState() == ConnectivityState.CONNECTED;
    }
    @Override
    public void onStart() {
        super.onStart();
        connectionPresenter.registerForNetworkUpdates();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        connectionPresenter.unregisterFromNetworkUpdates();
    }



    // instantiation
    protected PreferenceUtils getPreferenceInstance() {
        if (mPreferenceUtils == null) {
            DaggerDependancyInjection.builder()
                    .baseServiceComponent(AppController.getBaseServiceComponent())
                    .build()
                    .inject(this);
        }
        return mPreferenceUtils;
    }

    @Override
    public void onResponseSuccess(Object data, String apiName, String searchKey) {

    }
}

