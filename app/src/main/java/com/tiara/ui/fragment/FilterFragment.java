package com.tiara.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jakewharton.rxbinding.view.RxView;
import com.tiara.R;
import com.tiara.databinding.ActivityFilterBinding;
import com.tiara.events.FilterEvent;
import com.tiara.ui.adapter.CommonAdapter;
import com.tiara.ui.listener.OnFilterListener;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.FilterCopy;
import com.tiara.utils.AppUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;


import rx.functions.Action1;

import static com.tiara.utils.Constants.FILTER_DATA;
import static com.tiara.utils.Constants.IS_FILTER;


/**
 * Created by Yuvraj on 5/23/2017.
 */

public class FilterFragment extends Fragment {
    ActivityFilterBinding binding;
    OnFilterListener listener;
//    private BottomSheetBehavior mBehavior;


    FilterCategoriesFragment filterCategoriesFragment;
    FilterSizeFragment filterSizeFragment;
    FilterCopy filterCopy;

    public static Fragment newInstance(Activity context) {
        Bundle b = new Bundle();
        return Fragment.instantiate(context, FilterFragment.class.getName(), b);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_filter, null, false);
//        mBehavior = BottomSheetBehavior.from(binding.getRoot());

        setUpToolbar();
        initRecyclerView();
        setOnClicks();
        filterCopy = new FilterCopy();
        switchFragments(0);

        return binding.getRoot();
    }

    private void setUpToolbar() {
        binding.commonToolbarLayout.toolbarTitle.setText("Filter");
        binding.commonToolbarLayout.toolbarTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        binding.commonToolbarLayout.toolbarNavImage.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarNavImage.setImageResource(R.drawable.ic_cross_symbol);
        binding.commonToolbarLayout.toolbarRighttext.setVisibility(View.VISIBLE);
        binding.commonToolbarLayout.toolbarRighttext.setText("Reset");
        binding.commonToolbarLayout.toolbarRighttext.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
        binding.commonToolbarLayout.toolbarTitle.setPadding(Math.round(getResources().getDimension(R.dimen.margin50)), 0, 0, 0);
        binding.commonToolbarLayout.toolbar.setBackgroundResource(R.drawable.white);
    }

    public void setUpRecyclerView(RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }
    private void initRecyclerView() {
        final ArrayList<String> listFilterCategories = new ArrayList<>();
        listFilterCategories.add("Color");
        listFilterCategories.add("Size");
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        setUpRecyclerView(binding.recyclerCommon, new LinearLayoutManager(getContext()));
        binding.recyclerCommon.setAdapter(new CommonAdapter(getActivity(), listFilterCategories, new OnItemClickListener() {
            @Override
            public void onClick(Object Item, View v) {
                try {
                    for (int i = 0; i < listFilterCategories.size(); i++) {
                        binding.recyclerCommon.getChildAt(i).setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.app_bg_filter));
                        ((TextView) binding.recyclerCommon.getChildAt(i).findViewById(R.id.text_data)).setTextColor(ContextCompat.getColor(getActivity(), R.color.textHeading));
                    }
                    v.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.textBlue));
                    ((TextView) v.findViewById(R.id.text_data)).setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    switchFragments(Integer.parseInt(Item + ""));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, true));
    }

    private void setOnClicks() {
        RxView.clicks(binding.btnFilterApply).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                if (listener != null)
                    AppUtils.hideKeyboard(getActivity());
                if(TextUtils.isEmpty(filterCopy.getSizeid()) && TextUtils.isEmpty(filterCopy.getColorid())) {
                    AppUtils.toastMe(getContext(),"Please select atleast one color or size for filter.");
                }else{
                    if(!TextUtils.isEmpty(filterCopy.getColorid()) && !TextUtils.isEmpty(filterCopy.getSizeid())){
                        listener.onFilterApplied(filterCopy);
                    }else {
                        if (TextUtils.isEmpty(filterCopy.getColorid())) {
                            filterCopy.setColorid("0");
                            filterCopy.setColorname("0");
                            listener.onFilterApplied(filterCopy);
                        }
                        if (TextUtils.isEmpty(filterCopy.getSizeid())) {
                            //AppUtils.toastMe(getContext(),"Please select atleast one size.");
                            filterCopy.setSizeid("0");
                            filterCopy.setSizename("0");
                            listener.onFilterApplied(filterCopy);
                        }
                    }
                }
                // dismiss();
            }
        });

        RxView.clicks(binding.commonToolbarLayout.toolbarRighttext).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                filterCopy.clearFilter();
                listener.onFilterReset(filterCopy);
//                dismiss();
            }
        });

        RxView.clicks(binding.commonToolbarLayout.toolbarNavImage).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                listener.onFilterHide();
//                dismiss();
            }
        });
    }




    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        if (mBehavior != null) {
//            mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//        }
    }

    private void switchFragments(int position) {
        switch (position) {
            case 0:
                filterCategoriesFragment = FilterCategoriesFragment.newInstance();
                setArgumentsToFragment(filterCategoriesFragment);
                AppUtils.pushFragment(filterCategoriesFragment, binding.fragmentFilterLayout.getId(), (AppCompatActivity) getActivity(), null);
                break;

            case 1:
                filterSizeFragment = filterSizeFragment.newInstance();
                setArgumentsToFragment(filterSizeFragment);
                AppUtils.pushFragment(filterSizeFragment, binding.fragmentFilterLayout.getId(), (AppCompatActivity) getActivity(), null);
                break;


        }
    }


    private void setArgumentsToFragment(Fragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putString(FILTER_DATA, new Gson().toJson(filterCopy));
        bundle.putBoolean(IS_FILTER, true);
        if(filterCopy!=null && filterCopy.getSizeid()!=null) {
            bundle.putString("Size", filterCopy.getSizeid());
        }
        fragment.setArguments(bundle);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        if (activity instanceof OnFilterListener) {
            listener = (OnFilterListener) activity;
        }
    }
    @org.greenrobot.eventbus.Subscribe
    public void onEventMainThread(FilterEvent filterEvent) {
        this.filterCopy = filterEvent.getFilterCopy();
        AppUtils.logMe("FILTER", new Gson().toJson(filterCopy));
           }




}