package com.tiara.ui.fragment;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.drawable.ProgressBarDrawable;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.tiara.R;
import com.tiara.databinding.ViewImageBinding;
import com.tiara.utils.Utils;

import java.io.File;


/**
 * Created by user on 28/3/17.
 */
public class ImageFragment extends Fragment {


    String url;
    int pos = 1, count = 1;
    private ViewImageBinding binding;

    public ImageFragment() {
        // Required empty public constructor
    }

    public ImageFragment newInstance(String url, int pos, int count) {

        ImageFragment fragment = new ImageFragment();

        Bundle b = new Bundle();
        b.putString("url", url);
        b.putInt("pos", pos);
        b.putInt("count", count);
        fragment.setArguments(b);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.view_image, container, false);

        url = this.getArguments().getString("url");
        pos = this.getArguments().getInt("pos", 0);
        count = this.getArguments().getInt("count", 0);

        //Utils.loadImageInImageview1(getActivity(),url,binding.touchview);

        Uri uri;

                uri = Uri.parse(url);
            int width = 0, height = 0;
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            if (metrics != null) {
                width = metrics.widthPixels;
                height = metrics.heightPixels;
            }

            ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                    .setResizeOptions(new ResizeOptions(width, height))
                    .build();

            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(binding.touchview.getController())
                    .setTapToRetryEnabled(true)
                    .setAutoPlayAnimations(true)
                    .setImageRequest(request)
                    .build();
//                DraweeController ctrl = Fresco.newDraweeControllerBuilder().setUri(uri).setTapToRetryEnabled(true).build();
            GenericDraweeHierarchy hierarchy = new GenericDraweeHierarchyBuilder(getResources())
                    .setActualImageScaleType(ScalingUtils.ScaleType.FIT_CENTER)
                    .setProgressBarImage(new ProgressBarDrawable())
                    .build();

            binding.touchview.setController(controller);
            binding.touchview.setHierarchy(hierarchy);


        binding.Noview.setVisibility(View.VISIBLE);
        binding.Noview.setText((pos + 1) + " of " + count);

        binding.imageCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });




        //binding.setImages(url);

        return binding.getRoot();
    }

}