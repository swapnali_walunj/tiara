package com.tiara.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.databinding.FilterCategoryFragmentBinding;
import com.tiara.events.FilterEvent;
import com.tiara.mvp.Presenter.CommonPresenter;
import com.tiara.mvp.PresenterImpl.CommonPresenterImpl;
import com.tiara.ui.adapter.ColorAdapter;
import com.tiara.ui.listener.OnFilterItemClickListener;
import com.tiara.ui.listener.OnFilterListener;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.ColorResponse;
import com.tiara.ui.model.Response.FilterCopy;
import com.tiara.utils.AppUtils;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.FILTER_DATA;
import static com.tiara.utils.Constants.IS_FILTER;


public class FilterCategoriesFragment extends BaseFragment {

    FilterCategoryFragmentBinding binding;
    boolean isFromFilter = false;
    CommonPresenter commonPresenter;
    ColorAdapter adapter;
    FilterCopy filterCopy;
    OnItemClickListener listener;
    OnFilterListener filterListener;
    List<ColorResponse.Datum> listCategories;

    public FilterCategoriesFragment() {
        // Required empty public constructor
    }

    public static FilterCategoriesFragment newInstance() {
        return new FilterCategoriesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.filter_category_fragment, null, false);
        binding.linearParent.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.app_bg));
        AppUtils.hideKeyboard(getActivity());
        commonPresenter=new CommonPresenterImpl(this);
        binding.chooseText.setText("Choose Color");
        setUpRecyclerView(binding.recyclerCategories, new LinearLayoutManager(getContext()));
        binding.recyclerCategories.setNestedScrollingEnabled(false);
        commonPresenter.getColorstock(isConnected);
        filterCopy=new FilterCopy();



        binding.emptyLayout.imageRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commonPresenter.getColorstock(isConnected);
            }
        });
        return binding.getRoot();
    }

    public void parseIntent() {

        if (getArguments() != null) {
            String data = getArguments().getString(FILTER_DATA);
            if (!TextUtils.isEmpty(data)) {
                filterCopy = new Gson().fromJson(data, FilterCopy.class);
            } else {
                filterCopy = new FilterCopy();
            }
            isFromFilter = getArguments().getBoolean(IS_FILTER, false);

            if (filterCopy != null && filterCopy.getColorid() != null && filterCopy.getColorid().length() > 0) {
                String[] items = filterCopy.getColorid().split(",");
                for (String item : items) {
                    if (item.trim().length() > 0) {
                        for (int i = 0; i < listCategories.size(); i++) {
                            if (listCategories.get(i).getColourID().toString().equals(item)) {
                                listCategories.get(i).setSelected(true);
                            }
                           // listCategories.get(i).setFromFilter(isFromFilter);
                        }
                    }
                }
            } else {
                for (int i = 0; i < listCategories.size(); i++) {
                    listCategories.get(i).setSelected(false);
                    //listCategories.get(i).setFromFilter(isFromFilter);
                }
            }
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnItemClickListener) {
            listener = (OnItemClickListener) activity;
        }
    }

    @Override
    public void onSectionRefresh() {

    }

    public void setUpRecyclerView(RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }
    @Override
    public void onResponseSuccess(Object data, String apiName) {
        switch (apiName) {
            case ApiNames.GET_COLOR_DETAILS:
                ColorResponse response = (ColorResponse) data;
                binding.progressLayout.progressBar.setVisibility(View.GONE);
                listCategories = new ArrayList<>();
                //listCategories.add(0,new ColorResponse.Datum(500,"All",false));
                listCategories = response.getData();
                if (listCategories != null && listCategories.size() > 0) {
                    parseIntent();
                }
                adapter=new ColorAdapter(getActivity(),listCategories, new OnFilterItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v, int pos) {
                        int value = (int) v.getTag();
                        int position=0;
                        boolean isChecked = (boolean) Item;


                        for (int i = 0; i < adapter.getData().size(); i++) {
                           /* if(pos==0)
                            {
                                adapter.getData().get(i).setSelected(isChecked);
                            }else {*/
                                if (adapter.getData().get(i).getColourID() == value) {
                                    adapter.getData().get(i).setSelected(isChecked);
                                //}
                            }
                        }
                        if (listener != null) {
                            filterCopy.setColorid(adapter.getData().get(position).getColourID()+"");
                            filterCopy.setColorname(adapter.getData().get(position).getColourName());
                            listener.onClick(filterCopy, v);
                        }
                        filterCopy.setColorid(getSelectedCategories());
                        EventBus.getDefault().post(new FilterEvent(filterCopy));
                    }


                });
                binding.recyclerCategories.setAdapter(adapter);
                break;
        }
    }

    @Override
    public void onResponseSuccess(Object data, String apiName, int position, String grppostion) {

    }

    @Override
    public void onResponseFailure(String msg, String apiName, int position, String grppostion) {

    }
    private String getSelectedCategories() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < adapter.getData().size(); i++) {
            if (i == 0) {
                if (adapter.getData().get(i).isSelected()) {
                    builder.append(adapter.getData().get(i).getColourID());
                }
            } else {
                if (adapter.getData().get(i).isSelected()) {
                    builder.append(",").append(adapter.getData().get(i).getColourID());
                }
            }
        }
        return builder.toString();
    }
}





