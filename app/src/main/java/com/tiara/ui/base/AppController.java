package com.tiara.ui.base;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.common.logging.FLog;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.listener.RequestListener;
import com.facebook.imagepipeline.listener.RequestLoggingListener;

import com.tiara.dependancy.DaggerDependancyInjection;
import com.tiara.dependancy.component.BaseServiceComponent;
import com.tiara.dependancy.component.DaggerBaseServiceComponent;
import com.tiara.dependancy.module.AppModule;
import com.tiara.dependancy.module.BaseModule;
import com.tiara.utils.PreferenceUtils;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.ConnectionBuddyConfiguration;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

/**
 * Created by itrs-203 on 11/13/17.
 */

public class AppController extends Application {

    public static BaseServiceComponent baseServiceComponent;
    @Inject
    public PreferenceUtils mPreferenceUtils;
    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        Set<RequestListener> requestListeners = new HashSet<>();
        requestListeners.add(new RequestLoggingListener());
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setRequestListeners(requestListeners)
                .build();
        Fresco.initialize(this, config);
        FLog.setMinimumLoggingLevel(FLog.VERBOSE);

        ConnectionBuddyConfiguration networkInspectorConfiguration = new ConnectionBuddyConfiguration.Builder(
                this).build();
        ConnectionBuddy.getInstance().init(networkInspectorConfiguration);

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath(getString(R.string.font_standard_regular))
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );

        baseServiceComponent = DaggerBaseServiceComponent.builder()
                .baseModule(new BaseModule(this))
                .appModule(new AppModule(this))
                .build();

        //Analytics code
        mInstance = this;

    }

    // instantiation
    protected PreferenceUtils getPreferenceInstance() {
        if (mPreferenceUtils == null) {
            DaggerDependancyInjection.builder()
                    .baseServiceComponent(AppController.getBaseServiceComponent())
                    .build()
                    .inject(this);
        }
        return mPreferenceUtils;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static BaseServiceComponent getBaseServiceComponent() {
        return baseServiceComponent;
    }

    //Analytics code
    public static synchronized AppController getInstance() {
        return mInstance;
    }


    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */


    /***
     * Tracking exception
     *
     * @param e exception to be tracked


    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */


}
