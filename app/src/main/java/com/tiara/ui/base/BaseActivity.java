package com.tiara.ui.base;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.tiara.R;
import com.tiara.databinding.ActivityBaseBinding;
import com.tiara.databinding.ItemNavigationBinding;

import com.tiara.db.AppDatabase;
import com.tiara.db.DatabaseInitUtil;
import com.tiara.dependancy.DaggerDependancyInjection;
import com.tiara.mvp.PresenterImpl.ConnectionPresenterImpl;
import com.tiara.mvp.View.BaseView;
import com.tiara.mvp.View.ConnectionView;
import com.tiara.ui.activity.ApprovedOrderActivity;
import com.tiara.ui.activity.CatelogActivity;
import com.tiara.ui.activity.ChangePasswordActivity;
import com.tiara.ui.activity.DashBoardActivity;
import com.tiara.ui.activity.LabourChargeActivity;
import com.tiara.ui.activity.LiveStockActivity;
import com.tiara.ui.activity.LoginActivity;
import com.tiara.ui.activity.MyCustomerActivity;
import com.tiara.ui.activity.MyProfileActivity;
import com.tiara.ui.activity.OldPendingOrderActivity;
import com.tiara.ui.activity.PendingOrderActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.NavigationModel;

import com.tiara.ui.model.Response.CustomerLoginRes;
import com.tiara.ui.model.Response.SalesManLoginRes;
import com.tiara.utils.AppUtils;
import com.tiara.utils.Constants;
import com.tiara.utils.PreferenceUtils;
import com.tiara.utils.Utils;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;
import com.zplesac.connectionbuddy.models.ConnectivityState;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.tiara.utils.Constants.CUSTOMER_RESPONSE;
import static com.tiara.utils.Constants.IS_CUSTOMER;
import static com.tiara.utils.Constants.SALES_RESPONSE;

@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity implements BaseView, ConnectionView {
    public ActivityBaseBinding baseNavigationDrawerBinding;
   public NavigationVIewCustomAdapter customNavigationViewAdapter;
    ArrayList<NavigationModel> dataNavigation;
    ConnectionPresenterImpl connectionPresenter;
    public boolean isConnected = false;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private ProgressDialog progressDialog;
    public AppDatabase appDatabase;

    @Inject
    public PreferenceUtils mPreferenceUtils;


    // instantiation
    protected PreferenceUtils getPreferenceInstance() {
        if (mPreferenceUtils == null) {
            DaggerDependancyInjection.builder()
                    .baseServiceComponent(AppController.getBaseServiceComponent())
                    .build()
                    .inject(this);
        }
        return mPreferenceUtils;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseNavigationDrawerBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);

        connectionPresenter = new ConnectionPresenterImpl(this);
        connectionPresenter.init(savedInstanceState != null);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
            // Do something for lollipop and above versions
            disableAutoFill();
        } else{
            // do something for phones running an SDK before lollipop
        }

       createDB();
        // if (insideApp) {
        makeNavigationListData();
            setUpRecyclerView(baseNavigationDrawerBinding.recyclerNavigation, new LinearLayoutManager(this));
            customNavigationViewAdapter = new NavigationVIewCustomAdapter(this, dataNavigation, new OnItemClickListener() {
                @Override
                public void onClick(Object Item, View v) {

                    String item = (String) Item;
                    navigationRedirectionScreen(item);
                }
            });

        baseNavigationDrawerBinding.recyclerNavigation.setAdapter(customNavigationViewAdapter);
        clickListeners();

    }
    private void createDB() {
        appDatabase = AppDatabase.getDatabase(this.getApplication());
    }
    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    public void configureToolbar(Toolbar toolbar, int drawableResource, int color) {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material_new);
            upArrow.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    public void initNavigationDrawer(Toolbar toolbar) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, baseNavigationDrawerBinding.drawerLayout,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.nav_icon_drawer);

        baseNavigationDrawerBinding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    public int getCustUserId()
    {
        int id=0;
        if(!TextUtils.isEmpty(getPreferenceInstance().getValue(CUSTOMER_RESPONSE)))
        {

            String res=getPreferenceInstance().getValue(CUSTOMER_RESPONSE);
            CustomerLoginRes customerLoginRes = new Gson().fromJson(res, CustomerLoginRes.class);
            id=customerLoginRes.getData().get(0).getCustID();
        }else if(!TextUtils.isEmpty(getPreferenceInstance().getValue(SALES_RESPONSE))){
            String res=getPreferenceInstance().getValue(SALES_RESPONSE);
            SalesManLoginRes customerLoginRes = new Gson().fromJson(res, SalesManLoginRes.class);
            id=customerLoginRes.getData().get(0).getSalesPID();
        }else{
            id=0;
        }

        return id;
    }

    private void makeNavigationListData() {
        dataNavigation = new ArrayList<>();
        dataNavigation.add(new NavigationModel(Constants.NAV_DASHBOARD, R.drawable.ic_dashboard_nav_updated, false));
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            dataNavigation.add(new NavigationModel(Constants.NAV_MY_PROFILE, R.drawable.ic_dashboard_nav_updated, false));
        }
        if(getPreferenceInstance().getValue(IS_CUSTOMER,false)) {
            dataNavigation.add(new NavigationModel(Constants.NAV_LABOUR_CHARGE, R.drawable.ic_dashboard_nav_updated, false));
        }else{
            dataNavigation.add(new NavigationModel(Constants.NAV_MY_CUSTOMER, R.drawable.ic_dashboard_nav_updated, false));
        }
        dataNavigation.add(new NavigationModel(Constants.NAV_LIVE_STOCK, R.drawable.ic_dashboard_nav_updated, false));
        dataNavigation.add(new NavigationModel(Constants.NAV_CATALOG, R.drawable.ic_dashboard_nav_updated, false));
        dataNavigation.add(new NavigationModel(Constants.NAV_SALES_ORDER, R.drawable.ic_dashboard_nav_updated, false));
        dataNavigation.add(new NavigationModel(Constants.NAV_SALES_ORDER1, R.drawable.ic_dashboard_nav_updated, false));
        dataNavigation.add(new NavigationModel(Constants.NAV_SALES_ORDER2, R.drawable.ic_dashboard_nav_updated, false));
        dataNavigation.add(new NavigationModel(Constants.NAV_CHANGE_PASSWORD, R.drawable.ic_dashboard_nav_updated, false));
        dataNavigation.add(new NavigationModel(Constants.NAV_LOGOUT, R.drawable.ic_dashboard_nav_updated, false));
    }
    public void setUpRecyclerView(RecyclerView recyclerView, RecyclerView.LayoutManager layoutManager) {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showToast(String Message) {
        AppUtils.toastMe(BaseActivity.this, Message);
    }

    @Override
    public void showToast(int id) {
        AppUtils.toastMe(BaseActivity.this, getResources().getString(id));
    }

    @Override
    public void showProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = AppUtils.showNonDismissibleProgressDialog(this);
            } else if (!progressDialog.isShowing()) {
                progressDialog = AppUtils.showNonDismissibleProgressDialog(this);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void showCancelableProgressDialog() {
        if (progressDialog == null) {
            progressDialog = AppUtils.showProgressDialog(this);
        } else if (!progressDialog.isShowing()) {
            progressDialog = AppUtils.showNonDismissibleProgressDialog(this);
        }
    }

    @Override
    public void hideProgressDialog() {
        AppUtils.dismissProgressDialog(progressDialog);
    }

    @Override
    public void onResponseSuccess(Object data, String apiName, String searchKey) {

    }

    @Override
    public void onResponseSuccess(Object data, String apiName) {

    }

    @Override
    public void onResponseSuccess(Object data, String apiName, int position, String grppostion) {

    }

    @Override
    public void onResponseFailure(String msg, String apiName, int position, String grppostion) {

    }

    @Override
    public void onResponseFailure(String msg) {

    }

    @Override
    public void onErrorResponse(Object data, String apiName) {

    }

    @Override
    public void switchVisibility(int type) {

    }

    @Override
    public void switchVisibility(int type, String msg) {

    }

    @Override
    public void initViews(boolean isConnected) {
        this.isConnected = isConnected;
    }

    @Override
    public void onConnectionChangeEvent(ConnectivityEvent event) {
        isConnected = event.getState() == ConnectivityState.CONNECTED;
    }
    public class NavigationVIewCustomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private ArrayList<NavigationModel> data;
        OnItemClickListener listener;
        private Activity activity;
        //        int selectedPosition = 0;
        boolean isSubDashboardVisible = false;
        int selectedSubPosition = 0;


        public NavigationVIewCustomAdapter(Activity activity, ArrayList<NavigationModel> data, OnItemClickListener listener) {
            this.data = data;
            this.activity = activity;
            this.listener = listener;
        }

        public void showSelected(int pos, boolean subDashboard, int subpos) {
            if (pos >= 0) {
                this.data.get(pos).setNavigationSelected(true);
            }
            isSubDashboardVisible = subDashboard;
            selectedSubPosition = subpos;
            notifyDataSetChanged();
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
              ItemNavigationBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_navigation, parent, false);
                return new NavigationVIewCustomAdapter.CustomViewHolder(binding);

        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
            ((CustomViewHolder) holder).binding.textViewNavigationView.setText(data.get(position).getNavigationTitle());
            if (data.get(position).isNavigationSelected()) {
                ((CustomViewHolder) holder).binding.textViewNavigationView.setTextColor(activity.getResources().getColor(R.color.white));
                ((CustomViewHolder) holder).binding.relativeNavigation.setBackgroundColor(activity.getResources().getColor(R.color.nav_item_bg_selected));

                ColorStateList csl = AppCompatResources.getColorStateList(activity, R.color.white);
                Drawable drawable = DrawableCompat.wrap(((CustomViewHolder) holder).binding.imageViewNavigationView.getDrawable());
                DrawableCompat.setTintList(drawable, csl);
                ((CustomViewHolder) holder).binding.imageViewNavigationView.setImageDrawable(drawable);

//                    selectedPosition = position;
            } else {
                ((CustomViewHolder) holder).binding.textViewNavigationView.setTextColor(activity.getResources().getColor(R.color.text_heading));
                ((CustomViewHolder) holder).binding.relativeNavigation.setBackgroundColor(activity.getResources().getColor(android.R.color.transparent));

                ColorStateList csl = AppCompatResources.getColorStateList(activity, R.color.colorPrimary);
                Drawable drawable = DrawableCompat.wrap(((CustomViewHolder) holder).binding.imageViewNavigationView.getDrawable());
                DrawableCompat.setTintList(drawable, csl);
                ((CustomViewHolder) holder).binding.imageViewNavigationView.setImageDrawable(drawable);
            }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        notifyDataSetChanged();
                        if (listener != null)
                            listener.onClick(data.get(position).getNavigationTitle(), v);
                    }
                });
            }

        @Override
        public int getItemViewType(int position) {
                   return Constants.VIEW_NORMAL_NAVIGATION;

        }

        @Override
        public int getItemCount() {
            return data.size() ;
        }
        class CustomViewHolder extends RecyclerView.ViewHolder {
            ItemNavigationBinding binding;

            CustomViewHolder(ItemNavigationBinding bindig) {
                super(bindig.getRoot());
                binding = bindig;
            }
        }



    }


    private void clickListeners() {
        baseNavigationDrawerBinding.appBarNavView.rlHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  startActivity(new Intent(BaseActivity.this, ProfileHomeActivity.class));
                baseNavigationDrawerBinding.drawerLayout.closeDrawers();
            }
        });

//        activityNavigationDrawerBinding.appBarNavView.tvUserName.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(NavigationDrawerActivity.this, ProfileHomeActivity.class));
//                activityNavigationDrawerBinding.drawerLayout.closeDrawers();
//            }
//        });

        baseNavigationDrawerBinding.appBarNavView.tvEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(BaseActivity.this, EditProfileActivity.class));
                baseNavigationDrawerBinding.drawerLayout.closeDrawers();
            }
        });

//        baseNavigationDrawerBinding.appBarNav.fabAddGroup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialogAddGroup();
//                baseNavigationDrawerBinding.appBarNav.fab.close(true);
//            }
//        });

//        baseNavigationDrawerBinding.appBarNav.fabAddChart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(NavigationDrawerActivity.this, AddChartActivity.class);
//                startActivity(in);
//                baseNavigationDrawerBinding.appBarNav.fab.close(true);
//            }
//        });
    }

    private void navigationRedirectionScreen(String item) {

        switch (item) {
            case Constants.NAV_DASHBOARD:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                    Intent intent = new Intent(BaseActivity.this, DashBoardActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                break;

            case Constants.NAV_LABOUR_CHARGE:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("LabourChargeActivity")) {
                        startActivity(new Intent(BaseActivity.this, LabourChargeActivity.class));
                        if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                            finish();
                        }

                }
                break;
            case Constants.NAV_LIVE_STOCK:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("LiveStockActivity")) {
                    startActivity(new Intent(BaseActivity.this, LiveStockActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            case Constants.NAV_CATALOG:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("CatelogActivity")) {
                    startActivity(new Intent(BaseActivity.this, CatelogActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            case Constants.NAV_SALES_ORDER:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("ApprovedOrderActivity")) {
                    startActivity(new Intent(BaseActivity.this, ApprovedOrderActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            case Constants.NAV_SALES_ORDER1:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("PendingOrderActivity")) {
                    startActivity(new Intent(BaseActivity.this, PendingOrderActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            case Constants.NAV_SALES_ORDER2:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("OldPendingOrderActivity")) {
                    startActivity(new Intent(BaseActivity.this, OldPendingOrderActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            case Constants.NAV_MY_PROFILE:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("MyProfileActivity")) {
                    startActivity(new Intent(BaseActivity.this, MyProfileActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            case Constants.NAV_LOGOUT:
                AppUtils.showAlertDialog("Logout", "Are you sure you want to Logout ?", "Yes", "No", BaseActivity.this, new OnItemClickListener() {
                    @Override
                    public void onClick(Object Item, View v) {
                        getPreferenceInstance().clear();
                        DatabaseInitUtil.deleteAllTables(appDatabase);
                        Intent in = new Intent(BaseActivity.this, LoginActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        finish();
                    }
                });
                break;
            case Constants.NAV_CHANGE_PASSWORD:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("ChangePasswordActivity")) {
                    startActivity(new Intent(BaseActivity.this, ChangePasswordActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            case Constants.NAV_MY_CUSTOMER:
                if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("MyCustomerActivity")) {
                    startActivity(new Intent(BaseActivity.this, MyCustomerActivity.class));
                    if (!AppUtils.getCurrentActivity(BaseActivity.this).equalsIgnoreCase("DashBoardActivity")) {
                        finish();
                    }

                }
                break;
            default:
                break;
        }
        baseNavigationDrawerBinding.drawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        if (baseNavigationDrawerBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            baseNavigationDrawerBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        String imageUrl = getPreferenceInstance().getValue(Constants.USER_PIC);
        /*if (!TextUtils.isEmpty(imageUrl)) {
            Utils.loadImageInImageview(BaseActivity.this,imageUrl,baseNavigationDrawerBinding.appBarNavView.imageDrawerProfile);
        }*/
        baseNavigationDrawerBinding.appBarNavView.tvUserName.setText(getPreferenceInstance().getValue(Constants.USER_NAME));
    }

    }

