package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OldPendingOrderDetails {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public OldPendingOrderDetails() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public OldPendingOrderDetails(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("Old_OID")
        @Expose
        private Integer oldOID;
        @SerializedName("Old_ONumber")
        @Expose
        private String oldONumber;
        @SerializedName("Old_ODate")
        @Expose
        private String oldODate;
        @SerializedName("Cust_ID")
        @Expose
        private Integer custID;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("Address_ID")
        @Expose
        private Integer addressID;
        @SerializedName("Address_Address")
        @Expose
        private String addressAddress;
        @SerializedName("Cat_ID")
        @Expose
        private Integer catID;
        @SerializedName("Cat_Name")
        @Expose
        private String catName;
        @SerializedName("Design_ID")
        @Expose
        private Integer designID;
        @SerializedName("Design_DesignNo")
        @Expose
        private String designDesignNo;
        @SerializedName("Colour_ID")
        @Expose
        private Integer colourID;
        @SerializedName("Colour_Name")
        @Expose
        private String colourName;
        @SerializedName("Size_ID")
        @Expose
        private Integer sizeID;
        @SerializedName("Size_Value")
        @Expose
        private String sizeValue;
        @SerializedName("Old_Weight")
        @Expose
        private Double oldWeight;
        @SerializedName("Old_Rate")
        @Expose
        private Double oldRate;
        @SerializedName("Old_Qty")
        @Expose
        private Integer oldQty;
        @SerializedName("Old_LabourChrg")
        @Expose
        private Double oldLabourChrg;
        @SerializedName("Total")
        @Expose
        private Double total;
        @SerializedName("Old_Days")
        @Expose
        private Integer oldDays;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param total
         * @param oldOID
         * @param oldWeight
         * @param designID
         * @param custID
         * @param addressAddress
         * @param colourID
         * @param sizeValue
         * @param addressID
         * @param designDesignNo
         * @param oldRate
         * @param oldLabourChrg
         * @param oldODate
         * @param catID
         * @param oldDays
         * @param catName
         * @param oldONumber
         * @param oldQty
         * @param custName
         * @param sizeID
         * @param colourName
         */
        public Datum(Integer oldOID, String oldONumber, String oldODate, Integer custID, String custName, Integer addressID, String addressAddress, Integer catID, String catName, Integer designID, String designDesignNo, Integer colourID, String colourName, Integer sizeID, String sizeValue, Double oldWeight, Double oldRate, Integer oldQty, Double oldLabourChrg, Double total, Integer oldDays) {
            super();
            this.oldOID = oldOID;
            this.oldONumber = oldONumber;
            this.oldODate = oldODate;
            this.custID = custID;
            this.custName = custName;
            this.addressID = addressID;
            this.addressAddress = addressAddress;
            this.catID = catID;
            this.catName = catName;
            this.designID = designID;
            this.designDesignNo = designDesignNo;
            this.colourID = colourID;
            this.colourName = colourName;
            this.sizeID = sizeID;
            this.sizeValue = sizeValue;
            this.oldWeight = oldWeight;
            this.oldRate = oldRate;
            this.oldQty = oldQty;
            this.oldLabourChrg = oldLabourChrg;
            this.total = total;
            this.oldDays = oldDays;
        }

        public Integer getOldOID() {
            return oldOID;
        }

        public void setOldOID(Integer oldOID) {
            this.oldOID = oldOID;
        }

        public String getOldONumber() {
            return oldONumber;
        }

        public void setOldONumber(String oldONumber) {
            this.oldONumber = oldONumber;
        }

        public String getOldODate() {
            return oldODate;
        }

        public void setOldODate(String oldODate) {
            this.oldODate = oldODate;
        }

        public Integer getCustID() {
            return custID;
        }

        public void setCustID(Integer custID) {
            this.custID = custID;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public Integer getAddressID() {
            return addressID;
        }

        public void setAddressID(Integer addressID) {
            this.addressID = addressID;
        }

        public String getAddressAddress() {
            return addressAddress;
        }

        public void setAddressAddress(String addressAddress) {
            this.addressAddress = addressAddress;
        }

        public Integer getCatID() {
            return catID;
        }

        public void setCatID(Integer catID) {
            this.catID = catID;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public Integer getDesignID() {
            return designID;
        }

        public void setDesignID(Integer designID) {
            this.designID = designID;
        }

        public String getDesignDesignNo() {
            return designDesignNo;
        }

        public void setDesignDesignNo(String designDesignNo) {
            this.designDesignNo = designDesignNo;
        }

        public Integer getColourID() {
            return colourID;
        }

        public void setColourID(Integer colourID) {
            this.colourID = colourID;
        }

        public String getColourName() {
            return colourName;
        }

        public void setColourName(String colourName) {
            this.colourName = colourName;
        }

        public Integer getSizeID() {
            return sizeID;
        }

        public void setSizeID(Integer sizeID) {
            this.sizeID = sizeID;
        }

        public String getSizeValue() {
            return sizeValue;
        }

        public void setSizeValue(String sizeValue) {
            this.sizeValue = sizeValue;
        }

        public Double getOldWeight() {
            return oldWeight;
        }

        public void setOldWeight(Double oldWeight) {
            this.oldWeight = oldWeight;
        }

        public Double getOldRate() {
            return oldRate;
        }

        public void setOldRate(Double oldRate) {
            this.oldRate = oldRate;
        }

        public Integer getOldQty() {
            return oldQty;
        }

        public void setOldQty(Integer oldQty) {
            this.oldQty = oldQty;
        }

        public Double getOldLabourChrg() {
            return oldLabourChrg;
        }

        public void setOldLabourChrg(Double oldLabourChrg) {
            this.oldLabourChrg = oldLabourChrg;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public void setOldDays(Integer oldDays) {
            this.oldDays = oldDays;
        }

        public Integer getOldDays() {
            return oldDays;
        }

    }

}