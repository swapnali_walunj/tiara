package com.tiara.ui.model.Response;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tiara.db.Converters;

@Entity(tableName = "oldstock_order")
@TypeConverters(Converters.class)
public class OldStockOrder implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Integer id;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }
    @SerializedName("Cat_ID")
    @Expose
    private Integer catID;
    @SerializedName("Cat_Name")
    @Expose
    private String catName;
    @SerializedName("Design_ID")
    @Expose
    private Integer designID;
    @SerializedName("Design_DesignNo")
    @Expose
    private String designDesignNo;
    @SerializedName("Colour_ID")
    @Expose
    private Integer colourID;
    @SerializedName("Colour_Name")
    @Expose
    private String colourName;
    @SerializedName("Size_ID")
    @Expose
    private Integer sizeID;
    @SerializedName("Size_Value")
    @Expose
    private String sizeValue;
    @SerializedName("Design_Avgwt")
    @Expose
    private double designAvgwt;
    @SerializedName("Design_Avgdays")
    @Expose
    private Integer designAvgdays;
    @SerializedName("Rate")
    @Expose
    private double rate;
    @SerializedName("Design_Avgdays1")
    @Expose
    private Integer designAvgdays1;

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    private Integer qty;

    /**
     * No args constructor for use in serialization
     *
     */
    public OldStockOrder() {
    }

    /**
     *
     * @param designDesignNo
     * @param rate
     * @param designAvgdays1
     * @param designID
     * @param catID
     * @param catName
     * @param designAvgdays
     * @param colourID
     * @param designAvgwt
     * @param sizeValue
     * @param sizeID
     * @param colourName
     */
    public OldStockOrder(Integer catID, String catName, Integer designID, String designDesignNo, Integer colourID, String colourName, Integer sizeID, String sizeValue, Integer designAvgwt, Integer designAvgdays, double rate, Integer designAvgdays1,Integer qty) {
        super();
        this.catID = catID;
        this.catName = catName;
        this.designID = designID;
        this.designDesignNo = designDesignNo;
        this.colourID = colourID;
        this.colourName = colourName;
        this.sizeID = sizeID;
        this.sizeValue = sizeValue;
        this.designAvgwt = designAvgwt;
        this.designAvgdays = designAvgdays;
        this.rate = rate;
        this.designAvgdays1 = designAvgdays1;
        this.qty=qty;
    }

    protected OldStockOrder(Parcel in) {
        if (in.readByte() == 0) {
            catID = null;
        } else {
            catID = in.readInt();
        }
        catName = in.readString();
        if (in.readByte() == 0) {
            designID = null;
        } else {
            designID = in.readInt();
        }
        designDesignNo = in.readString();
        if (in.readByte() == 0) {
            colourID = null;
        } else {
            colourID = in.readInt();
        }
        colourName = in.readString();
        if (in.readByte() == 0) {
            sizeID = null;
        } else {
            sizeID = in.readInt();
        }
        sizeValue = in.readString();
        if (in.readByte() == 0) {
            designAvgwt = 0.0;
        } else {
            designAvgwt = in.readDouble();
        }
        if (in.readByte() == 0) {
            designAvgdays = null;
        } else {
            designAvgdays = in.readInt();
        }
        if (in.readByte() == 0) {
            rate = 0.0;
        } else {
            rate = in.readDouble();
        }
        if (in.readByte() == 0) {
            designAvgdays1 = null;
        } else {
            designAvgdays1 = in.readInt();
        }
        qty=in.readInt();
    }

    public static final Creator<OldStockOrder> CREATOR = new Creator<OldStockOrder>() {
        @Override
        public OldStockOrder createFromParcel(Parcel in) {
            return new OldStockOrder(in);
        }

        @Override
        public OldStockOrder[] newArray(int size) {
            return new OldStockOrder[size];
        }
    };

    public Integer getCatID() {
        return catID;
    }

    public void setCatID(Integer catID) {
        this.catID = catID;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Integer getDesignID() {
        return designID;
    }

    public void setDesignID(Integer designID) {
        this.designID = designID;
    }

    public String getDesignDesignNo() {
        return designDesignNo;
    }

    public void setDesignDesignNo(String designDesignNo) {
        this.designDesignNo = designDesignNo;
    }

    public Integer getColourID() {
        return colourID;
    }

    public void setColourID(Integer colourID) {
        this.colourID = colourID;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public Integer getSizeID() {
        return sizeID;
    }

    public void setSizeID(Integer sizeID) {
        this.sizeID = sizeID;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public double getDesignAvgwt() {
        return designAvgwt;
    }

    public void setDesignAvgwt(double designAvgwt) {
        this.designAvgwt = designAvgwt;
    }

    public Integer getDesignAvgdays() {
        return designAvgdays;
    }

    public void setDesignAvgdays(Integer designAvgdays) {
        this.designAvgdays = designAvgdays;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public Integer getDesignAvgdays1() {
        return designAvgdays1;
    }

    public void setDesignAvgdays1(Integer designAvgdays1) {
        this.designAvgdays1 = designAvgdays1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (catID == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(catID);
        }
        parcel.writeString(catName);
        if (designID == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(designID);
        }
        parcel.writeString(designDesignNo);
        if (colourID == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(colourID);
        }
        parcel.writeString(colourName);
        if (sizeID == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(sizeID);
        }
        parcel.writeString(sizeValue);
            parcel.writeDouble(designAvgwt);

        if (designAvgdays == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(designAvgdays);
        }
      /*  if (rate == null) {
            parcel.writeDouble((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(rate);
        }*/
        parcel.writeDouble(rate);
        if (designAvgdays1 == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(designAvgdays1);
        }
        parcel.writeInt(qty);
    }
}