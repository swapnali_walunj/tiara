package com.tiara.ui.model.Response;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tiara.db.Converters;

import java.util.List;


public class LiveStockOrderResponse implements Parcelable {

    @SerializedName("status")
    @Expose
    private Boolean status;


    @SerializedName("data")
    @Expose
    private List<LiveStockOrder> data = null;


    public LiveStockOrderResponse() {
    }


    public LiveStockOrderResponse(Boolean status, List<LiveStockOrder> data) {
        super();
        this.status = status;
        this.data = data;
    }

    protected LiveStockOrderResponse(Parcel in) {
        byte tmpStatus = in.readByte();
        status = tmpStatus == 0 ? null : tmpStatus == 1;
        data = in.createTypedArrayList(LiveStockOrder.CREATOR);
    }

    public static final Creator<LiveStockOrderResponse> CREATOR = new Creator<LiveStockOrderResponse>() {
        @Override
        public LiveStockOrderResponse createFromParcel(Parcel in) {
            return new LiveStockOrderResponse(in);
        }

        @Override
        public LiveStockOrderResponse[] newArray(int size) {
            return new LiveStockOrderResponse[size];
        }
    };

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<LiveStockOrder> getData() {
        return data;
    }

    public void setData(List<LiveStockOrder> data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (status == null ? 0 : status ? 1 : 2));
        parcel.writeTypedList(data);
    }
}



