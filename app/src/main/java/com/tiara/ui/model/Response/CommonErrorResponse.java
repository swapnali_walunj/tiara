package com.tiara.ui.model.Response;

/**
 * Created by itrs-203 on 12/22/17.
 */

public class CommonErrorResponse {
    public int code;
    public String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
