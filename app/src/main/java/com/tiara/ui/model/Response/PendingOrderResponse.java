package com.tiara.ui.model.Response;

        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class PendingOrderResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public PendingOrderResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public PendingOrderResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("Cust_Del")
        @Expose
        private Boolean custDel;
        @SerializedName("STO_Number")
        @Expose
        private String sTONumber;
        @SerializedName("RowNumber")
        @Expose
        private Integer rowNumber;
        @SerializedName("STO_Date")
        @Expose
        private String sTODate;
        @SerializedName("STO_CustID")
        @Expose
        private Integer sTOCustID;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param custDel
         * @param custName
         * @param rowNumber
         * @param sTONumber
         * @param iD
         * @param sTOCustID
         * @param sTODate
         */
        public Datum(Integer iD, Boolean custDel, String sTONumber, Integer rowNumber, String sTODate, Integer sTOCustID, String custName) {
            super();
            this.iD = iD;
            this.custDel = custDel;
            this.sTONumber = sTONumber;
            this.rowNumber = rowNumber;
            this.sTODate = sTODate;
            this.sTOCustID = sTOCustID;
            this.custName = custName;
        }

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public Boolean getCustDel() {
            return custDel;
        }

        public void setCustDel(Boolean custDel) {
            this.custDel = custDel;
        }

        public String getSTONumber() {
            return sTONumber;
        }

        public void setSTONumber(String sTONumber) {
            this.sTONumber = sTONumber;
        }

        public Integer getRowNumber() {
            return rowNumber;
        }

        public void setRowNumber(Integer rowNumber) {
            this.rowNumber = rowNumber;
        }

        public String getSTODate() {
            return sTODate;
        }

        public void setSTODate(String sTODate) {
            this.sTODate = sTODate;
        }

        public Integer getSTOCustID() {
            return sTOCustID;
        }

        public void setSTOCustID(Integer sTOCustID) {
            this.sTOCustID = sTOCustID;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

    }

}