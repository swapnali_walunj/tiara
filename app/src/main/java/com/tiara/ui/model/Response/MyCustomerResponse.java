package com.tiara.ui.model.Response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyCustomerResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public MyCustomerResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public MyCustomerResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("Cust_Email")
        @Expose
        private String custEmail;
        @SerializedName("Cust_ContactNo")
        @Expose
        private String custContactNo;
        @SerializedName("Cust_Address")
        @Expose
        private String custAddress;
        @SerializedName("Cust_ContPerson")
        @Expose
        private String custContPerson;
        @SerializedName("Cust_Mobile")
        @Expose
        private String custMobile;
        @SerializedName("Cust_CustStatus")
        @Expose
        private String custCustStatus;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param custAddress
         * @param custContPerson
         * @param custEmail
         * @param custCustStatus
         * @param custName
         * @param custMobile
         * @param custContactNo
         * @param iD
         */
        public Datum(Integer iD, String custName, String custEmail, String custContactNo, String custAddress, String custContPerson, String custMobile, String custCustStatus) {
            super();
            this.iD = iD;
            this.custName = custName;
            this.custEmail = custEmail;
            this.custContactNo = custContactNo;
            this.custAddress = custAddress;
            this.custContPerson = custContPerson;
            this.custMobile = custMobile;
            this.custCustStatus = custCustStatus;
        }

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public String getCustEmail() {
            return custEmail;
        }

        public void setCustEmail(String custEmail) {
            this.custEmail = custEmail;
        }

        public String getCustContactNo() {
            return custContactNo;
        }

        public void setCustContactNo(String custContactNo) {
            this.custContactNo = custContactNo;
        }

        public String getCustAddress() {
            return custAddress;
        }

        public void setCustAddress(String custAddress) {
            this.custAddress = custAddress;
        }

        public String getCustContPerson() {
            return custContPerson;
        }

        public void setCustContPerson(String custContPerson) {
            this.custContPerson = custContPerson;
        }

        public String getCustMobile() {
            return custMobile;
        }

        public void setCustMobile(String custMobile) {
            this.custMobile = custMobile;
        }

        public String getCustCustStatus() {
            return custCustStatus;
        }

        public void setCustCustStatus(String custCustStatus) {
            this.custCustStatus = custCustStatus;
        }

    }

}
