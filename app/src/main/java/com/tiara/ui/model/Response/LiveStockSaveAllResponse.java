package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LiveStockSaveAllResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Object> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public LiveStockSaveAllResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public LiveStockSaveAllResponse(Boolean status, List<Object> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

}
