package com.tiara.ui.model;

/**
 * Created by itrs-203 on 12/7/17.
 */

public class NavigationModel {

    String navigationTitle;
    int navigationDrawable;
    boolean navigationSelected;

    public NavigationModel(String navigationTitle, int navigationDrawable, boolean navigationSelected) {
        this.navigationTitle = navigationTitle;
        this.navigationDrawable = navigationDrawable;
        this.navigationSelected = navigationSelected;
    }

    public String getNavigationTitle() {
        return navigationTitle;
    }

    public void setNavigationTitle(String navigationTitle) {
        this.navigationTitle = navigationTitle;
    }

    public int getNavigationDrawable() {
        return navigationDrawable;
    }

    public void setNavigationDrawable(int navigationDrawable) {
        this.navigationDrawable = navigationDrawable;
    }

    public boolean isNavigationSelected() {
        return navigationSelected;
    }

    public void setNavigationSelected(boolean navigationSelected) {
        this.navigationSelected = navigationSelected;
    }
}
