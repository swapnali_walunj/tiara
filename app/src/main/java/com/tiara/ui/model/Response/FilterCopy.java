package com.tiara.ui.model.Response;

public class FilterCopy {
    public String colorid;
    public String colorname;

    public String getColorid() {
        return colorid;
    }

    public void setColorid(String colorid) {
        this.colorid = colorid;
    }

    public String getColorname() {
        return colorname;
    }

    public void setColorname(String colorname) {
        this.colorname = colorname;
    }

    public String getSizeid() {
        return Sizeid;
    }

    public void setSizeid(String sizeid) {
        Sizeid = sizeid;
    }

    public String getSizename() {
        return Sizename;
    }

    public void setSizename(String sizename) {
        Sizename = sizename;
    }

    public String Sizeid;
    public String Sizename;


    public void clearFilter() {
        colorid = "";
        colorname = "";
        Sizeid = "";
        Sizename = "";

    }


}
