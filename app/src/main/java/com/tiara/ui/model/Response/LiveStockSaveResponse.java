package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LiveStockSaveResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public LiveStockSaveResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public LiveStockSaveResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("STOID")
        @Expose
        private String sTOID;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param sTOID
         */
        public Datum(String sTOID) {
            super();
            this.sTOID = sTOID;
        }

        public String getSTOID() {
            return sTOID;
        }

        public void setSTOID(String sTOID) {
            this.sTOID = sTOID;
        }

    }


}
