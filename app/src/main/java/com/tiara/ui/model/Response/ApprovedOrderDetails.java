package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApprovedOrderDetails {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     */
    public ApprovedOrderDetails() {
    }

    /**
     * @param status
     * @param data
     */
    public ApprovedOrderDetails(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("SO_ID")
        @Expose
        private Integer sOID;
        @SerializedName("SO_Number")
        @Expose
        private String sONumber;
        @SerializedName("SO_Date")
        @Expose
        private String sODate;
        @SerializedName("So_OrderType")
        @Expose
        private String soOrderType;
        @SerializedName("SO_CustID")
        @Expose
        private Integer sOCustID;
        @SerializedName("SO_VAT")
        @Expose
        private Double sOVAT;
        @SerializedName("SOD_ID")
        @Expose
        private Integer sODID;
        @SerializedName("txtrate")
        @Expose
        private Double txtrate;
        @SerializedName("txtweight")
        @Expose
        private Double txtweight;
        @SerializedName("Address_Address")
        @Expose
        private String addressAddress;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("txtlabourcharge")
        @Expose
        private Double txtlabourcharge;
        @SerializedName("txtserialno")
        @Expose
        private Integer txtserialno;
        @SerializedName("PD_ID")
        @Expose
        private Integer pDID;
        @SerializedName("lblCategory")
        @Expose
        private String lblCategory;
        @SerializedName("lblDesign")
        @Expose
        private String lblDesign;
        @SerializedName("lbltotalamt")
        @Expose
        private Double lbltotalamt;
        @SerializedName("SO_CustAddressID")
        @Expose
        private Integer sOCustAddressID;
        @SerializedName("PD_Weight")
        @Expose
        private Double pDWeight;

        /**
         * No args constructor for use in serialization
         */
        public Datum() {
        }

        /**
         * @param pDWeight
         * @param txtrate
         * @param txtlabourcharge
         * @param lblCategory
         * @param sOID
         * @param sODID
         * @param addressAddress
         * @param txtweight
         * @param lbltotalamt
         * @param sONumber
         * @param sOVAT
         * @param sODate
         * @param txtserialno
         * @param sOCustAddressID
         * @param pDID
         * @param custName
         * @param sOCustID
         * @param soOrderType
         * @param lblDesign
         */
        public Datum(Integer sOID, String sONumber, String sODate, String soOrderType, Integer sOCustID, Double sOVAT, Integer sODID, Double txtrate, Double txtweight, String addressAddress, String custName, Double txtlabourcharge, Integer txtserialno, Integer pDID, String lblCategory, String lblDesign, Double lbltotalamt, Integer sOCustAddressID, Double pDWeight) {
            super();
            this.sOID = sOID;
            this.sONumber = sONumber;
            this.sODate = sODate;
            this.soOrderType = soOrderType;
            this.sOCustID = sOCustID;
            this.sOVAT = sOVAT;
            this.sODID = sODID;
            this.txtrate = txtrate;
            this.txtweight = txtweight;
            this.addressAddress = addressAddress;
            this.custName = custName;
            this.txtlabourcharge = txtlabourcharge;
            this.txtserialno = txtserialno;
            this.pDID = pDID;
            this.lblCategory = lblCategory;
            this.lblDesign = lblDesign;
            this.lbltotalamt = lbltotalamt;
            this.sOCustAddressID = sOCustAddressID;
            this.pDWeight = pDWeight;
        }

        public Integer getSOID() {
            return sOID;
        }

        public void setSOID(Integer sOID) {
            this.sOID = sOID;
        }

        public String getSONumber() {
            return sONumber;
        }

        public void setSONumber(String sONumber) {
            this.sONumber = sONumber;
        }

        public String getSODate() {
            return sODate;
        }

        public void setSODate(String sODate) {
            this.sODate = sODate;
        }

        public String getSoOrderType() {
            return soOrderType;
        }

        public void setSoOrderType(String soOrderType) {
            this.soOrderType = soOrderType;
        }

        public Integer getSOCustID() {
            return sOCustID;
        }

        public void setSOCustID(Integer sOCustID) {
            this.sOCustID = sOCustID;
        }

        public Double getSOVAT() {
            return sOVAT;
        }

        public void setSOVAT(Double sOVAT) {
            this.sOVAT = sOVAT;
        }

        public Integer getSODID() {
            return sODID;
        }

        public void setSODID(Integer sODID) {
            this.sODID = sODID;
        }

        public Double getTxtrate() {
            return txtrate;
        }

        public void setTxtrate(Double txtrate) {
            this.txtrate = txtrate;
        }

        public Double getTxtweight() {
            return txtweight;
        }

        public void setTxtweight(Double txtweight) {
            this.txtweight = txtweight;
        }

        public String getAddressAddress() {
            return addressAddress;
        }

        public void setAddressAddress(String addressAddress) {
            this.addressAddress = addressAddress;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public Double getTxtlabourcharge() {
            return txtlabourcharge;
        }

        public void setTxtlabourcharge(Double txtlabourcharge) {
            this.txtlabourcharge = txtlabourcharge;
        }

        public Integer getTxtserialno() {
            return txtserialno;
        }

        public void setTxtserialno(Integer txtserialno) {
            this.txtserialno = txtserialno;
        }

        public Integer getPDID() {
            return pDID;
        }

        public void setPDID(Integer pDID) {
            this.pDID = pDID;
        }

        public String getLblCategory() {
            return lblCategory;
        }

        public void setLblCategory(String lblCategory) {
            this.lblCategory = lblCategory;
        }

        public String getLblDesign() {
            return lblDesign;
        }

        public void setLblDesign(String lblDesign) {
            this.lblDesign = lblDesign;
        }

        public Double getLbltotalamt() {
            return lbltotalamt;
        }

        public void setLbltotalamt(Double lbltotalamt) {
            this.lbltotalamt = lbltotalamt;
        }

        public Integer getSOCustAddressID() {
            return sOCustAddressID;
        }

        public void setSOCustAddressID(Integer sOCustAddressID) {
            this.sOCustAddressID = sOCustAddressID;
        }

        public Double getPDWeight() {
            return pDWeight;
        }

        public void setPDWeight(Double pDWeight) {
            this.pDWeight = pDWeight;
        }

    }
}