package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RateResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public RateResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public RateResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("Rate_Value")
        @Expose
        private Double rateValue;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param rateValue
         */
        public Datum(Double rateValue) {
            super();
            this.rateValue = rateValue;
        }

        public Double getRateValue() {
            return rateValue;
        }

        public void setRateValue(Double rateValue) {
            this.rateValue = rateValue;
        }

    }

}