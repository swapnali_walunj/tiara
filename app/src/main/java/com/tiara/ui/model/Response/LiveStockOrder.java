package com.tiara.ui.model.Response;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tiara.db.Converters;

@Entity(tableName = "livestock_order")
@TypeConverters(Converters.class)
public class LiveStockOrder implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    private Integer id;

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    @SerializedName("Design_ID")
    @Expose
    private Integer designID;
    @SerializedName("Cat_Name")
    @Expose
    private String catName;
    @SerializedName("Design_DesignNo")
    @Expose
    private String designDesignNo;
    @SerializedName("Colour_Name")
    @Expose
    private String colourName;
    @SerializedName("Size_Value")
    @Expose
    private String sizeValue;
    @SerializedName("AvgWeight")
    @Expose
    private Double avgWeight;
    @SerializedName("PD_PDStatus")
    @Expose
    private String pDPDStatus;
    @SerializedName("NoOfStock")
    @Expose
    private Integer noOfStock;
    @SerializedName("Cat_ID")
    @Expose
    private Integer catID;

    @SerializedName("Colour_ID")
    @Expose
    private Integer colourID;
    @SerializedName("Size_ID")
    @Expose
    private Integer sizeID;


    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    private Integer qty;

    /**
     * No args constructor for use in serialization
     *
     */
    @Ignore
    public LiveStockOrder() {
    }

    /**
     *
     * @param designDesignNo
     * @param designID
     * @param catID
     * @param pDPDStatus
     * @param catName
     * @param avgWeight
     * @param colourID
     * @param noOfStock
     * @param sizeID
     * @param sizeValue
     * @param colourName
     */
    public LiveStockOrder(Integer id,String catName, String designDesignNo, String colourName, String sizeValue, Double avgWeight, String pDPDStatus, Integer noOfStock, Integer catID, Integer designID, Integer colourID, Integer sizeID,Integer qty) {
        super();
        this.id=id;
        this.catName = catName;
        this.designDesignNo = designDesignNo;
        this.colourName = colourName;
        this.sizeValue = sizeValue;
        this.avgWeight = avgWeight;
        this.pDPDStatus = pDPDStatus;
        this.noOfStock = noOfStock;
        this.catID = catID;
        this.designID = designID;
        this.colourID = colourID;
        this.sizeID = sizeID;
        this.qty=qty;
    }

    protected LiveStockOrder(Parcel in) {
        id=in.readInt();
        catName = in.readString();
        designDesignNo = in.readString();
        colourName = in.readString();
        sizeValue = in.readString();
        avgWeight = in.readDouble();
        pDPDStatus = in.readString();
        noOfStock = in.readInt();
        catID = in.readInt();
        designID = in.readInt();
        colourID = in.readInt();
        sizeID = in.readInt();
        qty=in.readInt();

    }

    public static final Creator<LiveStockOrder> CREATOR = new Creator<LiveStockOrder>() {
        @Override
        public LiveStockOrder createFromParcel(Parcel in) {
            return new LiveStockOrder(in);
        }

        @Override
        public LiveStockOrder[] newArray(int size) {
            return new LiveStockOrder[size];
        }
    };

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getDesignDesignNo() {
        return designDesignNo;
    }

    public void setDesignDesignNo(String designDesignNo) {
        this.designDesignNo = designDesignNo;
    }

    public String getColourName() {
        return colourName;
    }

    public void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public Double getAvgWeight() {
        return avgWeight;
    }

    public void setAvgWeight(Double avgWeight) {
        this.avgWeight = avgWeight;
    }

    public String getPDPDStatus() {
        return pDPDStatus;
    }

    public void setPDPDStatus(String pDPDStatus) {
        this.pDPDStatus = pDPDStatus;
    }

    public Integer getNoOfStock() {
        return noOfStock;
    }

    public void setNoOfStock(Integer noOfStock) {
        this.noOfStock = noOfStock;
    }

    public Integer getCatID() {
        return catID;
    }

    public void setCatID(Integer catID) {
        this.catID = catID;
    }

    public Integer getDesignID() {
        return designID;
    }

    public void setDesignID(Integer designID) {
        this.designID = designID;
    }

    public Integer getColourID() {
        return colourID;
    }

    public void setColourID(Integer colourID) {
        this.colourID = colourID;
    }

    public Integer getSizeID() {
        return sizeID;
    }

    public void setSizeID(Integer sizeID) {
        this.sizeID = sizeID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(catName);
        parcel.writeString(designDesignNo);
        parcel.writeString(colourName);
        parcel.writeString(sizeValue);
        parcel.writeDouble(avgWeight);
        parcel.writeString(pDPDStatus);
        parcel.writeInt(noOfStock);
        parcel.writeInt(catID);
        parcel.writeInt(designID);
        parcel.writeInt(colourID);
        parcel.writeInt(sizeID);
        parcel.writeInt(qty);

    }
}