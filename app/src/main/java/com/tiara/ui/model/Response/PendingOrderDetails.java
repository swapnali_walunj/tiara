package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PendingOrderDetails {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public PendingOrderDetails() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public PendingOrderDetails(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("STO_Date")
        @Expose
        private String sTODate;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("Address_Address")
        @Expose
        private String addressAddress;
        @SerializedName("STO_CustID")
        @Expose
        private Integer sTOCustID;
        @SerializedName("STO_CustAddressID")
        @Expose
        private Integer sTOCustAddressID;
        @SerializedName("STOD_ID")
        @Expose
        private Integer sTODID;
        @SerializedName("STOD_STOID")
        @Expose
        private Integer sTODSTOID;
        @SerializedName("STOD_Rate")
        @Expose
        private Double sTODRate;
        @SerializedName("PD_ID")
        @Expose
        private Integer pDID;
        @SerializedName("PD_SerialNo")
        @Expose
        private Integer pDSerialNo;
        @SerializedName("PD_Weight")
        @Expose
        private Double pDWeight;
        @SerializedName("PD_PDStatus")
        @Expose
        private String pDPDStatus;
        @SerializedName("Cat_ID")
        @Expose
        private Integer catID;
        @SerializedName("Cat_Name")
        @Expose
        private String catName;
        @SerializedName("Design_ID")
        @Expose
        private Integer designID;
        @SerializedName("Design_DesignNo")
        @Expose
        private String designDesignNo;
        @SerializedName("Colour_ID")
        @Expose
        private Integer colourID;
        @SerializedName("Colour_Name")
        @Expose
        private String colourName;
        @SerializedName("Size_ID")
        @Expose
        private Integer sizeID;
        @SerializedName("Size_Value")
        @Expose
        private String sizeValue;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param sTODRate
         * @param pDWeight
         * @param designID
         * @param pDSerialNo
         * @param pDPDStatus
         * @param colourID
         * @param addressAddress
         * @param sizeValue
         * @param sTOCustAddressID
         * @param designDesignNo
         * @param catID
         * @param sTODID
         * @param catName
         * @param sTODSTOID
         * @param custName
         * @param pDID
         * @param sizeID
         * @param colourName
         * @param sTOCustID
         * @param sTODate
         */
        public Datum(String sTODate, String custName, String addressAddress, Integer sTOCustID, Integer sTOCustAddressID, Integer sTODID, Integer sTODSTOID, Double sTODRate, Integer pDID, Integer pDSerialNo, Double pDWeight, String pDPDStatus, Integer catID, String catName, Integer designID, String designDesignNo, Integer colourID, String colourName, Integer sizeID, String sizeValue) {
            super();
            this.sTODate = sTODate;
            this.custName = custName;
            this.addressAddress = addressAddress;
            this.sTOCustID = sTOCustID;
            this.sTOCustAddressID = sTOCustAddressID;
            this.sTODID = sTODID;
            this.sTODSTOID = sTODSTOID;
            this.sTODRate = sTODRate;
            this.pDID = pDID;
            this.pDSerialNo = pDSerialNo;
            this.pDWeight = pDWeight;
            this.pDPDStatus = pDPDStatus;
            this.catID = catID;
            this.catName = catName;
            this.designID = designID;
            this.designDesignNo = designDesignNo;
            this.colourID = colourID;
            this.colourName = colourName;
            this.sizeID = sizeID;
            this.sizeValue = sizeValue;
        }

        public String getSTODate() {
            return sTODate;
        }

        public void setSTODate(String sTODate) {
            this.sTODate = sTODate;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public String getAddressAddress() {
            return addressAddress;
        }

        public void setAddressAddress(String addressAddress) {
            this.addressAddress = addressAddress;
        }

        public Integer getSTOCustID() {
            return sTOCustID;
        }

        public void setSTOCustID(Integer sTOCustID) {
            this.sTOCustID = sTOCustID;
        }

        public Integer getSTOCustAddressID() {
            return sTOCustAddressID;
        }

        public void setSTOCustAddressID(Integer sTOCustAddressID) {
            this.sTOCustAddressID = sTOCustAddressID;
        }

        public Integer getSTODID() {
            return sTODID;
        }

        public void setSTODID(Integer sTODID) {
            this.sTODID = sTODID;
        }

        public Integer getSTODSTOID() {
            return sTODSTOID;
        }

        public void setSTODSTOID(Integer sTODSTOID) {
            this.sTODSTOID = sTODSTOID;
        }

        public Double getSTODRate() {
            return sTODRate;
        }

        public void setSTODRate(Double sTODRate) {
            this.sTODRate = sTODRate;
        }

        public Integer getPDID() {
            return pDID;
        }

        public void setPDID(Integer pDID) {
            this.pDID = pDID;
        }

        public Integer getPDSerialNo() {
            return pDSerialNo;
        }

        public void setPDSerialNo(Integer pDSerialNo) {
            this.pDSerialNo = pDSerialNo;
        }

        public Double getPDWeight() {
            return pDWeight;
        }

        public void setPDWeight(Double pDWeight) {
            this.pDWeight = pDWeight;
        }

        public String getPDPDStatus() {
            return pDPDStatus;
        }

        public void setPDPDStatus(String pDPDStatus) {
            this.pDPDStatus = pDPDStatus;
        }

        public Integer getCatID() {
            return catID;
        }

        public void setCatID(Integer catID) {
            this.catID = catID;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public Integer getDesignID() {
            return designID;
        }

        public void setDesignID(Integer designID) {
            this.designID = designID;
        }

        public String getDesignDesignNo() {
            return designDesignNo;
        }

        public void setDesignDesignNo(String designDesignNo) {
            this.designDesignNo = designDesignNo;
        }

        public Integer getColourID() {
            return colourID;
        }

        public void setColourID(Integer colourID) {
            this.colourID = colourID;
        }

        public String getColourName() {
            return colourName;
        }

        public void setColourName(String colourName) {
            this.colourName = colourName;
        }

        public Integer getSizeID() {
            return sizeID;
        }

        public void setSizeID(Integer sizeID) {
            this.sizeID = sizeID;
        }

        public String getSizeValue() {
            return sizeValue;
        }

        public void setSizeValue(String sizeValue) {
            this.sizeValue = sizeValue;
        }

    }

}
