package com.tiara.ui.model.Response;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerLoginRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     */
    public CustomerLoginRes() {
    }

    /**
     * @param status
     * @param data
     */
    public CustomerLoginRes(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("Cust_ID")
        @Expose
        private Integer custID;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("Cust_Mobile")
        @Expose
        private String custMobile;
        @SerializedName("Cust_Logo")
        @Expose
        private String custLogo;
        @SerializedName("IsOTP")
        @Expose
        private Boolean isOTP;
        @SerializedName("Company_ID")
        @Expose
        private Integer companyID;
        @SerializedName("Cust_CompID")
        @Expose
        private Integer custCompID;

        /**
         * No args constructor for use in serialization
         */
        public Datum() {
        }

        /**
         * @param custLogo
         * @param custID
         * @param companyID
         * @param isOTP
         * @param custName
         * @param custMobile
         * @param custCompID
         */
        public Datum(Integer custID, String custName, String custMobile, String custLogo, Boolean isOTP, Integer companyID, Integer custCompID) {
            super();
            this.custID = custID;
            this.custName = custName;
            this.custMobile = custMobile;
            this.custLogo = custLogo;
            this.isOTP = isOTP;
            this.companyID = companyID;
            this.custCompID = custCompID;
        }

        public Integer getCustID() {
            return custID;
        }

        public void setCustID(Integer custID) {
            this.custID = custID;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public String getCustMobile() {
            return custMobile;
        }

        public void setCustMobile(String custMobile) {
            this.custMobile = custMobile;
        }

        public String getCustLogo() {
            return custLogo;
        }

        public void setCustLogo(String custLogo) {
            this.custLogo = custLogo;
        }

        public Boolean getIsOTP() {
            return isOTP;
        }

        public void setIsOTP(Boolean isOTP) {
            this.isOTP = isOTP;
        }

        public Integer getCompanyID() {
            return companyID;
        }

        public void setCompanyID(Integer companyID) {
            this.companyID = companyID;
        }

        public Integer getCustCompID() {
            return custCompID;
        }

        public void setCustCompID(Integer custCompID) {
            this.custCompID = custCompID;
        }

    }
}
