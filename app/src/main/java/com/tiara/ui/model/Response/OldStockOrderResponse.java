package com.tiara.ui.model.Response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;



public class OldStockOrderResponse implements Parcelable {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<OldStockOrder> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public OldStockOrderResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public OldStockOrderResponse(Boolean status, List<OldStockOrder> data) {
        super();
        this.status = status;
        this.data = data;
    }

    protected OldStockOrderResponse(Parcel in) {
        byte tmpStatus = in.readByte();
        status = tmpStatus == 0 ? null : tmpStatus == 1;
        data = in.createTypedArrayList(OldStockOrder.CREATOR);
    }

    public static final Creator<OldStockOrderResponse> CREATOR = new Creator<OldStockOrderResponse>() {
        @Override
        public OldStockOrderResponse createFromParcel(Parcel in) {
            return new OldStockOrderResponse(in);
        }

        @Override
        public OldStockOrderResponse[] newArray(int size) {
            return new OldStockOrderResponse[size];
        }
    };

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<OldStockOrder> getData() {
        return data;
    }

    public void setData(List<OldStockOrder> data) {
        this.data = data;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (status == null ? 0 : status ? 1 : 2));
        parcel.writeTypedList(data);
    }
}



