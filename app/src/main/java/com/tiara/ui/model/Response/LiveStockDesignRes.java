package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LiveStockDesignRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public LiveStockDesignRes() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public LiveStockDesignRes(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {

        @SerializedName("Design_ID")
        @Expose
        private Integer designID;
        @SerializedName("Design_DesignNo")
        @Expose
        private String designDesignNo;
        @SerializedName("Design_CatID")
        @Expose
        private Integer designCatID;
        @SerializedName("Design_Age")
        @Expose
        private String designAge;
        @SerializedName("Image_Path")
        @Expose
        private String imagePath;
        @SerializedName("Image_Name")
        @Expose
        private String imageName;
        @SerializedName("ImageCount")
        @Expose
        private Integer imageCount;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param designDesignNo
         * @param designAge
         * @param imageName
         * @param imagePath
         * @param designID
         * @param imageCount
         * @param designCatID
         */
        public Datum(Integer designID, String designDesignNo, Integer designCatID, String designAge, String imagePath, String imageName, Integer imageCount) {
            super();
            this.designID = designID;
            this.designDesignNo = designDesignNo;
            this.designCatID = designCatID;
            this.designAge = designAge;
            this.imagePath = imagePath;
            this.imageName = imageName;
            this.imageCount = imageCount;
        }

        public Integer getDesignID() {
            return designID;
        }

        public void setDesignID(Integer designID) {
            this.designID = designID;
        }

        public String getDesignDesignNo() {
            return designDesignNo;
        }

        public void setDesignDesignNo(String designDesignNo) {
            this.designDesignNo = designDesignNo;
        }

        public Integer getDesignCatID() {
            return designCatID;
        }

        public void setDesignCatID(Integer designCatID) {
            this.designCatID = designCatID;
        }

        public String getDesignAge() {
            return designAge;
        }

        public void setDesignAge(String designAge) {
            this.designAge = designAge;
        }

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public Integer getImageCount() {
            return imageCount;
        }

        public void setImageCount(Integer imageCount) {
            this.imageCount = imageCount;
        }

    }
}
