package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApprovedOrderResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     */
    public ApprovedOrderResponse() {
    }

    /**
     * @param status
     * @param data
     */
    public ApprovedOrderResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public static class Datum {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("SO_Number")
        @Expose
        private String sONumber;
        @SerializedName("RowNumber")
        @Expose
        private Integer rowNumber;
        @SerializedName("SO_Date")
        @Expose
        private String sODate;
        @SerializedName("SOD_Weight")
        @Expose
        private Double sODWeight;
        @SerializedName("GrandTotal")
        @Expose
        private Double grandTotal;
        @SerializedName("InvId")
        @Expose
        private Integer invId;

        /**
         * No args constructor for use in serialization
         */
        public Datum() {
        }

        /**
         * @param sODWeight
         * @param sONumber
         * @param sODate
         * @param grandTotal
         * @param invId
         * @param rowNumber
         * @param custName
         * @param iD
         */
        public Datum(Integer iD, String custName, String sONumber, Integer rowNumber, String sODate, Double sODWeight, Double grandTotal, Integer invId) {
            super();
            this.iD = iD;
            this.custName = custName;
            this.sONumber = sONumber;
            this.rowNumber = rowNumber;
            this.sODate = sODate;
            this.sODWeight = sODWeight;
            this.grandTotal = grandTotal;
            this.invId = invId;
        }

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public String getSONumber() {
            return sONumber;
        }

        public void setSONumber(String sONumber) {
            this.sONumber = sONumber;
        }

        public Integer getRowNumber() {
            return rowNumber;
        }

        public void setRowNumber(Integer rowNumber) {
            this.rowNumber = rowNumber;
        }

        public String getSODate() {
            return sODate;
        }

        public void setSODate(String sODate) {
            this.sODate = sODate;
        }

        public Double getSODWeight() {
            return sODWeight;
        }

        public void setSODWeight(Double sODWeight) {
            this.sODWeight = sODWeight;
        }

        public Double getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(Double grandTotal) {
            this.grandTotal = grandTotal;
        }

        public Integer getInvId() {
            return invId;
        }

        public void setInvId(Integer invId) {
            this.invId = invId;
        }

    }
}