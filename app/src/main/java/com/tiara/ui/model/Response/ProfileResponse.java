package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProfileResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public ProfileResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("Cust_ID")
        @Expose
        private Integer custID;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("Cust_Email")
        @Expose
        private String custEmail;
        @SerializedName("Cust_Password")
        @Expose
        private String custPassword;
        @SerializedName("Cust_ContactNo")
        @Expose
        private String custContactNo;
        @SerializedName("Cust_Address")
        @Expose
        private String custAddress;
        @SerializedName("Cust_ContPerson")
        @Expose
        private String custContPerson;
        @SerializedName("Cust_Mobile")
        @Expose
        private String custMobile;
        @SerializedName("Cust_Status")
        @Expose
        private String custStatus;
        @SerializedName("Cust_VatTin")
        @Expose
        private String custVatTin;
        @SerializedName("Cust_CstTin")
        @Expose
        private String custCstTin;
        @SerializedName("Cust_SalesPID")
        @Expose
        private Integer custSalesPID;
        @SerializedName("Cust_Logo")
        @Expose
        private String custLogo;
        @SerializedName("Cust_Logo1")
        @Expose
        private String custLogo1;
        @SerializedName("Cust_CustStatus")
        @Expose
        private String custCustStatus;
        @SerializedName("Cust_GSTNo")
        @Expose
        private String custGSTNo;
        @SerializedName("Cust_StateId")
        @Expose
        private Integer custStateId;
        @SerializedName("IsOTP")
        @Expose
        private Boolean isOTP;
        @SerializedName("Cust_ApprovEmail")
        @Expose
        private String custApprovEmail;
        @SerializedName("Cust_CompID")
        @Expose
        private Integer custCompID;

        public String getState_Name() {
            return State_Name;
        }

        public void setState_Name(String state_Name) {
            State_Name = state_Name;
        }

        private String State_Name;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param custStateId
         * @param custLogo
         * @param custVatTin
         * @param custAddress
         * @param custSalesPID
         * @param custContPerson
         * @param custID
         * @param custCustStatus
         * @param isOTP
         * @param custContactNo
         * @param custCstTin
         * @param custGSTNo
         * @param custEmail
         * @param custName
         * @param custPassword
         * @param custApprovEmail
         * @param custMobile
         * @param custStatus
         * @param custCompID
         * @param custLogo1
         */
        public Datum(Integer custID, String custName, String custEmail, String custPassword, String custContactNo, String custAddress, String custContPerson, String custMobile, String custStatus, String custVatTin, String custCstTin, Integer custSalesPID, String custLogo, String custLogo1, String custCustStatus, String custGSTNo, Integer custStateId, Boolean isOTP, String custApprovEmail, Integer custCompID,String State_Name) {
            super();
            this.custID = custID;
            this.custName = custName;
            this.custEmail = custEmail;
            this.custPassword = custPassword;
            this.custContactNo = custContactNo;
            this.custAddress = custAddress;
            this.custContPerson = custContPerson;
            this.custMobile = custMobile;
            this.custStatus = custStatus;
            this.custVatTin = custVatTin;
            this.custCstTin = custCstTin;
            this.custSalesPID = custSalesPID;
            this.custLogo = custLogo;
            this.custLogo1 = custLogo1;
            this.custCustStatus = custCustStatus;
            this.custGSTNo = custGSTNo;
            this.custStateId = custStateId;
            this.isOTP = isOTP;
            this.custApprovEmail = custApprovEmail;
            this.custCompID = custCompID;
            this.State_Name=State_Name;
        }

        public Integer getCustID() {
            return custID;
        }

        public void setCustID(Integer custID) {
            this.custID = custID;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public String getCustEmail() {
            return custEmail;
        }

        public void setCustEmail(String custEmail) {
            this.custEmail = custEmail;
        }

        public String getCustPassword() {
            return custPassword;
        }

        public void setCustPassword(String custPassword) {
            this.custPassword = custPassword;
        }

        public String getCustContactNo() {
            return custContactNo;
        }

        public void setCustContactNo(String custContactNo) {
            this.custContactNo = custContactNo;
        }

        public String getCustAddress() {
            return custAddress;
        }

        public void setCustAddress(String custAddress) {
            this.custAddress = custAddress;
        }

        public String getCustContPerson() {
            return custContPerson;
        }

        public void setCustContPerson(String custContPerson) {
            this.custContPerson = custContPerson;
        }

        public String getCustMobile() {
            return custMobile;
        }

        public void setCustMobile(String custMobile) {
            this.custMobile = custMobile;
        }

        public String getCustStatus() {
            return custStatus;
        }

        public void setCustStatus(String custStatus) {
            this.custStatus = custStatus;
        }

        public String getCustVatTin() {
            return custVatTin;
        }

        public void setCustVatTin(String custVatTin) {
            this.custVatTin = custVatTin;
        }

        public String getCustCstTin() {
            return custCstTin;
        }

        public void setCustCstTin(String custCstTin) {
            this.custCstTin = custCstTin;
        }

        public Integer getCustSalesPID() {
            return custSalesPID;
        }

        public void setCustSalesPID(Integer custSalesPID) {
            this.custSalesPID = custSalesPID;
        }

        public String getCustLogo() {
            return custLogo;
        }

        public void setCustLogo(String custLogo) {
            this.custLogo = custLogo;
        }

        public String getCustLogo1() {
            return custLogo1;
        }

        public void setCustLogo1(String custLogo1) {
            this.custLogo1 = custLogo1;
        }

        public String getCustCustStatus() {
            return custCustStatus;
        }

        public void setCustCustStatus(String custCustStatus) {
            this.custCustStatus = custCustStatus;
        }

        public String getCustGSTNo() {
            return custGSTNo;
        }

        public void setCustGSTNo(String custGSTNo) {
            this.custGSTNo = custGSTNo;
        }

        public Integer getCustStateId() {
            return custStateId;
        }

        public void setCustStateId(Integer custStateId) {
            this.custStateId = custStateId;
        }

        public Boolean getIsOTP() {
            return isOTP;
        }

        public void setIsOTP(Boolean isOTP) {
            this.isOTP = isOTP;
        }

        public String getCustApprovEmail() {
            return custApprovEmail;
        }

        public void setCustApprovEmail(String custApprovEmail) {
            this.custApprovEmail = custApprovEmail;
        }

        public Integer getCustCompID() {
            return custCompID;
        }

        public void setCustCompID(Integer custCompID) {
            this.custCompID = custCompID;
        }

    }

}
