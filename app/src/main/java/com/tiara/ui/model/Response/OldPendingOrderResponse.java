package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OldPendingOrderResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public OldPendingOrderResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public OldPendingOrderResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {

        @SerializedName("Old_ODate")
        @Expose
        private String oldODate;
        @SerializedName("RowNumber")
        @Expose
        private Integer rowNumber;
        @SerializedName("Old_ONumber")
        @Expose
        private String oldONumber;
        @SerializedName("Cust_Name")
        @Expose
        private String custName;
        @SerializedName("Old_CustID")
        @Expose
        private Integer oldCustID;
        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("Cust_SalesPID")
        @Expose
        private Integer custSalesPID;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param custSalesPID
         * @param oldODate
         * @param oldONumber
         * @param custName
         * @param rowNumber
         * @param iD
         * @param oldCustID
         */
        public Datum(String oldODate, Integer rowNumber, String oldONumber, String custName, Integer oldCustID, Integer iD, Integer custSalesPID) {
            super();
            this.oldODate = oldODate;
            this.rowNumber = rowNumber;
            this.oldONumber = oldONumber;
            this.custName = custName;
            this.oldCustID = oldCustID;
            this.iD = iD;
            this.custSalesPID = custSalesPID;
        }

        public String getOldODate() {
            return oldODate;
        }

        public void setOldODate(String oldODate) {
            this.oldODate = oldODate;
        }

        public Integer getRowNumber() {
            return rowNumber;
        }

        public void setRowNumber(Integer rowNumber) {
            this.rowNumber = rowNumber;
        }

        public String getOldONumber() {
            return oldONumber;
        }

        public void setOldONumber(String oldONumber) {
            this.oldONumber = oldONumber;
        }

        public String getCustName() {
            return custName;
        }

        public void setCustName(String custName) {
            this.custName = custName;
        }

        public Integer getOldCustID() {
            return oldCustID;
        }

        public void setOldCustID(Integer oldCustID) {
            this.oldCustID = oldCustID;
        }

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public Integer getCustSalesPID() {
            return custSalesPID;
        }

        public void setCustSalesPID(Integer custSalesPID) {
            this.custSalesPID = custSalesPID;
        }

    }

}