package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     */
    public AddressResponse() {
    }

    /**
     * @param status
     * @param data
     */
    public AddressResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("Address_ID")
        @Expose
        private Integer addressID;
        @SerializedName("Address_Address")
        @Expose
        private String addressAddress;

        /**
         * No args constructor for use in serialization
         */
        public Datum() {
        }

        /**
         * @param addressAddress
         * @param addressID
         */
        public Datum(Integer addressID, String addressAddress) {
            super();
            this.addressID = addressID;
            this.addressAddress = addressAddress;
        }

        public Integer getAddressID() {
            return addressID;
        }

        public void setAddressID(Integer addressID) {
            this.addressID = addressID;
        }

        public String getAddressAddress() {
            return addressAddress;
        }

        public void setAddressAddress(String addressAddress) {
            this.addressAddress = addressAddress;
        }

    }
}