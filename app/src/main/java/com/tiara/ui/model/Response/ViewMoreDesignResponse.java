package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewMoreDesignResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public ViewMoreDesignResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public ViewMoreDesignResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("Design_ID")
        @Expose
        private Integer designID;
        @SerializedName("Design_DesignNo")
        @Expose
        private String designDesignNo;
        @SerializedName("Design_CatID")
        @Expose
        private Integer designCatID;
        @SerializedName("Design_Age")
        @Expose
        private String designAge;
        @SerializedName("Image_Path")
        @Expose
        private String imagePath;
        @SerializedName("Image_Name")
        @Expose
        private String imageName;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param designDesignNo
         * @param designAge
         * @param imageName
         * @param imagePath
         * @param designID
         * @param designCatID
         */
        public Datum(Integer designID, String designDesignNo, Integer designCatID, String designAge, String imagePath, String imageName) {
            super();
            this.designID = designID;
            this.designDesignNo = designDesignNo;
            this.designCatID = designCatID;
            this.designAge = designAge;
            this.imageName = imageName;
            this.imagePath = imagePath;
        }

        public Integer getDesignID() {
            return designID;
        }

        public void setDesignID(Integer designID) {
            this.designID = designID;
        }

        public String getDesignDesignNo() {
            return designDesignNo;
        }

        public void setDesignDesignNo(String designDesignNo) {
            this.designDesignNo = designDesignNo;
        }

        public Integer getDesignCatID() {
            return designCatID;
        }

        public void setDesignCatID(Integer designCatID) {
            this.designCatID = designCatID;
        }

        public String getDesignAge() {
            return designAge;
        }

        public void setDesignAge(String designAge) {
            this.designAge = designAge;
        }

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }

        public String getImageName() {
            return imageName;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

    }

}