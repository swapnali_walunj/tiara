package com.tiara.ui.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ColorResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     */
    public ColorResponse() {
    }

    /**
     * @param status
     * @param data
     */
    public ColorResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public static class Datum {

        @SerializedName("Colour_ID")
        @Expose
        private Integer colourID;
        @SerializedName("Colour_Name")
        @Expose
        private String colourName;

        private boolean isSelected = false;

        /**
         * No args constructor for use in serialization
         */
        public Datum() {
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        /**
         * @param colourID
         * @param colourName
         */
        public Datum(Integer colourID, String colourName,boolean isSelected ) {
            super();
            this.colourID = colourID;
            this.colourName = colourName;
            this.isSelected=isSelected;
        }

        public Integer getColourID() {
            return colourID;
        }

        public void setColourID(Integer colourID) {
            this.colourID = colourID;
        }

        public String getColourName() {
            return colourName;
        }

        public void setColourName(String colourName) {
            this.colourName = colourName;
        }

    }
}