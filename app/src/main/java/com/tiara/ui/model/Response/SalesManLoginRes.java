package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
public class SalesManLoginRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public SalesManLoginRes() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public SalesManLoginRes(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("SalesP_ID")
        @Expose
        private Integer salesPID;
        @SerializedName("SalesP_Name")
        @Expose
        private String salesPName;
        @SerializedName("Company_ID")
        @Expose
        private Integer companyID;
        @SerializedName("Company_Name")
        @Expose
        private String companyName;
        @SerializedName("SalesP_Mobile")
        @Expose
        private String salesPMobile;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param salesPMobile
         * @param salesPName
         * @param salesPID
         * @param companyID
         * @param companyName
         */
        public Datum(Integer salesPID, String salesPName, Integer companyID, String companyName, String salesPMobile) {
            super();
            this.salesPID = salesPID;
            this.salesPName = salesPName;
            this.companyID = companyID;
            this.companyName = companyName;
            this.salesPMobile = salesPMobile;
        }

        public Integer getSalesPID() {
            return salesPID;
        }

        public void setSalesPID(Integer salesPID) {
            this.salesPID = salesPID;
        }

        public String getSalesPName() {
            return salesPName;
        }

        public void setSalesPName(String salesPName) {
            this.salesPName = salesPName;
        }

        public Integer getCompanyID() {
            return companyID;
        }

        public void setCompanyID(Integer companyID) {
            this.companyID = companyID;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getSalesPMobile() {
            return salesPMobile;
        }

        public void setSalesPMobile(String salesPMobile) {
            this.salesPMobile = salesPMobile;
        }

    }

}
