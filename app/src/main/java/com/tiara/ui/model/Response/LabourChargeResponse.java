package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LabourChargeResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public LabourChargeResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public LabourChargeResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {

        @SerializedName("LC_ID")
        @Expose
        private Integer lCID;
        @SerializedName("Cat_Name")
        @Expose
        private String catName;
        @SerializedName("Cat_Del")
        @Expose
        private Boolean catDel;
        @SerializedName("LC_RatePerGram")
        @Expose
        private Integer lCRatePerGram;
        @SerializedName("RowNumber")
        @Expose
        private Integer rowNumber;

        public Integer getlCID() {
            return lCID;
        }

        public void setlCID(Integer lCID) {
            this.lCID = lCID;
        }

        public Integer getlCRatePerGram() {
            return lCRatePerGram;
        }

        public void setlCRatePerGram(Integer lCRatePerGram) {
            this.lCRatePerGram = lCRatePerGram;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @SerializedName("type")
        @Expose
        private String type="Labour";

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param catDel
         * @param lCRatePerGram
         * @param lCID
         * @param catName
         * @param rowNumber
         */
        public Datum(Integer lCID, String catName, Boolean catDel, Integer lCRatePerGram, Integer rowNumber) {
            super();
            this.lCID = lCID;
            this.catName = catName;
            this.catDel = catDel;
            this.lCRatePerGram = lCRatePerGram;
            this.rowNumber = rowNumber;
        }

        public Integer getLCID() {
            return lCID;
        }

        public void setLCID(Integer lCID) {
            this.lCID = lCID;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public Boolean getCatDel() {
            return catDel;
        }

        public void setCatDel(Boolean catDel) {
            this.catDel = catDel;
        }

        public Integer getLCRatePerGram() {
            return lCRatePerGram;
        }

        public void setLCRatePerGram(Integer lCRatePerGram) {
            this.lCRatePerGram = lCRatePerGram;
        }

        public Integer getRowNumber() {
            return rowNumber;
        }

        public void setRowNumber(Integer rowNumber) {
            this.rowNumber = rowNumber;
        }

    }

}