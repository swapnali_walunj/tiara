package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SizeResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public SizeResponse() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public SizeResponse(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("Size_ID")
        @Expose
        private Integer sizeID;
        @SerializedName("Size_Value")
        @Expose
        private String sizeValue;
        private boolean isSelected;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param sizeValue
         * @param sizeID
         */
        public Datum(Integer sizeID, String sizeValue,boolean isSelected) {
            super();
            this.sizeID = sizeID;
            this.sizeValue = sizeValue;
            this.isSelected=isSelected;
        }

        public Integer getSizeID() {
            return sizeID;
        }

        public void setSizeID(Integer sizeID) {
            this.sizeID = sizeID;
        }

        public String getSizeValue() {
            return sizeValue;
        }

        public void setSizeValue(String sizeValue) {
            this.sizeValue = sizeValue;
        }

    }


}
