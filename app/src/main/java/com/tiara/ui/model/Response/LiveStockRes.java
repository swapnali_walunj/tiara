package com.tiara.ui.model.Response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;




public class LiveStockRes {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public LiveStockRes() {
    }

    /**
     *
     * @param status
     * @param data
     */
    public LiveStockRes(Boolean status, List<Datum> data) {
        super();
        this.status = status;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum {

        @SerializedName("ID")
        @Expose
        private Integer iD;
        @SerializedName("Cat_Name")
        @Expose
        private String catName;
        @SerializedName("Cat_Del")
        @Expose
        private Boolean catDel;
        @SerializedName("Cat_Image")
        @Expose
        private String catImage;
        @SerializedName("RowNumber")
        @Expose
        private Integer rowNumber;

        /**
         * No args constructor for use in serialization
         *
         */
        public Datum() {
        }

        /**
         *
         * @param catImage
         * @param catDel
         * @param catName
         * @param rowNumber
         * @param iD
         */
        public Datum(Integer iD, String catName, Boolean catDel, String catImage, Integer rowNumber) {
            super();
            this.iD = iD;
            this.catName = catName;
            this.catDel = catDel;
            this.catImage = catImage;
            this.rowNumber = rowNumber;
        }

        public Integer getID() {
            return iD;
        }

        public void setID(Integer iD) {
            this.iD = iD;
        }

        public String getCatName() {
            return catName;
        }

        public void setCatName(String catName) {
            this.catName = catName;
        }

        public Boolean getCatDel() {
            return catDel;
        }

        public void setCatDel(Boolean catDel) {
            this.catDel = catDel;
        }

        public String getCatImage() {
            return catImage;
        }

        public void setCatImage(String catImage) {
            this.catImage = catImage;
        }

        public Integer getRowNumber() {
            return rowNumber;
        }

        public void setRowNumber(Integer rowNumber) {
            this.rowNumber = rowNumber;
        }

    }

}