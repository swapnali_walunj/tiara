package com.tiara.ui.listener;

public interface OnLoadMoreListener {

    void onLoadMore(String type);
}