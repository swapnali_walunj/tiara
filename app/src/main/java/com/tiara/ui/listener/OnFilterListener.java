package com.tiara.ui.listener;

import com.tiara.ui.model.Response.FilterCopy;

/**
 * Created by yuvrajpandey on 02/07/17.
 */

public interface OnFilterListener {

    void onFilterApplied(FilterCopy filterCopy);

    void onFilterReset(FilterCopy filterCopy);

    void onFilterHide();
}
