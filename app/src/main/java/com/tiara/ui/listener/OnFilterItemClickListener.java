package com.tiara.ui.listener;

import android.view.View;

/**
 * Created by 123 on 10/31/2017.
 */

public interface OnFilterItemClickListener {

    void onClick(Object Item, View v, int pos);

}
