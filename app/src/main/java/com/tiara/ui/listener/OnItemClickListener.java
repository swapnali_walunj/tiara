package com.tiara.ui.listener;

import android.view.View;

public interface OnItemClickListener {

    void onClick(Object Item, View v);
}