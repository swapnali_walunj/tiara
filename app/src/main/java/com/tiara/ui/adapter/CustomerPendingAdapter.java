package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemLiveStockBinding;
import com.tiara.databinding.ProgressLayoutBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.Response.LiveStockRes;
import com.tiara.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.LOADING;
import static com.tiara.utils.Constants.VIEW_LABOUR_CHARGE;
import static com.tiara.utils.Constants.VIEW_LOADING;
import static com.tiara.utils.Constants.VISIBLE_THRESHOLD;

public class CustomerPendingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final Activity activity;
    private List<LiveStockRes.Datum> data=new ArrayList<>();
    private final OnItemClickListener listener;
    private boolean isLoading;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, lastVisibleItem, visibleThreshold = 3;
    private OnLoadMoreListener mOnLoadMoreListener;
    RecyclerView recyclerView;


    public CustomerPendingAdapter(Activity activity, List<LiveStockRes.Datum> data11,
                                  OnItemClickListener listener) {
        this.data.addAll(data11);
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_LABOUR_CHARGE:
                ItemLiveStockBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_live_stock, parent, false);
                return new CustomerPendingAdapter.CustomViewHolder(binding);

            case VIEW_LOADING:
                ProgressLayoutBinding binding4 = DataBindingUtil.inflate(activity.getLayoutInflater(),
                        R.layout.progress_layout, parent, false);
                return new CustomerPendingAdapter.CustomLoadingHolder(binding4);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  notificationViewHolder, final int position) {
        if (notificationViewHolder instanceof CustomerPendingAdapter.CustomViewHolder) {
            handleDataView(((CustomerPendingAdapter.CustomViewHolder) notificationViewHolder), position);
        }else {
            handleLoadingView(((CustomerPendingAdapter.CustomLoadingHolder) notificationViewHolder), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        String viewType = data.get(position).getCatName();
        switch (viewType) {
            case LOADING:
                return VIEW_LOADING;
            default:
                return VIEW_LABOUR_CHARGE;
        }

    }

    private void handleDataView(final CustomerPendingAdapter.CustomViewHolder holder, final int position) {
        holder.bindingNotification.tvProductname.setText(data.get(position).getCatName());
        Utils.loadImageInImageview(activity,data.get(position).getCatImage(),holder.bindingNotification.ivProductImage);
    }

    private void handleLoadingView(final CustomerPendingAdapter.CustomLoadingHolder holder, final int position) {

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemLiveStockBinding bindingNotification;

        public CustomViewHolder(ItemLiveStockBinding binding) {
            super(binding.getRoot());
            bindingNotification = binding;
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void refresh(List<LiveStockRes.Datum> arrayList) {
        this.data.clear();
        this.data.addAll(arrayList);
        notifyDataSetChanged();
    }
    public void setUpScrollListener(RecyclerView recyclerView, final LinearLayoutManager mLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore("");
                    }
                    isLoading = true;
                }
            }
        });
    }



    public void setLoaded() {
        isLoading = false;
    }
    private class CustomLoadingHolder extends RecyclerView.ViewHolder {
        public ProgressLayoutBinding binding;

        CustomLoadingHolder(ProgressLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
