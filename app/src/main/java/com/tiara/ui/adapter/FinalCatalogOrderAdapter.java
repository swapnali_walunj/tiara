package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemFinalOrderBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.OldStockOrder;

import java.util.List;

public class FinalCatalogOrderAdapter extends RecyclerView.Adapter<FinalCatalogOrderAdapter.CustomViewHolder> {

    OnItemClickListener listener;
    private Activity activity;
    public List<OldStockOrder> getData() {
        return data;
    }
    public void setData(List<OldStockOrder> data) {
        this.data = data;
    }
    List<OldStockOrder> data;

public FinalCatalogOrderAdapter(Activity activity, List<OldStockOrder> list, OnItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
        }

@Override
public FinalCatalogOrderAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFinalOrderBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_final_order, parent, false);
        return new FinalCatalogOrderAdapter.CustomViewHolder(binding);
        }

  @Override
   public void onBindViewHolder(final FinalCatalogOrderAdapter.CustomViewHolder holder, final int position) {
         holder.binding.tvColor.setText(data.get(position).getColourName());
          holder.binding.tvSize.setText(data.get(position).getSizeValue());
          holder.binding.tvAvgWeight.setText(data.get(position).getDesignAvgwt()+"");
          holder.binding.tvDesign.setText(data.get(position).getDesignDesignNo()+"");
          holder.binding.tvQty.setText(data.get(position).getQty()+"");

          holder.binding.ivDelete.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  listener.onClick(data.get(position),holder.binding.ivDelete);
              }
          });
         }

    public void updateAdapter( List<OldStockOrder> list) {
        data = list;
        notifyDataSetChanged();
    }
@Override
public int getItemCount() {
        return data.size();
        }

class CustomViewHolder extends RecyclerView.ViewHolder {
    ItemFinalOrderBinding binding;

    CustomViewHolder(ItemFinalOrderBinding bindig) {
        super(bindig.getRoot());
        binding = bindig;
    }
}

}

