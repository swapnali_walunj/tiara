package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemApprovedOrderBinding;
import com.tiara.databinding.ItemFinalOrderBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.ApprovedOrderDetails;
import com.tiara.ui.model.Response.PendingOrderDetails;

import java.util.List;

public class ApprovedOrderDetailsAdapter extends RecyclerView.Adapter<ApprovedOrderDetailsAdapter.CustomViewHolder> {

    OnItemClickListener listener;
    private Activity activity;
    public List<ApprovedOrderDetails.Datum> getData() {
        return data;
    }
    public void setData(List<ApprovedOrderDetails.Datum> data) {
        this.data = data;
    }
    List<ApprovedOrderDetails.Datum> data;

public ApprovedOrderDetailsAdapter(Activity activity, List<ApprovedOrderDetails.Datum> list, OnItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
        }

@Override
public ApprovedOrderDetailsAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemApprovedOrderBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_approved_order, parent, false);
        return new ApprovedOrderDetailsAdapter.CustomViewHolder(binding);
        }

  @Override
   public void onBindViewHolder(final ApprovedOrderDetailsAdapter.CustomViewHolder holder, final int position) {
         holder.binding.tvDesign.setText(data.get(position).getTxtserialno()+"");
          holder.binding.tvColor.setText(data.get(position).getLblCategory());
          holder.binding.tvSize.setText(data.get(position).getLblDesign()+"");
          holder.binding.tvAvgWeight.setText(data.get(position).getPDWeight()+"");
          /* holder.binding.tvQty.setVisibility(View.GONE);
          holder.binding.tvAmt.setVisibility(View.GONE);*/
          holder.binding.tvQty.setText(data.get(position).getTxtlabourcharge()+"");
          holder.binding.tvAmt.setText(data.get(position).getLbltotalamt()+"");

          /*holder.binding.ivDelete.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  listener.onClick(data.get(position),holder.binding.ivDelete);
              }
          });*/
         }

    public void updateAdapter( List<ApprovedOrderDetails.Datum> list) {
        data = list;
        notifyDataSetChanged();
    }
@Override
public int getItemCount() {
        return data.size();
        }

class CustomViewHolder extends RecyclerView.ViewHolder {
    ItemApprovedOrderBinding binding;

    CustomViewHolder(ItemApprovedOrderBinding bindig) {
        super(bindig.getRoot());
        binding = bindig;
    }
}

}

