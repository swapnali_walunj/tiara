package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.jakewharton.rxbinding.view.RxView;
import com.tiara.R;
import com.tiara.databinding.ItemStoresTextOnlyBinding;
import com.tiara.ui.listener.OnItemClickListener;


import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

import static com.tiara.utils.Constants.VIEW_TEXT;


public class CommonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private boolean isSelected = false;
    private ArrayList<String> data;
    OnItemClickListener listener;
    private Activity activity;
    int selectedPos = 0;

    public CommonAdapter(Activity Activity, ArrayList<String> data, OnItemClickListener listener) {
        this.activity = Activity;
        this.listener = listener;
        this.data = data;
    }

    public CommonAdapter(Activity Activity, ArrayList<String> data, OnItemClickListener listener, boolean isSelected) {
        this.activity = Activity;
        this.listener = listener;
        this.data = data;
        this.isSelected = isSelected;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TEXT:
                ItemStoresTextOnlyBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(),
                        R.layout.item_stores_text_only, parent, false);
                return new CommonTextOnlyViewHolder(binding);
            default:
                ItemStoresTextOnlyBinding binding2 = DataBindingUtil.inflate(activity.getLayoutInflater(),
                        R.layout.item_stores_text_only, parent, false);
                return new CommonTextOnlyViewHolder(binding2);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CommonTextOnlyViewHolder) {
            handleTextOnlyView((CommonTextOnlyViewHolder) holder, position);

        }
    }

    @Override
    public int getItemCount() {
            return data.size();

    }

    @Override
    public int getItemViewType(int position) {
        if (data != null) {
            return VIEW_TEXT;
        }  else {
            return VIEW_TEXT;
        }
    }

    private class CommonTextOnlyViewHolder extends RecyclerView.ViewHolder {
        ItemStoresTextOnlyBinding binding;

        CommonTextOnlyViewHolder(ItemStoresTextOnlyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }




    private void handleTextOnlyView(final CommonTextOnlyViewHolder holder, final int position) {
        holder.binding.textData.setText(data.get(position));
        if (position == 0) {
            holder.binding.textData.setTextColor(ContextCompat.getColor(activity, R.color.white));
        } else {
            holder.binding.textData.setTextColor(ContextCompat.getColor(activity, R.color.textHeading));
        }
//        holder.itemView.setSelected(selectedPos == position);
        if (isSelected)
            holder.itemView.setBackgroundColor(ContextCompat.getColor(activity, R.color.textBlue));
        isSelected = false;

        RxView.clicks(holder.itemView).subscribe(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
               /* notifyItemChanged(selectedPos);
                selectedPos = position;
                notifyItemChanged(selectedPos);
                listener.onClick(selectedPos, null);*/
                listener.onClick(position, holder.itemView);
            }
        });
    }



}