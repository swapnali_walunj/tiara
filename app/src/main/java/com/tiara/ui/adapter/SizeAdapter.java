package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemColorBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.ColorResponse;
import com.tiara.ui.model.Response.SizeResponse;

import java.util.List;


public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.CustomViewHolder> {
    OnItemClickListener listener;
    private Activity activity;
    List<SizeResponse.Datum> data;

    public SizeAdapter(Activity activity, List<SizeResponse.Datum> list, OnItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
    }

    @Override
    public SizeAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemColorBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.   item_color, parent, false);
        return new SizeAdapter.CustomViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final SizeAdapter.CustomViewHolder holder, final int position) {
        holder.binding.tvName.setText(data.get(position).getSizeValue());
        holder.binding.tvName.setChecked(data.get(position).isSelected());


        holder.binding.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(data.get(position).getSizeID());
                listener.onClick(holder.binding.tvName.isChecked(), v);
            }
        });

    }

    public List<SizeResponse.Datum> getData() {
        return data;
    }

    public void setData(List<SizeResponse.Datum> data) {
        this.data = data;
    }
    @Override
    public int getItemCount() {
        return data.size() ;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemColorBinding binding;

        CustomViewHolder(ItemColorBinding bindig) {
            super(bindig.getRoot());
            binding = bindig;
        }
    }

}

