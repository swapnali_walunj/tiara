package com.tiara.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemLiveStockBinding;
import com.tiara.databinding.ItemLiveStockDesignBinding;
import com.tiara.databinding.ProgressLayoutBinding;
import com.tiara.ui.activity.ImagePagerActivity;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.Response.LiveStockDesignRes;
import com.tiara.ui.model.Response.LiveStockRes;
import com.tiara.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.LOADING;
import static com.tiara.utils.Constants.VIEW_LABOUR_CHARGE;
import static com.tiara.utils.Constants.VIEW_LOADING;
import static com.tiara.utils.Constants.VISIBLE_THRESHOLD;

public class LiveStockDesignAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final Activity activity;
    private List<LiveStockDesignRes.Datum> data=new ArrayList<>();
    private final OnItemClickListener listener;
    private boolean isLoading;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, lastVisibleItem, visibleThreshold = 3;
    private OnLoadMoreListener mOnLoadMoreListener;
    RecyclerView recyclerView;


    public LiveStockDesignAdapter(Activity activity, List<LiveStockDesignRes.Datum> data11,
                                  OnItemClickListener listener) {
        this.data.addAll(data11);
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_LABOUR_CHARGE:
                ItemLiveStockDesignBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_live_stock_design, parent, false);
                return new LiveStockDesignAdapter.CustomViewHolder(binding);

            case VIEW_LOADING:
                ProgressLayoutBinding binding4 = DataBindingUtil.inflate(activity.getLayoutInflater(),
                        R.layout.progress_layout, parent, false);
                return new LiveStockDesignAdapter.CustomLoadingHolder(binding4);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  notificationViewHolder, final int position) {
        if (notificationViewHolder instanceof LiveStockDesignAdapter.CustomViewHolder) {
            handleDataView(((LiveStockDesignAdapter.CustomViewHolder) notificationViewHolder), position);
        }else {
            handleLoadingView(((LiveStockDesignAdapter.CustomLoadingHolder) notificationViewHolder), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        String viewType = data.get(position).getDesignDesignNo();
        switch (viewType) {
            case LOADING:
                return VIEW_LOADING;
            default:
                return VIEW_LABOUR_CHARGE;
        }

    }

    private void handleDataView(final LiveStockDesignAdapter.CustomViewHolder holder, final int position) {
        holder.bindingNotification.tvProductname.setText(data.get(position).getImageName()+"");
        if(data.get(position).getImageCount()>1) {
            holder.bindingNotification.tvViewMore.setVisibility(View.VISIBLE);
        }else{
            holder.bindingNotification.tvViewMore.setVisibility(View.GONE);
        }
        holder.bindingNotification.tvOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(data.get(position),view);
            }
        });
        holder.bindingNotification.tvViewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(activity,ImagePagerActivity.class);
                i.putExtra("isMore",true);
                i.putExtra("companyid",data.get(position).getDesignCatID());
                i.putExtra("designid",data.get(position).getDesignID());
                activity.startActivity(i);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(activity,ImagePagerActivity.class);
                i.putExtra("isMore",false);
                i.putExtra("imagePath",data.get(position).getImagePath());
                activity.startActivity(i);
            }
        });

        Utils.loadImageInImageview(activity,data.get(position).getImagePath(),holder.bindingNotification.ivProductImage);
    }

    private void handleLoadingView(final LiveStockDesignAdapter.CustomLoadingHolder holder, final int position) {

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemLiveStockDesignBinding bindingNotification;

        public CustomViewHolder(ItemLiveStockDesignBinding binding) {
            super(binding.getRoot());
            bindingNotification = binding;
        }
    }




    private class CustomLoadingHolder extends RecyclerView.ViewHolder {
        public ProgressLayoutBinding binding;

        CustomLoadingHolder(ProgressLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
