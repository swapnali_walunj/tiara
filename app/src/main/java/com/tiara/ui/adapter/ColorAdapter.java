package com.tiara.ui.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemColorBinding;
import com.tiara.databinding.ItemLabourChargeBinding;
import com.tiara.ui.listener.OnFilterItemClickListener;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.ColorResponse;

import java.util.List;


public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.CustomViewHolder> {
    OnFilterItemClickListener listener;
    private Activity activity;
    List<ColorResponse.Datum> data;

    public ColorAdapter(Activity activity, List<ColorResponse.Datum> list, OnFilterItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
    }

    @Override
    public ColorAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemColorBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.   item_color, parent, false);
        return new ColorAdapter.CustomViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ColorAdapter.CustomViewHolder holder, final int position) {
        holder.binding.tvName.setText(data.get(position).getColourName());
        holder.binding.tvName.setChecked(data.get(position).isSelected());


        holder.binding.tvName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    v.setTag(data.get(position).getColourID());
                    listener.onClick(holder.binding.tvName.isChecked(), v,position);

            }
        });

    }
    public List<ColorResponse.Datum> getData() {
        return data;
    }

    public void setData(List<ColorResponse.Datum> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data.size() ;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemColorBinding binding;

        CustomViewHolder(ItemColorBinding bindig) {
            super(bindig.getRoot());
            binding = bindig;
        }
    }

}

