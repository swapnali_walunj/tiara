package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemCatalogOrderBinding;
import com.tiara.databinding.ItemOrderBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.OldStockOrder;
import com.tiara.utils.AppUtils;

import java.util.List;

public class CatalogOrderAdapter extends RecyclerView.Adapter<CatalogOrderAdapter.CustomViewHolder> {
        OnItemClickListener listener;
       private Activity activity;

    public List<OldStockOrder> getData() {
        return data;
    }

    public void setData(List<OldStockOrder> data) {
        this.data = data;
    }

    List<OldStockOrder> data;

public CatalogOrderAdapter(Activity activity, List<OldStockOrder> list, OnItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
        }

@Override
public CatalogOrderAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCatalogOrderBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_catalog_order, parent, false);
        return new CatalogOrderAdapter.CustomViewHolder(binding);
        }

@Override
public void onBindViewHolder(final CatalogOrderAdapter.CustomViewHolder holder, final int position) {
         holder.binding.tvColor.setText(data.get(position).getColourName());
          holder.binding.tvSize.setText(data.get(position).getSizeValue());
          holder.binding.tvAvgWeight.setText(data.get(position).getDesignAvgwt()+"");


          holder.binding.tvQty.addTextChangedListener(new TextWatcher() {
              @Override
              public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

              }

              @Override
              public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                  /*if(!TextUtils.isEmpty(holder.binding.tvQty.getText().toString().trim()) && Integer.valueOf(holder.binding.tvQty.getText().toString())<= data.get(position).getNoOfStock()) {
                      data.get(position).setQty(Integer.valueOf(holder.binding.tvQty.getText().toString()));
                  }else{
                      AppUtils.toastMe(activity,"Available stock is "+ data.get(position).getNoOfStock());
                  }*/
                  data.get(position).setQty(Integer.valueOf(holder.binding.tvQty.getText().toString()));
                  double totalwt=Integer.valueOf(holder.binding.tvQty.getText().toString())*data.get(position).getDesignAvgwt();
                  holder.binding.tvTotalWt.setText(totalwt+"");
              }

              @Override
              public void afterTextChanged(Editable editable) {
                 // data.get(position).setQty(Integer.valueOf(holder.binding.tvQty.getText().toString()));

              }
          });

         }


@Override
public int getItemCount() {
        return data.size();
        }

class CustomViewHolder extends RecyclerView.ViewHolder {
    ItemCatalogOrderBinding binding;

    CustomViewHolder(ItemCatalogOrderBinding bindig) {
        super(bindig.getRoot());
        binding = bindig;
    }
}

}

