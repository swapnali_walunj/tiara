package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemLabourChargeBinding;
import com.tiara.databinding.ItemLayoutMycustomerBinding;
import com.tiara.databinding.ProgressLayoutBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.Response.MyCustomerResponse;


import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.LOADING;
import static com.tiara.utils.Constants.VIEW_LABOUR_CHARGE;
import static com.tiara.utils.Constants.VIEW_LOADING;
import static com.tiara.utils.Constants.VISIBLE_THRESHOLD;


public class MyCustomerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final Activity activity;
    private List<MyCustomerResponse.Datum> data=new ArrayList<>();
    private final OnItemClickListener listener;
    private boolean isLoading;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, lastVisibleItem, visibleThreshold = 3;
    private OnLoadMoreListener mOnLoadMoreListener;
    RecyclerView recyclerView;


    public MyCustomerAdapter(Activity activity, List<MyCustomerResponse.Datum> data11,
                             OnItemClickListener listener) {
        this.data.addAll(data11);
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_LABOUR_CHARGE:
                 ItemLayoutMycustomerBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_layout_mycustomer, parent, false);
                return new MyCustomerAdapter.CustomViewHolder(binding);

            case VIEW_LOADING:
                ProgressLayoutBinding binding4 = DataBindingUtil.inflate(activity.getLayoutInflater(),
                        R.layout.progress_layout, parent, false);
                return new MyCustomerAdapter.CustomLoadingHolder(binding4);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  notificationViewHolder, final int position) {
        if (notificationViewHolder instanceof MyCustomerAdapter.CustomViewHolder) {
            handleDataView(((MyCustomerAdapter.CustomViewHolder) notificationViewHolder), position);
        }else {
            handleLoadingView(((MyCustomerAdapter.CustomLoadingHolder) notificationViewHolder), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        String viewType = data.get(position).getCustAddress();
        switch (viewType) {
            case LOADING:
                return VIEW_LOADING;
            default:
                return VIEW_LABOUR_CHARGE;
        }

    }

    private void handleDataView(final MyCustomerAdapter.CustomViewHolder holder, final int position) {
        holder.bindingNotification.tvName.setText(data.get(position).getCustName());
        holder.bindingNotification.tvMobile.setText(data.get(position).getCustMobile());
        holder.bindingNotification.tvStatus.setText(data.get(position).getCustCustStatus());




    }

    private void handleLoadingView(final MyCustomerAdapter.CustomLoadingHolder holder, final int position) {

    }


    @Override
    public int getItemCount() {
        return
        data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemLayoutMycustomerBinding bindingNotification;

        public CustomViewHolder(ItemLayoutMycustomerBinding binding) {
            super(binding.getRoot());
            bindingNotification = binding;
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void refresh(List<MyCustomerResponse.Datum> arrayList) {
        this.data.clear();
        this.data.addAll(arrayList);
        notifyDataSetChanged();
    }
    public void setUpScrollListener(RecyclerView recyclerView, final LinearLayoutManager mLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore("");
                    }
                    isLoading = true;
                }
            }
        });
    }



    public void setLoaded() {
        isLoading = false;
    }
    private class CustomLoadingHolder extends RecyclerView.ViewHolder {
        public ProgressLayoutBinding binding;

        CustomLoadingHolder(ProgressLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}

