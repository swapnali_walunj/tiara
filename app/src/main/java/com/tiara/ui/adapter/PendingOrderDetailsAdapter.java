package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemFinalOrderBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.PendingOrderDetails;

import java.util.List;

public class PendingOrderDetailsAdapter extends RecyclerView.Adapter<PendingOrderDetailsAdapter.CustomViewHolder> {

    OnItemClickListener listener;
    private Activity activity;
    public List<PendingOrderDetails.Datum> getData() {
        return data;
    }
    public void setData(List<PendingOrderDetails.Datum> data) {
        this.data = data;
    }
    List<PendingOrderDetails.Datum> data;

public PendingOrderDetailsAdapter(Activity activity, List<PendingOrderDetails.Datum> list, OnItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
        }

@Override
public PendingOrderDetailsAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFinalOrderBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_final_order, parent, false);
        return new PendingOrderDetailsAdapter.CustomViewHolder(binding);
        }

  @Override
   public void onBindViewHolder(final PendingOrderDetailsAdapter.CustomViewHolder holder, final int position) {
         holder.binding.tvColor.setText(data.get(position).getColourName());
          holder.binding.tvSize.setText(data.get(position).getSizeValue());
          holder.binding.tvAvgWeight.setText(data.get(position).getPDWeight()+"");
          holder.binding.tvQty.setVisibility(View.GONE);
          holder.binding.tvDesign.setText(data.get(position).getDesignDesignNo()+"");
          holder.binding.ivDelete.setVisibility(View.VISIBLE);

          holder.binding.ivDelete.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  listener.onClick(position,holder.binding.ivDelete);
              }
          });
         }

    public void updateAdapter( List<PendingOrderDetails.Datum> list) {
        data = list;
        notifyDataSetChanged();
    }
@Override
public int getItemCount() {
        return data.size();
        }

class CustomViewHolder extends RecyclerView.ViewHolder {
    ItemFinalOrderBinding binding;

    CustomViewHolder(ItemFinalOrderBinding bindig) {
        super(bindig.getRoot());
        binding = bindig;
    }
}

}

