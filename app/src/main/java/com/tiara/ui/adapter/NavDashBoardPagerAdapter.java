package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;


import com.tiara.R;
import com.tiara.databinding.ItemNavDashboardViewPagerBinding;
import com.tiara.ui.helper.NavDashBoardPagerList;

import java.util.ArrayList;

/**
 * Created by itrs-203 on 11/14/17.
 */

public class NavDashBoardPagerAdapter extends PagerAdapter {

    private Activity activity;
    ArrayList<NavDashBoardPagerList> arrayList;
    String moduleData;

    public NavDashBoardPagerAdapter(Activity context, ArrayList<NavDashBoardPagerList> arrayList, String moduleData) {
        activity = context;
        this.arrayList = arrayList;
        this.moduleData = moduleData;
    }

    @Override
    public View instantiateItem(ViewGroup collection, final int position) {
        ItemNavDashboardViewPagerBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_nav_dashboard_view_pager, collection, false);
        binding.imageviewNavType.setImageResource(arrayList.get(position).getDeviceResImage());
        binding.textviewNavType.setText(arrayList.get(position).getDeviceType());
        binding.textviewNavQuantity.setText(arrayList.get(position).getDeviceQuantity());


        binding.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 0) {

                } else if (position == 1) {
                } else if (position == 2) {

                }
            }
        });

        collection.addView(binding.getRoot());

        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
