package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemFinalOrderBinding;
import com.tiara.databinding.ItemFinalOrderOldpendingBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.OldPendingOrderDetails;
import com.tiara.ui.model.Response.PendingOrderDetails;

import java.util.List;

public class OldPendingOrderDetailsAdapter extends RecyclerView.Adapter<OldPendingOrderDetailsAdapter.CustomViewHolder> {

    OnItemClickListener listener;
    private Activity activity;
    public List<OldPendingOrderDetails.Datum> getData() {
        return data;
    }
    public void setData(List<OldPendingOrderDetails.Datum> data) {
        this.data = data;
    }
    List<OldPendingOrderDetails.Datum> data;

public OldPendingOrderDetailsAdapter(Activity activity, List<OldPendingOrderDetails.Datum> list, OnItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
        }

@Override
public OldPendingOrderDetailsAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFinalOrderOldpendingBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_final_order_oldpending, parent, false);
        return new OldPendingOrderDetailsAdapter.CustomViewHolder(binding);
        }

  @Override
   public void onBindViewHolder(final OldPendingOrderDetailsAdapter.CustomViewHolder holder, final int position) {
          holder.binding.tvCategory.setText(data.get(position).getCatName()+"");
          holder.binding.tvDesign.setText(data.get(position).getDesignDesignNo());
          holder.binding.tvColor.setText(data.get(position).getColourName());
          holder.binding.tvSize.setText(data.get(position).getSizeValue()+"");
          holder.binding.tvWeight.setText(data.get(position).getOldWeight()+"");
          holder.binding.tvQty.setText(data.get(position).getOldQty()+"");
          holder.binding.tvTotal.setText(data.get(position).getTotal()+"");
          //holder.binding.tvQty.setText(data.get(position).get()+"");
          holder.binding.ivDelete.setVisibility(View.GONE);

          /*holder.binding.ivDelete.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  listener.onClick(data.get(position),holder.binding.ivDelete);
              }
          });*/
         }

    public void updateAdapter( List<OldPendingOrderDetails.Datum> list) {
        data = list;
        notifyDataSetChanged();
    }
@Override
public int getItemCount() {
        return data.size();
        }

class CustomViewHolder extends RecyclerView.ViewHolder {
    ItemFinalOrderOldpendingBinding binding;

    CustomViewHolder(ItemFinalOrderOldpendingBinding bindig) {
        super(bindig.getRoot());
        binding = bindig;
    }
}

}

