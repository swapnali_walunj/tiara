package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemColorBinding;
import com.tiara.databinding.ItemOrderBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.LiveStockOrderResponse;
import com.tiara.utils.AppUtils;

import java.util.List;

import static com.tiara.ui.model.Response.LiveStockOrderResponse.*;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.CustomViewHolder> {
        OnItemClickListener listener;
       private Activity activity;

    public List<LiveStockOrder> getData() {
        return data;
    }

    public void setData(List<LiveStockOrder> data) {
        this.data = data;
    }

    List<LiveStockOrder> data;

public OrderAdapter(Activity activity, List<LiveStockOrder> list, OnItemClickListener itemClickListener) {
        this.activity = activity;
        this.data=list;
        this.listener=itemClickListener;
        }

@Override
public OrderAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemOrderBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_order, parent, false);
        return new OrderAdapter.CustomViewHolder(binding);
        }

@Override
public void onBindViewHolder(final OrderAdapter.CustomViewHolder holder, final int position) {
         holder.binding.tvColor.setText(data.get(position).getColourName());
          holder.binding.tvSize.setText(data.get(position).getSizeValue());
          holder.binding.tvAvgWeight.setText(data.get(position).getAvgWeight()+"");
          holder.binding.tvStock.setText(data.get(position).getNoOfStock()+"");

          holder.binding.tvQty.addTextChangedListener(new TextWatcher() {
              @Override
              public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

              }

              @Override
              public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

              }

              @Override
              public void afterTextChanged(Editable editable) {
                  if(!TextUtils.isEmpty(holder.binding.tvQty.getText().toString().trim()) && Integer.parseInt(holder.binding.tvQty.getText().toString())<= data.get(position).getNoOfStock()) {
                      data.get(position).setQty(Integer.parseInt(holder.binding.tvQty.getText().toString()));
                      AppUtils.logMe("Qty",Integer.parseInt(holder.binding.tvQty.getText().toString())+"");
                  }else{
                      data.get(position).setQty(0);
                      AppUtils.toastMe(activity,"Available stock is "+ data.get(position).getNoOfStock());
                  }

              }
          });

         }


@Override
public int getItemCount() {
        return data.size();
        }

class CustomViewHolder extends RecyclerView.ViewHolder {
    ItemOrderBinding binding;

    CustomViewHolder(ItemOrderBinding bindig) {
        super(bindig.getRoot());
        binding = bindig;
    }
}

}

