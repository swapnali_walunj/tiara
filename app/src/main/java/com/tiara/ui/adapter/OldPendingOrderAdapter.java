package com.tiara.ui.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tiara.R;
import com.tiara.databinding.ItemPendingOrderBinding;
import com.tiara.databinding.ProgressLayoutBinding;
import com.tiara.ui.listener.OnItemClickListener;
import com.tiara.ui.listener.OnLoadMoreListener;
import com.tiara.ui.model.Response.OldPendingOrderResponse;
import com.tiara.ui.model.Response.PendingOrderResponse;

import java.util.ArrayList;
import java.util.List;

import static com.tiara.utils.Constants.LOADING;
import static com.tiara.utils.Constants.VIEW_LABOUR_CHARGE;
import static com.tiara.utils.Constants.VIEW_LOADING;
import static com.tiara.utils.Constants.VISIBLE_THRESHOLD;

public class OldPendingOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final Activity activity;
    private List<OldPendingOrderResponse.Datum> data=new ArrayList<>();
    private final OnItemClickListener listener;
    private boolean isLoading;
    private int pastVisiblesItems, visibleItemCount, totalItemCount, lastVisibleItem, visibleThreshold = 3;
    private OnLoadMoreListener mOnLoadMoreListener;
    RecyclerView recyclerView;


    public OldPendingOrderAdapter(Activity activity, List<OldPendingOrderResponse.Datum> data11,
                                  OnItemClickListener listener) {
        this.data.addAll(data11);
        this.listener = listener;
        this.activity = activity;
    }

    @Override
    public  RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_LABOUR_CHARGE:
                ItemPendingOrderBinding binding = DataBindingUtil.inflate(activity.getLayoutInflater(), R.layout.item_pending_order, parent, false);
                return new OldPendingOrderAdapter.CustomViewHolder(binding);

            case VIEW_LOADING:
                ProgressLayoutBinding binding4 = DataBindingUtil.inflate(activity.getLayoutInflater(),
                        R.layout.progress_layout, parent, false);
                return new OldPendingOrderAdapter.CustomLoadingHolder(binding4);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  notificationViewHolder, final int position) {
        if (notificationViewHolder instanceof OldPendingOrderAdapter.CustomViewHolder) {
            handleDataView(((OldPendingOrderAdapter.CustomViewHolder) notificationViewHolder), position);
        }else {
            handleLoadingView(((OldPendingOrderAdapter.CustomLoadingHolder) notificationViewHolder), position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        String viewType = data.get(position).getCustName();
        switch (viewType) {
            case LOADING:
                return VIEW_LOADING;
            default:
                return VIEW_LABOUR_CHARGE;
        }

    }

    private void handleDataView(final OldPendingOrderAdapter.CustomViewHolder holder, final int position) {
        holder.bindingNotification.tvCustName.setText(data.get(position).getCustName());
        holder.bindingNotification.tvOrderNo.setText(data.get(position).getOldONumber());
        holder.bindingNotification.tvOrderDate.setText(data.get(position).getOldODate());
        holder.bindingNotification.tvAmount.setVisibility(View.GONE);
        holder.bindingNotification.tvWeight.setVisibility(View.GONE);
        holder.bindingNotification.tvAmount1.setVisibility(View.GONE);
        holder.bindingNotification.tvWeight1.setVisibility(View.GONE);
        // holder.bindingNotification.tvAmount.setText(data.get(position).get());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(data.get(position),holder.itemView);
            }
        });
    }

    private void handleLoadingView(final OldPendingOrderAdapter.CustomLoadingHolder holder, final int position) {

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ItemPendingOrderBinding bindingNotification;

        public CustomViewHolder(ItemPendingOrderBinding binding) {
            super(binding.getRoot());
            bindingNotification = binding;
        }
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void refresh(List<OldPendingOrderResponse.Datum> arrayList) {
        this.data.clear();
        this.data.addAll(arrayList);
        notifyDataSetChanged();
    }
    public void setUpScrollListener(RecyclerView recyclerView, final LinearLayoutManager mLayoutManager) {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore("");
                    }
                    isLoading = true;
                }
            }
        });
    }



    public void setLoaded() {
        isLoading = false;
    }
    private class CustomLoadingHolder extends RecyclerView.ViewHolder {
        public ProgressLayoutBinding binding;

        CustomLoadingHolder(ProgressLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
