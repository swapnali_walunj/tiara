package com.tiara.utils;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.tiara.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by user on 13/2/17.
 */

public class Utils {
	public static Picasso picasso;
	private static final int MAX_WIDTH = 1280;
	private static final int MAX_HEIGHT = 1000;


	public Utils() {
	}
	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	public static String getCurrentDate() {
		String formattedDate = "";
		try {
			Calendar c = Calendar.getInstance();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddhhmmss", Locale.US);
			formattedDate = simpleDateFormat.format(c.getTime());
		} catch (Exception e) {
			formattedDate = "";
		}
		return formattedDate;
	}
	public static void showPermissionDialog(final Activity context) {

		android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Permissions Required")
				.setMessage("You have forcefully denied some of the required permissions " +
						"for this action. Please open settings, go to permissions and allow them.")
				.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
								Uri.fromParts("package", context.getPackageName(), null));
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(intent);
					}
				})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				})
				.setCancelable(false)
				.create()
				.show();
	}
	public static void loadImageInImageview(Context mContext, final String filepath, final ImageView view) {

		final int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
		try {
			if (filepath != null && URLUtil.isValidUrl(filepath)) {
				if(picasso==null) {
					picasso = new Picasso.Builder(mContext)
							.memoryCache(new LruCache(24000)).build();
				}
				picasso.load(filepath)
						.networkPolicy(NetworkPolicy.OFFLINE)
						.transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
						.skipMemoryCache().resize(size, size).centerInside()
						.placeholder(R.color.placeholder_color)
						.into(view, new Callback() {

							@Override
							public void onSuccess() {
								view.setScaleType(ImageView.ScaleType.FIT_XY);
							}

							@Override
							public void onError() {
								picasso.load(filepath)
										.placeholder(R.color.placeholder_color)
										.transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
										.skipMemoryCache().resize(size, size).centerInside()
										.into(view, new Callback() {

											@Override
											public void onSuccess() {
												view.setScaleType(ImageView.ScaleType.FIT_XY);
											}

											@Override
											public void onError() {
												try {
													//view.setImageResource(R.drawable.logog);
												} catch (Exception | OutOfMemoryError e) {
													e.printStackTrace();
												}
											}
										});
							}
						});


			} else {
				view.setScaleType(ImageView.ScaleType.FIT_XY);
				//view.setImageResource(R.color.textBlue);
			}
		} catch (Exception |
				OutOfMemoryError e)

		{
			e.printStackTrace();
		}
	}


	public static void loadImageInImageview1(Context mContext, final String filepath, final ImageView view) {

		final int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
		try {
			if (filepath != null && URLUtil.isValidUrl(filepath)) {
				if(picasso==null) {
					picasso = new Picasso.Builder(mContext).build();
					//.memoryCache(new LruCache(24000)).build();
				}
				picasso.load(filepath)
						.networkPolicy(NetworkPolicy.OFFLINE)
						.transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
						.skipMemoryCache().resize(size, size).centerInside()
						.placeholder(R.color.placeholder_color)
						.into(view, new Callback() {

							@Override
							public void onSuccess() {
								view.setScaleType(ImageView.ScaleType.FIT_XY);
							}

							@Override
							public void onError() {
								picasso.load(filepath)
										.placeholder(R.color.placeholder_color)
										.transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
										.skipMemoryCache().resize(size, size).centerInside()
										.into(view, new Callback() {

											@Override
											public void onSuccess() {
												view.setScaleType(ImageView.ScaleType.FIT_XY);
											}

											@Override
											public void onError() {
												try {
													//view.setImageResource(R.drawable.logog);
												} catch (Exception | OutOfMemoryError e) {
													e.printStackTrace();
												}
											}
										});
							}
						});


			} else {
				view.setScaleType(ImageView.ScaleType.FIT_XY);
				//view.setImageResource(R.color.textBlue);
			}
		} catch (Exception |
				OutOfMemoryError e)

		{
			e.printStackTrace();
		}
	}
}
