package com.tiara.utils;

/**
 * Created by itrs-203 on 11/14/17.
 */

public class Constants {



    public static final String BASEURL="https://b2b.tiarasilver.com/";
    public static final int FADE_INTERVAL = 200;
    public static final String APPLICATION_DIR = "APPNAME";
    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";
    public static final String USER_ID="USER_ID";
    public static final String COMPANY_ID="COMPANY_ID";
    public static final String USER_NAME="USER_NAME";
    public static final String USER_PIC="USER_PIC";
    public static final String MOBILE_NO="MOBILE_NO";
    public static final String CHANGE_PASSWORD="ChangePassword/Customer";
    public static final String COMPANY_NAME="COMPANY_NAME";






    // Navigation Data
    public static final String NAV_DASHBOARD = "Dashboard";
    public static final String NAV_MY_PROFILE = "My Profile";
    public static final String NAV_LABOUR_CHARGE = "Labour Charge";
    public static final String NAV_LIVE_STOCK = "Live Stock";
    public static final String NAV_CATALOG = "Catalog";
    public static final String NAV_SALES_ORDER = "Approved Order";
    public static final String NAV_CHANGE_PASSWORD = "Change Password";
    public static final String NAV_LOGOUT = "Logout";
    public static final String NAV_SALES_ORDER1 = "Pending Order";
    public static final String NAV_SALES_ORDER2 = "Old Pending Order";
    public static final String NAV_MY_CUSTOMER = "My Customer";

    public static final int VIEW_TOP_NAVIGATION = 1;
    public static final int VIEW_NORMAL_NAVIGATION = 2;
    public static final int VIEW_BOTTOM_NAVIGATION = 3;
    public static final int SWIPE_DELAY = 500;
    public static final String IMAGELIST_KEY = "image_list";
    public static final String POSITION_KEY = "POSITION";

    public static final String BEARER = "Bearer ";
    public static final String TIARA_FILEPROVIDER = "com.tiara.fileprovider";
    public static final String TIARA_DIR = "Tiara";
    public static final String FILTER = "filter";
    public static final int VIEW_TEXT = 0;
    public static final String IS_FILTER = "is_filter";
    public static final String FILTER_DATA = "Filter";

    public static final String CUSTOMER_RESPONSE="CUSTOMER_RESPONSE";
    public static final String SALES_RESPONSE="SALES_RESPONSE";
    public static final int VIEW_LOADING = 1;
    public static final int VIEW_LABOUR_CHARGE = 2;
    public static final int VISIBLE_THRESHOLD = 1;
    public static final String LOADING = "Loading";
    public static final int LOAD_MORE_DELAY = 2000;
    public static final String IS_CUSTOMER="IS_CUSTOMER";



}
