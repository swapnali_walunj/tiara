package com.tiara.utils;

import android.Manifest.permission;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tiara.R;
import com.tiara.ui.fragment.BaseFragment;
import com.tiara.ui.listener.OnItemClickListener;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.os.Environment.getExternalStorageDirectory;
import static com.tiara.utils.Constants.FADE_INTERVAL;


/**
 * Created by user on 13/2/17.
 */

public class AppUtils {
    public static Toast toast;

    public AppUtils() {
    }
    public static void animateFromBottomtoTop(Context context, View view, Boolean value) {
        if (value) {
            Animation bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_up);
            view.startAnimation(bottomUp);
        } else {
            Animation bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_down);
            view.startAnimation(bottomUp);
        }
    }
    public static int pushFragment(BaseFragment fragment, int containerViewId, AppCompatActivity mActivity, String tag) {
        return pushFragment(fragment, containerViewId, mActivity, tag, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    }

    public static int pushFragment(Fragment fragment, int containerViewId, AppCompatActivity mActivity, String tag) {
        return pushFragment(fragment, containerViewId, mActivity, tag, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    }

    public static int pushFragment(Fragment fragment, int containerViewId, Fragment mActivity, String tag) {
        return pushFragment(fragment, containerViewId, mActivity, tag, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    }

    public static BaseFragment getActiveFragment(AppCompatActivity mActivity) {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }
        String tag = mActivity.getSupportFragmentManager()
                .getBackStackEntryAt(mActivity.getSupportFragmentManager().getBackStackEntryCount() - 1)
                .getName();
        return ((BaseFragment) mActivity.getSupportFragmentManager().findFragmentByTag(tag));
    }

    public static int pushFragment(Fragment fragment, int containerViewId, AppCompatActivity mActivity, String tag,
                                   int transaction) {
        if (tag == null || tag.isEmpty()) {
            tag = fragment.getClass().getSimpleName();
        }
        Fragment activeFragment = getActiveFragment(mActivity);
        if (activeFragment == null) {
            FragmentTransaction trans = mActivity.getSupportFragmentManager().beginTransaction();
            trans.setTransition(transaction);
            trans.replace(containerViewId, fragment, tag);
            trans.addToBackStack(tag);
            return trans.commit();
        } else if (!(activeFragment.getTag().equalsIgnoreCase(tag))) {
            FragmentTransaction trans = mActivity.getSupportFragmentManager().beginTransaction();
            trans.setTransition(transaction);
            trans.replace(containerViewId, fragment, tag);
            trans.addToBackStack(tag);
            return trans.commit();
        }
        return -33;
    }

    public static int pushFragment(Fragment fragment, int containerViewId, Fragment mActivity, String tag,
                                   int transaction) {
        if (tag == null || tag.isEmpty()) {
            tag = fragment.getClass().getSimpleName();
        }
        FragmentTransaction trans = mActivity.getChildFragmentManager().beginTransaction();
        trans.setTransition(transaction);
        trans.replace(containerViewId, fragment, tag);
        trans.addToBackStack(tag);
        return trans.commit();
    }
    public static void openMailIntent(Activity activity, String Recipient, String subject, String msg) {
        try {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:" + Recipient +
                    "?subject=" + Uri.encode(subject) +
                    "&body=" + Uri.encode(msg)));
            activity.startActivity(emailIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            toastMe(activity, "E-Mail not configured");
        }
    }

    public static void openShareIntent(Activity activity, String message, String subject) {
        try {
            ShareCompat.IntentBuilder.from(activity)
                    .setType("text/plain")
//                  .addEmailTo(getString(R.string.support_email_id))
                    .setSubject(subject)
                    .setText(message)
//                  .setHtmlText(body) //If you are using HTML in your body text
                    .setChooserTitle("Share")
                    .startChooser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String convertBudgetToCrore(String budget) {
        String s = "";

        try {
            if (!budget.equalsIgnoreCase("")) {
                double num = Integer.parseInt(budget);
                double float_num = 0;
                if (budget.length() >= 8) {
                    float_num = num / 10000000;
                    s = String.format("%.2f", float_num);
                    String dec = s.substring(0, s.indexOf(".") + 2);
                    s = "₹ " + dec + " Cr";

                } else if (budget.length() >= 6) {
                    float_num = num / 100000;
                    s = String.format("%.2f", float_num);
                    String dec = s.substring(0, s.indexOf(".") + 2);
                    s = "₹ " + dec + " Lac";

                } else if (budget.length() >= 4) {
                    float_num = num / 1000;
                    s = String.format("%.2f", float_num);
                    String dec = s.substring(0, s.indexOf(".") + 2);
                    s = "₹ " + dec + " K";

                }

            } else {
                s = "";
            }
        } catch (Exception e) {
            Log.e("Covert exp", e + "");
        }
        return s;

    }

    public static String convertBudgetToCrore1(String budget) {
        String s = "";

        try {
            if (!budget.equalsIgnoreCase("")) {
                int num = Integer.parseInt(budget);
                float float_num = num / 10000000;
                s = String.format("%.2f", float_num);
                String dec = s.substring(s.indexOf(".") + 1);
                if (dec.equals("00")) {
                    int newnum = (int) Float.parseFloat(s);
                    s = Integer.toString(newnum);
                }
            } else {
                s = "";
            }
        } catch (Exception e) {
            Log.e("Covert exp", e + "");
        }
        return s;

    }

    public static void hideKeyboard(Activity mAcivity) {

        try {
            InputMethodManager inputManager = (InputMethodManager)
                    mAcivity.getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(mAcivity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
//            e.printStackTrace();
        }

    }

    public static int convertDip2Pixels(Context context, int dip) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, context.getResources().getDisplayMetrics());
    }

    /**
     * Method for logging the message
     *
     * @param tag     - tag for log
     * @param message - message for log
     **/
    public static void logMe(String tag, String message) {
//        if (BuildConfig.DEBUG) {
        Log.e(tag, message);
//        }
    }

//    public void getWebserviceLog(String tag, Object obj) {
//        Gson gson = new Gson();
//        logMe(tag, gson.toJson(obj));
//    }

    /**
     * Method for toast the message
     *
     * @param mContext - context of activity
     * @param message  - message to toast
     */
    public static void toastMe(Context mContext, String message) {
        if (!message.equalsIgnoreCase("")) {
            if (toast != null) {
                toast.cancel();
            }
            toast = Toast.makeText(mContext, message, Toast.LENGTH_LONG);
            toast.show();
//            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }
    }

    public static Bitmap compressImageResolution(Bitmap originalBitmap, int maxSize) {
        Bitmap processedImage;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPurgeable = true;
            int outWidth;
            int outHeight;
            int inWidth = originalBitmap.getWidth();
            int inHeight = originalBitmap.getHeight();
            if (inWidth > inHeight) {
                outWidth = maxSize;
                outHeight = (inHeight * maxSize) / inWidth;
            } else {
                outHeight = maxSize;
                outWidth = (inWidth * maxSize) / inHeight;
            }
            processedImage = Bitmap.createScaledBitmap(originalBitmap, outWidth, outHeight, true);
        } catch (Exception e) {
            processedImage = null;
        }
        return processedImage;
    }

    public static Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL("http://" + strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Mehtod for converting dates from one format to another
     *
     * @param from - date format that the actual date is in
     * @param to   - date format in which the method will convert it
     * @param date - date which have to convert in required format
     * @return Converted date
     */
    public static String convertDateFormat(String from, String to, String date) {
        SimpleDateFormat sdfFrom = new SimpleDateFormat(from);
        SimpleDateFormat sdfTo = new SimpleDateFormat(to);
        Date mDate = null;
        String convertedDate = "";

        try {
            mDate = sdfFrom.parse(date);
            convertedDate = sdfTo.format(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    /**
     * Method to get configures account's email address from device
     *
     * @param mContext - context of Activity
     * @return ArrayList of accounts
     */
//  public ArrayList<String> getConfigiredAccounts(Context mContext) {
//    ArrayList<String> emails = new ArrayList<>();
//
//    Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
//    Account[] accounts = AccountManager.get(mContext).getAccounts();
//    for (Account account : accounts) {
//      if (emailPattern.matcher(account.name).matches()) {
//        emails.add(account.name);
//      }
//    }
//    return emails;
//  }
//
//  public String getUserDeviceEmailID(Context context) {
//    ArrayList<String> mEmailIDs = new ArrayList<>();
//    try {
//      Account[] accounts = AccountManager.get(context)
//          .getAccounts();
//      for (Account account : accounts) {
//        // Check here for the type and name to find the email
//        if (account.type.equalsIgnoreCase("com.google")) {
//          mEmailIDs.add(account.name);
//        }
//      }
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//
//    String sEmailID = mEmailIDs.toString().replace("[", "").replace("]", "");
//    return sEmailID;
//  }

    /**
     * Method to check the entered email address is valid or not
     *
     * @param email - email address to check
     * @return true / false accroding to email address
     */
    public static boolean isEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * Mehtod to get device's unique device id
     *
     * @return unique device id
     */
    public String getUniqueIdOfDevice() {
        String deviceId = null;
        String serial = null;

        String m_szDevIDShort = "35" + (Build.BOARD.length() % 10)
                + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10)
                + (Build.DEVICE.length() % 10)
                + (Build.MANUFACTURER.length() % 10)
                + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);

        try {
            serial = Build.class.getField("SERIAL").get(null)
                    .toString();
            logMe("unique-1", serial);
            logMe("unique-2", m_szDevIDShort);
            // Go ahead and return the serial for api => 9
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode())
                    .toString();
        } catch (Exception e) {
            // String needs to be initialized
            serial = "serial"; // some value
        }

        deviceId = new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();

        return deviceId;
    }

    public void toastAlert(Activity mContext, String message) {

        if (!message.equalsIgnoreCase("")) {
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }
    }

    public void toastInfo(Activity mContext, String message) {

        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    public boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void openCallScreen(Context context, String phoneNo) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phoneNo));
        context.startActivity(callIntent);
    }

    public static void openShareMailIntent(Activity activity, String Recipient) {
        try {
            ShareCompat.IntentBuilder.from(activity)
                    .setType("message/rfc822")
                    .addEmailTo(Recipient)
                    .setSubject("")
                    .setText("")
                    .setChooserTitle("Choose")
                    .startChooser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static String getCurrentDate() {
        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddhhmmss", Locale.US);
            formattedDate = simpleDateFormat.format(c.getTime());
        } catch (Exception e) {
            formattedDate = "";
        }
        return formattedDate;
    }

    public static void call360View(Uri view360, Context mContext) {
        Intent browserIntent = new Intent(
                Intent.ACTION_VIEW,
                view360);
        mContext.startActivity(browserIntent);
    }

    public static void ExpandCompressDescription(final TextView textAboutMe, final TextView tvMore) {
        final int[] totalLineCount = new int[1];
        final int[] maxLineCount = new int[1];
        textAboutMe.post(new Runnable() {
            @Override
            public void run() {
                totalLineCount[0] = textAboutMe.getLineCount();
                // Use lineCount here
                if (totalLineCount[0] > 5) {
                    tvMore.setVisibility(View.VISIBLE);
                    tvMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            maxLineCount[0] = textAboutMe.getMaxLines();
                            if (maxLineCount[0] == 5) {
                                textAboutMe.setMaxLines(100);
                                tvMore.setText("....LESS");
                            } else {
                                textAboutMe.setMaxLines(5);
                                tvMore.setText("....MORE");
                            }
                        }
                    });
                } else {
                    tvMore.setVisibility(View.GONE);
                }
            }
        });
    }

    public static void showAlertDialog(String title, String message, String postivebuttontext, String negativebuttontext, Context context, final OnItemClickListener listener) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(postivebuttontext, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onClick("", null);
                    }

                })
                .setNegativeButton(negativebuttontext, null)
                .show();
    }

    public static void showAlertDialog(String title, String message, String postivebuttontext, String negativebuttontext, Context context, final OnItemClickListener positivelistener, final OnItemClickListener negativelistener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(postivebuttontext, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                positivelistener.onClick("", null);
            }

        });
        builder.setNegativeButton(negativebuttontext, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                negativelistener.onClick("", null);
            }

        });

        AlertDialog alert = builder.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(context.getResources().getColor(R.color.colorAccent));
    }
    public static void showPermissionDialog(final Activity context) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", context.getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }


    /**
     * Convert URL to Bitmap using AsyncTask
     */
    public class GetBitmapFromUrl extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }

    }


    public static void openGoogleMap(Context mContext, String Lat, String Lng, String mName) {
        if (TextUtils.isEmpty(Lat) || TextUtils.isEmpty(Lng)) {
            toastMe(mContext, "Location unavailable");
        } else {
            Double mLatitude = TextUtils.isEmpty(Lat) ? 0.0 : Double.parseDouble(Lat);
            Double mLongitude = TextUtils.isEmpty(Lat) ? 0.0 : Double.parseDouble(Lng);
            Uri gmmIntentUri = null;
            gmmIntentUri = Uri.parse("geo:" + Double.toString(mLatitude)
                    + "," + Double.toString(mLongitude) + "?q="
                    + Double.toString(mLatitude) + ","
                    + Double.toString(mLongitude) + "(" + mName + ")");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                    gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            mContext.startActivity(mapIntent);
        }
    }

    /* Method shows the Progress Dialog for Activity */
    public static ProgressDialog showProgressDialog(Activity activity) {
        ProgressDialog mProgressDialog = null;
        try {
            mProgressDialog = ProgressDialog.show(activity, "", "Loading...", true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mProgressDialog;
    }

    /* Method shows the Progress Dialog for Context */
    public static ProgressDialog showProgressDialog(Context context) {
        ProgressDialog mProgressDialog = null;
        try {
            mProgressDialog = ProgressDialog.show(context, "", "Loading...", true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.setCanceledOnTouchOutside(true);
            mProgressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mProgressDialog;
    }

    /* Method shows the dismissable Progress Dialog for Activity */
    public static ProgressDialog showNonDismissibleProgressDialog(Activity activity) {
        ProgressDialog mProgressDialog = null;
        try {
            mProgressDialog = ProgressDialog.show(activity, "", "Loading...", true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);

            final ProgressDialog finalMProgressDialog = mProgressDialog;
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    finalMProgressDialog.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mProgressDialog;
    }

    public static void dismissProgressDialog(ProgressDialog mProgressDialog) {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissDialog(Dialog mProgressDialog) {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkDangerousPermission(Activity mContext) {
        boolean isPermissionGranted = false;
        if (ContextCompat.checkSelfPermission(mContext, permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(mContext, permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(mContext, permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(mContext, permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(mContext, new String[]{permission.SEND_SMS,
                    permission.READ_SMS,
                    permission.READ_CONTACTS,
                    permission.WRITE_CONTACTS}, 100);
            isPermissionGranted = true;
        } else {
            isPermissionGranted = true;
        }

        return isPermissionGranted;
    }

//  public static int getNotificationSmallIcon() {
//    boolean whiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
//    return whiteIcon ? R.drawable.notification_push : R.mipmap.ic_launcher;
//  }

    public static String getUrlFileName(String url) {
        String urlName;

        try {
            String[] urlNameArray = url.split("/");
            urlName = urlNameArray[urlNameArray.length - 1];
        } catch (Exception e) {
            AppUtils.logMe("GetUrlException", e.toString());
            return String.valueOf(SystemClock.currentThreadTimeMillis());
        }

        return urlName;
    }

    public static File createImageFile() {
        // Create an image file name
        String imageFileName = "JPEG_" + "DEFAULT" + "_";
//    File storageDir = getContext().getFilesDir();
        File spentaDirectory = new File(
                getExternalStorageDirectory() + File.separator + Constants.APPLICATION_DIR
                        + File.separator);
        if (!spentaDirectory.exists()) {
            spentaDirectory.mkdirs();
        }
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    spentaDirectory      /* directory */
            );
        } catch (IOException e) {
            image = new File("");
        }
        return image;
    }

//    public static boolean writeResponseBodyToDisk(Context context, String mFileName,
//                                                  ResponseBody body) {
//        try {
//            // todo change the file location/name according to your needs
//
//            // create a File object for the parent directory
//            File spentaDirectory = new File(
//                    getExternalStorageDirectory() + File.separator + Constants.APPLICATION_DIR + File.separator);
//            // have the object build the directory structure, if needed.
//            spentaDirectory.mkdirs();
//            // create a File object for the output file
//            File outputFile = new File(spentaDirectory, mFileName);
//
//            InputStream inputStream = null;
//            OutputStream outputStream = null;
//
//            try {
//                byte[] fileReader = new byte[4096];
//
//                long fileSize = body.contentLength();
//                long fileSizeDownloaded = 0;
//
//                inputStream = body.byteStream();
//                outputStream = new FileOutputStream(outputFile);
//
//                while (true) {
//                    int read = inputStream.read(fileReader);
//
//                    if (read == -1) {
//                        break;
//                    }
//
//                    outputStream.write(fileReader, 0, read);
//
//                    fileSizeDownloaded += read;
//
//                    Log.d("Download Files", "file download: " + fileSizeDownloaded + " of " + fileSize);
//                }
//
//                outputStream.flush();
//
//                return true;
//            } catch (IOException e) {
//                return false;
//            } finally {
//                if (inputStream != null) {
//                    inputStream.close();
//                }
//
//                if (outputStream != null) {
//                    outputStream.close();
//                }
//            }
//        } catch (IOException e) {
//            return false;
//        }
//    }

    /**
     * Fragments Transition
     **/


    public static String repeat(String str, int count) {
        if (count <= 0) {
            return "";
        }
        return new String(new char[count]).replace("\0", str);
    }

    public static void clearLastStack(AppCompatActivity mActivity) {


        FragmentManager fm = mActivity.getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
        }

    }








    public static int getBackStackCount(AppCompatActivity mActivity) {

        int countBackstack = 0;

        FragmentManager mFragmentManager = mActivity.getSupportFragmentManager();

        countBackstack = mFragmentManager.getBackStackEntryCount();

        return countBackstack;
    }

    public static int getScreenHeight(Context mContext) {
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        int height = metrics.heightPixels;
        return height;
    }

    public static int getScreenWidth(Context mContext) {
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        return width;
    }



    public static String getTimeFromVideoDuration(long duration) {
        return String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(duration),
                TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(duration)));
    }

    public static String makeCamelCase(String stringToChange) {
        String[] strArray = stringToChange.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            if (!TextUtils.isEmpty(s)) {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                builder.append(cap + " ");
            }
        }

        return builder.toString();
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

//    public static void fetchEmailsFromDevice(Context context, AutoCompleteTextView autoCompleteTextView) {
//
//        final String email_pattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
//        Pattern EMAIL_PATTERN = Pattern.compile(email_pattern);
//        Account[] accounts = AccountManager.get(context).getAccounts();
//        Set<String> emailSet = new HashSet<String>();
//        for (Account account : accounts) {
//            if (EMAIL_PATTERN.matcher(account.name).matches()) {
//                emailSet.add(account.name);
//            }
//        }
//        autoCompleteTextView.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, new ArrayList<String>(emailSet)));
//
//    }

    public static void showView(View view) {
        view.animate().alpha(1).setDuration(FADE_INTERVAL);
        view.setVisibility(View.VISIBLE);
    }

    public static void hideView(View view) {
        view.animate().alpha(0).setDuration(FADE_INTERVAL);
        view.setVisibility(View.GONE);
    }

    public static void invisibleView(View view) {
        view.animate().alpha(0).setDuration(FADE_INTERVAL);
        view.setVisibility(View.INVISIBLE);
    }

    /**
     * This method is use to select image for notification icon according to OS of device
     **/



    public static String getTextCamelCase(String name) {
        String value = "";

        if (name != null && name.length() > 0) {
            String[] words = name.toString().split(" ");
            StringBuilder sb = new StringBuilder();
            if (words[0].length() > 0) {
                sb.append(Character.toUpperCase(words[0].charAt(0)) + words[0].subSequence(1, words[0].length()).toString().toLowerCase());
                for (int i = 1; i < words.length; i++) {
                    sb.append(" ");
                    sb.append(Character.toUpperCase(words[i].charAt(0)) + words[i].subSequence(1, words[i].length()).toString().toLowerCase());
                }
            }
            value = sb.toString();
        }
        return value;
    }

//    public static void animateFromBottomtoTop(Context context, View view, Boolean value) {
//        if (value) {
//            Animation bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_up);
//            view.startAnimation(bottomUp);
//        } else {
//            Animation bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_down);
//            view.startAnimation(bottomUp);
//        }
//    }

    public static void setPersonNameInputFilter(EditText editText, int maxLength) {
        // TODO: 7/29/17 Add this in create pet and owner adoption

        StringBuilder sb;
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
        editText.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        StringBuilder sb = new StringBuilder(end - start);
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        for (int i = start; i < end; ++i) {
                            char c = cs.charAt(i);
                            if (dStart == 0 && cs.toString().equals(" ")) {
                                sb.append("");
                            } else if (Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ]*").
                                    matcher(String.valueOf(c)).matches()) {
                                sb.append(c);
                            }
                        }
                        if (cs instanceof Spanned) {
                            SpannableString sp = new SpannableString(sb);
                            TextUtils.copySpansFrom((Spanned) cs, start, sb.length(), null, sp, 0);
                            return sp;
                        } else {
                            return sb;
                        }
                    }
                }
                , new InputFilter.LengthFilter(maxLength)});
    }

    public static void setDescriptionInputFilter(EditText editText, boolean isMultiline, int maxLength) {
        if (maxLength == 0) {
            maxLength = 1000;
        }
        // TODO: 7/29/17 Add this in create event and create emergency and pet and owner adoption
        if (isMultiline) {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        } else {
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        }
        editText.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        for (int i = start; i < end; ++i) {
                            if (dStart == 0 && cs.equals(" ")) {
                                return "";
                            }
                            return cs;
                        }
                        return "";
                    }
                }
                , new InputFilter.LengthFilter(maxLength)});
    }

  /*  public static void callIntent(BaseActivity activity, String number) {
        if (!TextUtils.isEmpty(number)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + number));
            activity.startActivity(intent);
        } else {
            activity.showToast(R.string.invalid_mobile_found);
        }
    }*/

//    public static String getFirstImageUrl(List<?> objectArrayList) {
//
//        String imageUrl = "";
//
//        if (objectArrayList != null) {
//            for (int i = 0; i <= objectArrayList.size(); i++) {
//
//                if (i == 0) {
//
//                    Object o = objectArrayList.get(i);
//
//                    if (o instanceof StoreProductListingResponse.Images) {
//                        imageUrl = ((StoreProductListingResponse.Images) o).getSrc();
//                        break;
//                    }
//
//                }
//
//            }
//        }
//
//        return imageUrl;
//
//    }

    public static double getProductOffPercent(String newAmount, String oldAmount) {
        double percentOff;

        try {

            double diff = Double.parseDouble(newAmount) / Double.parseDouble(oldAmount);

            percentOff = diff * 100;
            DecimalFormat df = new DecimalFormat("#.##");
            percentOff = Double.valueOf(df.format(percentOff));

        } catch (Exception e) {

            AppUtils.logMe("Store Amount Exception", e.toString());
            return 0.0;
        }

        return percentOff;
    }

    public static void setImageOffline(SimpleDraweeView imageview, String fileName) {
        imageview.setImageURI(Uri.parse("file://" + fileName));
//        if (fileName.length() > 0) {
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inJustDecodeBounds = true;
//            BitmapFactory.decodeFile(fileName, options);
//            int width = options.outWidth;
//            int height = options.outHeight;
//            AppUtils.logMe("image original width & height ", width + "," + height);
//
//            if (width > 1000 && height > 1000) {
//                BitmapFactory.Options bitopt = new BitmapFactory.Options();
//                bitopt.inSampleSize = 4;
//                imageview.setImageBitmap(BitmapFactory.decodeFile(fileName, bitopt));
//            } else {
//                imageview.setImageBitmap(BitmapFactory.decodeFile(fileName));
//            }
//        }
    }

    public static String getImageUrl(String imageString) {
        return "https://xpandretail-assets.s3.amazonaws.com/" + imageString;
    }



    public static String makeFirstLetterCap(String str) {
        if (str.length() > 0) {
            StringBuilder sb = new StringBuilder(str);
            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
            return sb.toString();
        } else {
            return str;
        }
    }




    public static int convertDpToPixel(int dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = dp * ((int)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static int convertPixelsToDp(int px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = px / ((int)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }



    public static String getCurrentActivity(Activity context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        String[] name = cn.getClassName().split("\\.");
        String classname = name[name.length - 1];
        logMe("activity", classname);
        return classname;
    }

}
