package com.tiara.events;


import com.tiara.ui.model.Response.FilterCopy;

public class FilterEvent {
    private FilterCopy filterCopy;

    public FilterEvent(FilterCopy filterCopy) {
        this.filterCopy = filterCopy;
    }

    public FilterCopy getFilterCopy() {
        return filterCopy;
    }
}