package com.tiara.mvp.PresenterImpl;


import android.text.TextUtils;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.mvp.Model.LoginModel;
import com.tiara.mvp.Presenter.LoginPresenter;
import com.tiara.mvp.View.BaseView;
import com.tiara.ui.model.Response.CommonErrorResponse;
import com.tiara.ui.model.Response.CustomerLoginRes;
import com.tiara.ui.model.Response.SalesManLoginRes;
import com.tiara.utils.AppUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by itrs-203 on 12/22/17.
 */

public class LoginPresenterImpl implements LoginPresenter {

    private BaseView baseView;
    private LoginModel mainModel;

    public LoginPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
        mainModel = new LoginModel(this);
    }

    private LoginModel getMainModel() {
        if (mainModel == null) {
            mainModel = new LoginModel(this);
        }
        return mainModel;
    }
    @Override
    public void getLoginSalesMan(boolean isConnected, String username, String password) {
        if (isConnected) {
            mainModel.getSalesManLoginResponse(username,password);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getLoginCustomer(boolean isConnected, String username, String password) {
        if (isConnected) {
            mainModel.getCustomerLoginResponse(username,password);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }
    private boolean isValidMobile(String mobile) {
        if (TextUtils.isEmpty(mobile)) {
            return false;
        } else {
            Pattern p = Pattern.compile("^[6789]\\d{9,9}$");
            Matcher m = p.matcher(mobile);
            return m.matches();
        }
    }
    private boolean isValidEmail(String emailID) {
        if (TextUtils.isEmpty(emailID)) {
            return false;
        } else {
            return AppUtils.isEmail(emailID);
        }
    }

    @Override
    public boolean validateRegistration(String mobile, String password) {
        if (!isValidMobile(mobile)) {
            baseView.showToast(R.string.invalid_mobile);
            return false;
        }  else if (!isValidPassword(password)) {
            return false;
        } else {
            return true;
        }
    }
    public boolean isValidPassword(String password) {
        if (TextUtils.isEmpty(password)) {
            baseView.showToast(R.string.invalid_password);
            return false;
        } /*else if (password.length() <  8 ) {
            baseView.showToast(R.string.invalid_password_length);
            return false;
        }*/ else {
            return true;
        }
    }


    @Override
    public void onResponse(Object responseJson, boolean isSuccess, String apiName) {
        baseView.hideProgressDialog();
        switch (apiName) {
            case ApiNames.SALES_LOGIN:
                if (isSuccess) {
                    SalesManLoginRes response = (SalesManLoginRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            if(response.getData()!=null && response.getData().size() >0) {
                                baseView.onResponseSuccess(response, apiName);
                            }else{
                                baseView.showToast("Invalid Credentials");
                            }
                        } else {
                            /*if (response.getMessage() != null && response.getMessage().length() > 0) {
                                baseView.showToast(response.getMessage());
                            }*/
                        }
                    } else {
                        baseView.onResponseFailure("");
                    }
                } else {
                    baseView.onResponseFailure("");
                }
                break;
            case ApiNames.CUSTOMER_LOGIN:
                if (isSuccess) {
                    CustomerLoginRes response = (CustomerLoginRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            if(response.getData()!=null && response.getData().size() >0) {
                                baseView.onResponseSuccess(response, apiName);
                            }else{
                                baseView.showToast("Invalid Credentials");
                            }
                        } else {
                            /*if (response.getMessage() != null && response.getMessage().length() > 0) {
                                baseView.showToast(response.getMessage());
                            }*/
                        }
                    } else {
                        baseView.onResponseFailure("");
                    }
                } else {
                    baseView.onResponseFailure("");
                }
                break;
        }
    }

    @Override
    public void onResponse(Object responseJson, boolean isSuccess, String apiName, String searchKey) {

    }

    @Override
    public void onResponse(Object responseJson, boolean isSuccess, String apiName, int position, String grpid) {

    }


    @Override
    public void onError(Object responseJson, String apiName) {
        baseView.hideProgressDialog();
        CommonErrorResponse response = (CommonErrorResponse) responseJson;
        if (response != null) {
            baseView.onErrorResponse(response, apiName);
        }
    }
}
