package com.tiara.mvp.PresenterImpl;

import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.tiara.mvp.Presenter.SearchPresenter;
import com.tiara.mvp.View.BaseView;
import com.tiara.mvp.View.SearchProcessView;


import rx.functions.Action1;
import rx.functions.Func1;

public class SearchPresenterImpl implements SearchPresenter {

    private BaseView baseView;
    private SearchProcessView searchView;

    public SearchPresenterImpl(BaseView baseView, SearchProcessView searchView) {
        this.baseView = baseView;
        this.searchView = searchView;
    }

    @Override
    public void getSearchKeywordDetails(final EditText edtSearch, final String mUserID) {
        RxTextView.textChanges(edtSearch)
                .map(new Func1<CharSequence, Integer>() {
                    @Override
                    public Integer call(CharSequence inputText) {
                        if (TextUtils.isEmpty(edtSearch.getText().toString())) {
                            return 0;
                        } else if (edtSearch.getText().length() > 0
                                && edtSearch.getText().length() < 3) {
                            return 1;
                        } else {
                            return 2;
                        }
                    }
                })
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer isValid) {
                        Log.e("Search Length", isValid + "");
                        if (/*isValid == 1 || */isValid == 0) {


                            /*before using it for Friends check the usage of this method(used in
                             adoption, it causes crashes on adoption->activity)*/

                            searchView.onSearchCompleted(null);

                        } else {
                            /*new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    searchView.onSearchStarted(edtSearch.getText().toString());
                                }
                            }, 1000);*/
                            searchView.onSearchStarted(edtSearch.getText().toString());
                        }
                    }
                });
    }
}
