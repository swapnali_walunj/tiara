package com.tiara.mvp.PresenterImpl;


import com.tiara.mvp.Presenter.ConnectionPresenter;
import com.tiara.mvp.View.ConnectionView;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.cache.ConnectionBuddyCache;
import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;

/**
 * Created by user on 24/3/17.
 */

public class ConnectionPresenterImpl implements ConnectionPresenter, ConnectivityChangeListener {
    private ConnectionView connectionView;

    public ConnectionPresenterImpl(ConnectionView view) {
        this.connectionView = view;
    }

    @Override
    public void init(boolean hasSavedInstanceState) {
        if (!hasSavedInstanceState) {
            try {
                ConnectionBuddyCache.clearLastNetworkState(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        connectionView.initViews(ConnectionBuddy.getInstance().hasNetworkConnection());

    }

    @Override
    public void registerForNetworkUpdates() {
        try {
            ConnectionBuddy.getInstance().registerForConnectivityEvents(this, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unregisterFromNetworkUpdates() {
        try {
            ConnectionBuddy.getInstance().unregisterFromConnectivityEvents(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionChange(ConnectivityEvent event) {
        connectionView.onConnectionChangeEvent(event);
    }
}
