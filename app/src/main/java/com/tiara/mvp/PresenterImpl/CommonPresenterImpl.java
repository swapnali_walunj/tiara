package com.tiara.mvp.PresenterImpl;

import com.tiara.R;
import com.tiara.api.ApiNames;
import com.tiara.mvp.Model.CommonBaseModule;
import com.tiara.mvp.Presenter.CommonPresenter;
import com.tiara.mvp.View.BaseView;
import com.tiara.ui.model.Response.AddressResponse;
import com.tiara.ui.model.Response.ApprovedOrderDetails;
import com.tiara.ui.model.Response.ApprovedOrderResponse;
import com.tiara.ui.model.Response.ColorResponse;
import com.tiara.ui.model.Response.CommonErrorResponse;
import com.tiara.ui.model.Response.CustomerListResponse;
import com.tiara.ui.model.Response.FILLGSTRes;
import com.tiara.ui.model.Response.LabourChargeOTPResponse;
import com.tiara.ui.model.Response.LabourChargeResponse;
import com.tiara.ui.model.Response.LiveStockDesignRes;
import com.tiara.ui.model.Response.LiveStockOrderResponse;
import com.tiara.ui.model.Response.LiveStockRes;
import com.tiara.ui.model.Response.LiveStockSaveAllResponse;
import com.tiara.ui.model.Response.LiveStockSaveResponse;
import com.tiara.ui.model.Response.MyCustomerResponse;
import com.tiara.ui.model.Response.OldPendingOrderDetails;
import com.tiara.ui.model.Response.OldPendingOrderResponse;
import com.tiara.ui.model.Response.OldStockOrderResponse;
import com.tiara.ui.model.Response.PendingOrderDetails;
import com.tiara.ui.model.Response.PendingOrderResponse;
import com.tiara.ui.model.Response.ProfileResponse;
import com.tiara.ui.model.Response.RateResponse;
import com.tiara.ui.model.Response.SizeResponse;
import com.tiara.ui.model.Response.ViewMoreDesignResponse;

public class CommonPresenterImpl implements CommonPresenter {

    private BaseView baseView;
    private CommonBaseModule mainModel;

    public CommonPresenterImpl(BaseView baseView) {
        this.baseView = baseView;
        mainModel = new CommonBaseModule(this);
    }

    private CommonBaseModule getMainModel() {
        if (mainModel == null) {
            mainModel = new CommonBaseModule(this);
        }
        return mainModel;
    }

    @Override
    public void onResponse(Object responseJson, boolean isSuccess, String apiName) {
        baseView.hideProgressDialog();
        switch (apiName) {
            case ApiNames.PROFILE:
                if (isSuccess) {
                    ProfileResponse response = (ProfileResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            if(response.getData()!=null && response.getData().size() >0) {
                                baseView.onResponseSuccess(response, apiName);
                            }else{
                                baseView.showToast("Invalid Credentials");
                            }
                        } else {
                            /*if (response.getMessage() != null && response.getMessage().length() > 0) {
                                baseView.showToast(response.getMessage());
                            }*/
                        }
                    } else {
                        baseView.onResponseFailure("");
                    }
                } else {
                    baseView.onResponseFailure("");
                }
                break;
            case ApiNames.LABOUR_CHARGE:
            baseView.hideProgress();
            if (isSuccess) {
                LabourChargeResponse response = (LabourChargeResponse) responseJson;
                if (response != null) {
                    if (response.getStatus()) {
                        baseView.onResponseSuccess(response, ApiNames.LABOUR_CHARGE);
                    } else {
                        baseView.onResponseFailure(ApiNames.LABOUR_CHARGE);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LABOUR_CHARGE);
                }
            } else {
                baseView.onResponseFailure(ApiNames.LABOUR_CHARGE);
            }
                break;
            case ApiNames.LIVE_STOCK:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockRes response = (LiveStockRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVE_STOCK);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVE_STOCK);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVE_STOCK);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVE_STOCK);
                }
                break;
            case ApiNames.FILLGST:
                baseView.hideProgress();
                if (isSuccess) {
                    FILLGSTRes response = (FILLGSTRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.FILLGST);
                        } else {
                            baseView.onResponseFailure(ApiNames.FILLGST);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.FILLGST);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.FILLGST);
                }
                break;

            case ApiNames.OLD_STOCK:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockRes response = (LiveStockRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLD_STOCK);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLD_STOCK);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLD_STOCK);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLD_STOCK);
                }
                break;
            case ApiNames.PENDING_ORDER:
                baseView.hideProgress();
                if (isSuccess) {
                    PendingOrderResponse response = (PendingOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.PENDING_ORDER);
                        } else {
                            baseView.onResponseFailure(ApiNames.PENDING_ORDER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.PENDING_ORDER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.PENDING_ORDER);
                }
                break;
            case ApiNames.OLD_PENDING_ORDER:
                baseView.hideProgress();
                if (isSuccess) {
                    OldPendingOrderResponse response = (OldPendingOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLD_PENDING_ORDER);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER);
                }
                break;
            case ApiNames.LIVE_STOCK_DESIGN:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockDesignRes response = (LiveStockDesignRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVE_STOCK_DESIGN);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVE_STOCK_DESIGN);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVE_STOCK_DESIGN);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVE_STOCK_DESIGN);
                }
                break;
            case ApiNames.GET_COLOR_DETAILS:
                baseView.hideProgress();
                if (isSuccess) {
                    ColorResponse response = (ColorResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.GET_COLOR_DETAILS);
                        } else {
                            baseView.onResponseFailure(ApiNames.GET_COLOR_DETAILS);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.GET_COLOR_DETAILS);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.GET_COLOR_DETAILS);
                }
                break;
            case ApiNames.GET_SIZE_DETAILS:
                baseView.hideProgress();
                if (isSuccess) {
                    SizeResponse response = (SizeResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.GET_SIZE_DETAILS);
                        } else {
                            baseView.onResponseFailure(ApiNames.GET_SIZE_DETAILS);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.GET_SIZE_DETAILS);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.GET_SIZE_DETAILS);
                }
                break;

            case ApiNames.GET_MORE_DETAILS:
                baseView.hideProgress();
                if (isSuccess) {
                    ViewMoreDesignResponse response = (ViewMoreDesignResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.GET_MORE_DETAILS);
                        } else {
                            baseView.onResponseFailure(ApiNames.GET_MORE_DETAILS);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.GET_MORE_DETAILS);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.GET_MORE_DETAILS);
                }
                break;

            case ApiNames.GET_SEARCH_MORE_DETAILS:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockDesignRes response = (LiveStockDesignRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.GET_SEARCH_MORE_DETAILS);
                        } else {
                            baseView.onResponseFailure(ApiNames.GET_SEARCH_MORE_DETAILS);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.GET_SEARCH_MORE_DETAILS);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.GET_SEARCH_MORE_DETAILS);
                }
                break;
            case ApiNames.CATALOG_DESIGN:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockDesignRes response = (LiveStockDesignRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.CATALOG_DESIGN);
                        } else {
                            baseView.onResponseFailure(ApiNames.CATALOG_DESIGN);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.CATALOG_DESIGN);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.CATALOG_DESIGN);
                }
                break;
            case ApiNames.GET_MORE_DETAILS_CATALOG:
                baseView.hideProgress();
                if (isSuccess) {
                    ViewMoreDesignResponse response = (ViewMoreDesignResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.GET_MORE_DETAILS_CATALOG);
                        } else {
                            baseView.onResponseFailure(ApiNames.GET_MORE_DETAILS_CATALOG);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.GET_MORE_DETAILS_CATALOG);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.GET_MORE_DETAILS_CATALOG);
                }
                break;
            case ApiNames.LIVE_STOCK_ORDER:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockOrderResponse response = (LiveStockOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVE_STOCK_ORDER);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVE_STOCK_ORDER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVE_STOCK_ORDER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVE_STOCK_ORDER);
                }
                break;
            case ApiNames.OLD_STOCK_ORDER:
                baseView.hideProgress();
                if (isSuccess) {
                    OldStockOrderResponse response = (OldStockOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLD_STOCK_ORDER);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLD_STOCK_ORDER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLD_STOCK_ORDER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLD_STOCK_ORDER);
                }
                break;
            case ApiNames.LIVESTOCK_FILLADDRES:
                baseView.hideProgress();
                if (isSuccess) {
                    AddressResponse response = (AddressResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVESTOCK_FILLADDRES);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLADDRES);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLADDRES);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLADDRES);
                }
                break;
            case ApiNames.LIVESTOCK_SAVE:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockSaveResponse response = (LiveStockSaveResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVESTOCK_SAVE);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVESTOCK_SAVE);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVESTOCK_SAVE);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVESTOCK_SAVE);
                }
                break;
            case ApiNames.LIVESTOCK_SAVEALL:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockSaveAllResponse response = (LiveStockSaveAllResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVESTOCK_SAVEALL);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVESTOCK_SAVEALL);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVESTOCK_SAVEALL);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVESTOCK_SAVEALL);
                }
                break;
            case ApiNames.LIVESTOCK_FILLRATE:
                baseView.hideProgress();
                if (isSuccess) {
                    RateResponse response = (RateResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVESTOCK_FILLRATE);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLRATE);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLRATE);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLRATE);
                }
                break;
            case ApiNames.OLDSTOCK_FILLADDRES:
                baseView.hideProgress();
                if (isSuccess) {
                    AddressResponse response = (AddressResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLDSTOCK_FILLADDRES);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLADDRES);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLADDRES);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLADDRES);
                }
                break;
            case ApiNames.OLDSTOCK_SAVE:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockSaveResponse response = (LiveStockSaveResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLDSTOCK_SAVE);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLDSTOCK_SAVE);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLDSTOCK_SAVE);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLDSTOCK_SAVE);
                }
                break;
            case ApiNames.OLDSTOCK_SAVEALL:
                baseView.hideProgress();
                if (isSuccess) {
                    LiveStockSaveAllResponse response = (LiveStockSaveAllResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLDSTOCK_SAVEALL);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLDSTOCK_SAVEALL);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLDSTOCK_SAVEALL);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLDSTOCK_SAVEALL);
                }
                break;
            case ApiNames.OLDSTOCK_FILLRATE:
                baseView.hideProgress();
                if (isSuccess) {
                    RateResponse response = (RateResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLDSTOCK_FILLRATE);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLRATE);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLRATE);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLRATE);
                }
                break;

            case ApiNames.APPROVED_ORDER:
                baseView.hideProgress();
                if (isSuccess) {
                    ApprovedOrderResponse response = (ApprovedOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.APPROVED_ORDER);
                        } else {
                            baseView.onResponseFailure(ApiNames.APPROVED_ORDER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.APPROVED_ORDER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.APPROVED_ORDER);
                }
                break;

           case ApiNames.PENDING_ORDER_DETAILS:
                baseView.hideProgress();
                if (isSuccess) {
                    PendingOrderDetails response = (PendingOrderDetails) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.PENDING_ORDER_DETAILS);
                        } else {
                            baseView.onResponseFailure(ApiNames.PENDING_ORDER_DETAILS);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.PENDING_ORDER_DETAILS);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.PENDING_ORDER_DETAILS);
                }
                break;

            case ApiNames.OLD_PENDING_ORDER_DETAILS:
                baseView.hideProgress();
                if (isSuccess) {
                    OldPendingOrderDetails response = (OldPendingOrderDetails) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLD_PENDING_ORDER_DETAILS);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER_DETAILS);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER_DETAILS);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER_DETAILS);
                }
                break;
            case ApiNames.APPROVED_ORDER_DETAILS:
                baseView.hideProgress();
                if (isSuccess) {
                    ApprovedOrderDetails response = (ApprovedOrderDetails) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.APPROVED_ORDER_DETAILS);
                        } else {
                            baseView.onResponseFailure(ApiNames.APPROVED_ORDER_DETAILS);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.APPROVED_ORDER_DETAILS);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.APPROVED_ORDER_DETAILS);
                }
                break;

            case ApiNames.LABOURCHARGE_OTP:
                baseView.hideProgress();
                if (isSuccess) {
                    LabourChargeOTPResponse response = (LabourChargeOTPResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LABOURCHARGE_OTP);
                        } else {
                            baseView.onResponseFailure(ApiNames.LABOURCHARGE_OTP);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LABOURCHARGE_OTP);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LABOURCHARGE_OTP);
                }
                break;
            case ApiNames.CHANGE_PASSWORD:
                baseView.hideProgress();
                if (isSuccess) {
                    CommonErrorResponse response = (CommonErrorResponse) responseJson;
                    if (response != null) {
                        //if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.CHANGE_PASSWORD);
                        /*} else {
                            baseView.onResponseFailure(ApiNames.LABOURCHARGE_OTP);
                        }*/
                    } else {
                        //baseView.onResponseFailure(ApiNames.LABOURCHARGE_OTP);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LABOURCHARGE_OTP);
                }
                break;

            case ApiNames.MY_CUSTOMER:
                baseView.hideProgress();
                if (isSuccess) {
                    MyCustomerResponse response = (MyCustomerResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.MY_CUSTOMER);
                        } else {
                            baseView.onResponseFailure(ApiNames.MY_CUSTOMER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.MY_CUSTOMER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.MY_CUSTOMER);
                }
                break;
            case ApiNames.PENDING_ORDER_SALESMAN:
                baseView.hideProgress();
                if (isSuccess) {
                    PendingOrderResponse response = (PendingOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.PENDING_ORDER_SALESMAN);
                        } else {
                            baseView.onResponseFailure(ApiNames.PENDING_ORDER_SALESMAN);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.PENDING_ORDER_SALESMAN);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.PENDING_ORDER_SALESMAN);
                }
                break;
            case ApiNames.OLD_PENDING_ORDER_SALESMAN:
                baseView.hideProgress();
                if (isSuccess) {
                    OldPendingOrderResponse response = (OldPendingOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLD_PENDING_ORDER_SALESMAN);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER_SALESMAN);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER_SALESMAN);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLD_PENDING_ORDER_SALESMAN);
                }
                break;

            case ApiNames.APPROVED_ORDER_SALESMAN:
                baseView.hideProgress();
                if (isSuccess) {
                    ApprovedOrderResponse response = (ApprovedOrderResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.APPROVED_ORDER_SALESMAN);
                        } else {
                            baseView.onResponseFailure(ApiNames.APPROVED_ORDER_SALESMAN);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.APPROVED_ORDER_SALESMAN);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.APPROVED_ORDER_SALESMAN);
                }
                break;
            case ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER:
                baseView.hideProgress();
                if (isSuccess) {
                    CustomerListResponse response = (CustomerListResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER);
                        } else {
                            baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER);
                }
                break;
            case ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER:
                baseView.hideProgress();
                if (isSuccess) {
                    CustomerListResponse response = (CustomerListResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER);
                        } else {
                            baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER);
                        }
                    } else {
                        baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER);
                    }
                } else {
                    baseView.onResponseFailure(ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER);
                }
                break;
        }
    }

    @Override
    public void onResponse(Object responseJson, boolean isSuccess, String apiName, String searchKey) {
        baseView.hideProgressDialog();
        switch (apiName) {
            case ApiNames.LIVE_STOCK_SEARCH:
                if (isSuccess) {
                    LiveStockRes response = (LiveStockRes) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            baseView.onResponseSuccess(response, apiName,searchKey);
                        } else {
                        }
                    } else {
                        baseView.onResponseFailure("");
                    }
                } else {
                    baseView.onResponseFailure("");
                }
                break;

        }
    }

    @Override
    public void onResponse(Object responseJson, boolean isSuccess, String apiName, int position, String grpid) {
        baseView.hideProgressDialog();
        switch (apiName) {
            case ApiNames.LIVESTOCK_SAVEALL:
                if (isSuccess) {
                    LiveStockSaveAllResponse response = (LiveStockSaveAllResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            /*if (response.getData() != null && response.getData().size() > 0) {*/
                                baseView.onResponseSuccess(response, apiName,position,grpid);
                            /*} else {
                                baseView.showToast("Invalid Credentials");
                            }*/
                        } else {
                            /*if (response.getMessage() != null && response.getMessage().length() > 0) {
                                baseView.showToast(response.getMessage());
                            }*/
                        }
                    } else {
                        baseView.onResponseFailure("");
                    }
                } else {
                    baseView.onResponseFailure("");
                }
                break;
            case ApiNames.OLDSTOCK_SAVEALL:
                if (isSuccess) {
                    LiveStockSaveAllResponse response = (LiveStockSaveAllResponse) responseJson;
                    if (response != null) {
                        if (response.getStatus()) {
                            /*if (response.getData() != null && response.getData().size() > 0) {*/
                            baseView.onResponseSuccess(response, apiName,position,grpid);
                            /*} else {
                                baseView.showToast("Invalid Credentials");
                            }*/
                        } else {
                            /*if (response.getMessage() != null && response.getMessage().length() > 0) {
                                baseView.showToast(response.getMessage());
                            }*/
                        }
                    } else {
                        baseView.onResponseFailure("");
                    }
                } else {
                    baseView.onResponseFailure("");
                }
                break;
        }
    }


    @Override
    public void onError(Object responseJson, String apiName) {

    }

    @Override
    public void getLiveStock(int id, int offset, boolean isConnected)  {
        if (isConnected) {
            mainModel.getLiveStockResponse(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }

    }

    @Override
    public void getGST(boolean isConnected) {
        if (isConnected) {
            mainModel.getGstState();
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getProfileDetails(boolean isConnected, int id) {
        if (isConnected) {
            mainModel.getProfileDetails(id);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getLabourCharge(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getLabourChargeResponse(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getCatelog(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getCatelogResponse(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }

    }

    @Override
    public void updateProfile(boolean isConnected, ProfileResponse.Datum profile) {
        if (isConnected) {
            mainModel.updateProfile(profile);
        } else {
            baseView.showToast(R.string.no_internet);
        }
    }



    @Override
    public void getLiveStockDesign(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getLiveStockDesign(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getColorstock(boolean isConnected) {
        if (isConnected) {
            mainModel.getColor();
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getSize(boolean isConnected) {
        if (isConnected) {
            mainModel.getSize();
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getCatalogDesign(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getCatalogkDesign(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getViewMoreDesignCatalog(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getViewMoreDesignCatalog(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getViewMoreDesign(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getViewMoreDesign(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getViewMoreDesignBysearch(String colorvalue, String sizevalue, int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getViewMoreDesignBySearch(colorvalue,sizevalue,id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getLiveStockOrder(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getLiveStockOrder(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getOldStockOrder(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getOldStockOrder(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getAddress(int custid, boolean isConnetced) {
        if (isConnetced) {
            mainModel.getAddress(custid);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getRate(int custid, boolean isConnetced) {
        if (isConnetced) {
            mainModel.getRate(custid);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getSaveLiveStock(String purchasedate, int custid, int addressid,boolean isConnetced) {
        if (isConnetced) {
            mainModel.getSaveLiveStock(purchasedate,custid,addressid);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getSaveAllLiveStock(int STOID, double Rate, int CatID, int DesignID, int ColourID, int SizeID, int Qty, int Company_ID, boolean isConnected,int position) {
        if (isConnected) {
            mainModel.getSaveAllLiveStock(STOID, Rate,CatID,DesignID,ColourID,SizeID,Qty,Company_ID,position,0);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getOldAddress(int custid, boolean isConnetced) {
        if (isConnetced) {
            mainModel.getOldAddress(custid);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getOldRate(int custid, boolean isConnetced) {
        if (isConnetced) {
            mainModel.getOldRate(custid);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getSaveOldStock(String purchasedate, int custid, int addressid, boolean isConnected) {
        if (isConnected) {
            mainModel.getSaveOldStock(purchasedate,custid,addressid);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getSaveAlloldStock(int STOID, double Rate, int CatID, int DesignID, int ColourID, int SizeID, double weight, int Qty, int Company_ID,int olddays, boolean isConnected, int position) {
        if (isConnected) {
            mainModel.getSaveAllOldStock(STOID, Rate,CatID,DesignID,ColourID,SizeID,weight,Qty,Company_ID,olddays,position,0);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getPendingOrder(int custid, int companyid, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getpendingOrder(custid,companyid,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getOldPendingOrder(int custid, int companyid, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getoldpendingOrder(custid,companyid,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getApprovedOrder(int custid, int companyid, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getApprovedOrder(custid,companyid,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getLabourChargeOtp(boolean isConnected, String id) {
        if (isConnected) {
            mainModel.getLabourCharge(id);
        } else {
            //baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getPendingOrderDetails(boolean isConnected, int id) {
        if (isConnected) {
            mainModel.getPendingOrderDetails(id);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getOldPendingOrderDetails(boolean isConnected, int id) {
        if (isConnected) {
            mainModel.getOldPendingOrderDetails(id);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getApprovedOrderDetails(boolean isConnected, int id) {
        if (isConnected) {
            mainModel.getApprovedOrderDetails(id);
        } else {
            baseView.showToast(R.string.no_internet);
            baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getChangePassword(int id, String offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getChangePassword(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getSearchLiveStock(int offset, String str, boolean isConnected) {
        if (isConnected) {
            mainModel.getSearchLiveStockResponse(offset,str);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getDeleteOrder(boolean isConnected, int id) {
        if (isConnected) {
            mainModel.getDeleteOrder(id);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getMyCustomer(int id, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getMyCustomerResponse(id,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            //baseView.switchVisibility(4,"");
        }
    }

    @Override
    public void getPendingOrdersaleman(int custid, int companyid, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getpendingOrdersaleman(custid,companyid,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getOldPendingOrdersaleman(int custid, int companyid, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getoldpendingOrdersaleman(custid,companyid,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getApprovedOrdersaleman(int custid, int companyid, int offset, boolean isConnected) {
        if (isConnected) {
            mainModel.getApprovedOrdersaleman(custid,companyid,offset);
        } else {
            baseView.showToast(R.string.no_internet);
            if(offset==0) {
                baseView.switchVisibility(4, "");
            }
        }
    }

    @Override
    public void getCustomerList(int custid, int companyid, boolean isConnected) {
        if (isConnected) {
            mainModel.getCustomerList(custid,companyid);
        } else {
            baseView.showToast(R.string.no_internet);
        }
    }
    @Override
    public void getCustomerListold(int custid, int companyid, boolean isConnected) {
        if (isConnected) {
            mainModel.getCustomerListold(custid,companyid);
        } else {
            baseView.showToast(R.string.no_internet);
        }
    }
}