package com.tiara.mvp.Model;

import com.tiara.api.NetworkService;
import com.tiara.dependancy.DaggerDependancyInjection;
import com.tiara.ui.base.AppController;
import com.tiara.ui.model.Response.CommonErrorResponse;
import com.tiara.utils.PreferenceUtils;


import java.lang.annotation.Annotation;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;

import static com.tiara.utils.Constants.BEARER;


/**
 * Created by swapnali on 31/05/17.
 */

public class MainBaseModel {

    @Inject
    NetworkService networkService;

    @Inject
    public PreferenceUtils mPreferenceUtils;

    @Inject
    Retrofit retrofit;

    // instantiation
    NetworkService getServiceConnection() {
        if (networkService == null) {
            DaggerDependancyInjection.builder()
                    .baseServiceComponent(AppController.getBaseServiceComponent())
                    .build()
                    .inject(this);
        }
        return networkService;
    }

    Retrofit getRetrofit() {
        if (retrofit == null) {
            DaggerDependancyInjection.builder()
                    .baseServiceComponent(AppController.getBaseServiceComponent())
                    .build()
                    .inject(this);
        }
        return retrofit;
    }

    // instantiation
    protected PreferenceUtils getPreferenceInstance() {
        if (mPreferenceUtils == null) {
            DaggerDependancyInjection.builder()
                    .baseServiceComponent(AppController.getBaseServiceComponent())
                    .build()
                    .inject(this);
        }
        return mPreferenceUtils;
    }

//    public void commonErrorHandling(Throwable e, String apiName) {
//        AppUtils.logMe(apiName+ " RESPONSE", e.toString());
//
//        if (e instanceof HttpException) {
//            ResponseBody body = ((HttpException) e).response().errorBody();
//            Converters<ResponseBody, CommonErrorResponse> errorConverter =
//                    getRetrofit().responseBodyConverter(CommonErrorResponse.class, new Annotation[0]);
//
//            try {
//                CommonErrorResponse error = errorConverter.convert(body);
//                presenter.onError(error, ApiNames.USER_LOGIN);
//            } catch (IOException exp) {
//                e.printStackTrace();
//            }
//
//        } else {
//            presenter.onResponse(e.toString(), false, ApiNames.USER_LOGIN);
//        }
//    }

    protected String getToken() {
        //return BEARER + getPreferenceInstance().getValue(Constants.USER_TOKEN);
        return "";
    }

    protected CommonErrorResponse getError(Throwable e) {
        CommonErrorResponse error = null;
        try {
            ResponseBody body = ((HttpException) e).response().errorBody();
            Converter<ResponseBody, CommonErrorResponse> errorConverter =
                    getRetrofit().responseBodyConverter(CommonErrorResponse.class, new Annotation[0]);

            error = errorConverter.convert(body);
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return error;
    }
}
