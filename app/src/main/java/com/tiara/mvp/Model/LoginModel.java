package com.tiara.mvp.Model;

import com.google.gson.Gson;
import com.tiara.api.ApiNames;
import com.tiara.mvp.Presenter.LoginPresenter;
import com.tiara.ui.model.Response.CustomerLoginRes;
import com.tiara.ui.model.Response.SalesManLoginRes;
import com.tiara.utils.AppUtils;

import java.util.ArrayList;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by itrs-203 on 12/22/17.
 */

public class LoginModel extends MainBaseModel {

    private LoginPresenter presenter;

    public LoginModel(LoginPresenter presenter) {
        this.presenter = presenter;
    }
    public void getSalesManLoginResponse(String username,String password) {
        AppUtils.logMe(ApiNames.SALES_LOGIN + " REQUEST", "");
        getServiceConnection().getSalesmanLogin(username,password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SalesManLoginRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.SALES_LOGIN + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.SALES_LOGIN);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.SALES_LOGIN);
                        }
                    }

                    @Override
                    public void onNext(SalesManLoginRes response) {
                        AppUtils.logMe(ApiNames.SALES_LOGIN + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.SALES_LOGIN);
                    }
                });
    }

    public void getCustomerLoginResponse(String username,String password) {
        AppUtils.logMe(ApiNames.CUSTOMER_LOGIN + " REQUEST", "");
        getServiceConnection().getCustomerLogin(username,password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CustomerLoginRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.CUSTOMER_LOGIN + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.CUSTOMER_LOGIN);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.CUSTOMER_LOGIN);
                        }
                    }

                    @Override
                    public void onNext(CustomerLoginRes customerLoginRes) {
                        AppUtils.logMe(ApiNames.CUSTOMER_LOGIN + " RESPONSE", new Gson().toJson(customerLoginRes));
                        presenter.onResponse(customerLoginRes, true, ApiNames.CUSTOMER_LOGIN);
                    }
                });
    }


}
