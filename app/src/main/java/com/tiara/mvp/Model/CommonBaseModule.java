package com.tiara.mvp.Model;

import com.google.gson.Gson;
import com.tiara.api.ApiNames;
import com.tiara.mvp.Presenter.CommonPresenter;
import com.tiara.ui.model.Response.AddressResponse;
import com.tiara.ui.model.Response.ApprovedOrderDetails;
import com.tiara.ui.model.Response.ApprovedOrderResponse;
import com.tiara.ui.model.Response.ColorResponse;
import com.tiara.ui.model.Response.CommonErrorResponse;
import com.tiara.ui.model.Response.CustomerListResponse;
import com.tiara.ui.model.Response.FILLGSTRes;
import com.tiara.ui.model.Response.LabourChargeOTPResponse;
import com.tiara.ui.model.Response.LabourChargeResponse;
import com.tiara.ui.model.Response.LiveStockDesignRes;
import com.tiara.ui.model.Response.LiveStockOrderResponse;
import com.tiara.ui.model.Response.LiveStockRes;
import com.tiara.ui.model.Response.LiveStockSaveAllResponse;
import com.tiara.ui.model.Response.LiveStockSaveResponse;
import com.tiara.ui.model.Response.MyCustomerResponse;
import com.tiara.ui.model.Response.OldPendingOrderDetails;
import com.tiara.ui.model.Response.OldPendingOrderResponse;
import com.tiara.ui.model.Response.OldStockOrderResponse;
import com.tiara.ui.model.Response.PendingOrderDetails;
import com.tiara.ui.model.Response.PendingOrderResponse;
import com.tiara.ui.model.Response.ProfileResponse;
import com.tiara.ui.model.Response.RateResponse;
import com.tiara.ui.model.Response.SizeResponse;
import com.tiara.ui.model.Response.ViewMoreDesignResponse;
import com.tiara.utils.AppUtils;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CommonBaseModule  extends MainBaseModel
{
    CommonPresenter presenter;
public CommonBaseModule(CommonPresenter presenter)
{
    this.presenter=presenter;
}

    public void getLiveStockResponse(int id,int offset) {
        AppUtils.logMe(ApiNames.LIVE_STOCK + " REQUEST", "");
        getServiceConnection().getLiveStock(offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVE_STOCK);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVE_STOCK);
                        }
                    }

                    @Override
                    public void onNext(LiveStockRes response) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVE_STOCK);
                    }
                });
    }


    public void getCatelogResponse(int id,int offset) {
        AppUtils.logMe(ApiNames.OLD_STOCK + " REQUEST", "");
        getServiceConnection().getoldStock(offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLD_STOCK + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLD_STOCK);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLD_STOCK);
                        }
                    }

                    @Override
                    public void onNext(LiveStockRes response) {
                        AppUtils.logMe(ApiNames.OLD_STOCK + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLD_STOCK);
                    }
                });
    }

    public void getProfileDetails(int id) {
        AppUtils.logMe(ApiNames.PROFILE + " REQUEST", "");
        getServiceConnection().getProfileDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProfileResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.PROFILE + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.PROFILE);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.PROFILE);
                        }
                    }

                    @Override
                    public void onNext(ProfileResponse response) {
                        AppUtils.logMe(ApiNames.PROFILE + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.PROFILE);
                    }
                });
    }

    public void getLabourChargeResponse(int id,int offset) {
        AppUtils.logMe(ApiNames.LABOUR_CHARGE + " REQUEST", "");
        getServiceConnection().getLabourCharge(id,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LabourChargeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LABOUR_CHARGE + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LABOUR_CHARGE);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LABOUR_CHARGE);
                        }
                    }

                    @Override
                    public void onNext(LabourChargeResponse response) {
                        AppUtils.logMe(ApiNames.LABOUR_CHARGE + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LABOUR_CHARGE);
                    }
                });
    }


    public void getGstState() {
        AppUtils.logMe(ApiNames.FILLGST + " REQUEST", "");
        getServiceConnection().getGst()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<FILLGSTRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.FILLGST + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.FILLGST);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.FILLGST);
                        }
                    }

                    @Override
                    public void onNext(FILLGSTRes response) {
                        AppUtils.logMe(ApiNames.FILLGST + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.FILLGST);
                    }
                });
    }


    public void updateProfile(ProfileResponse.Datum profileresponse) {
        AppUtils.logMe(ApiNames.PROFILE_UPDATE + " REQUEST", "");
        getServiceConnection().updateProfile(profileresponse.getCustName(),profileresponse.getCustEmail(),profileresponse.getCustContactNo(),profileresponse.getCustMobile(),profileresponse.getCustVatTin(),profileresponse.getCustCstTin(),profileresponse.getCustSalesPID(),profileresponse.getCustPassword(),profileresponse.getCustLogo(),profileresponse.getCustGSTNo(),profileresponse.getCustStateId(),profileresponse.getIsOTP(),profileresponse.getCustApprovEmail())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CommonErrorResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.PROFILE_UPDATE + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.PROFILE_UPDATE);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.PROFILE_UPDATE);
                        }
                    }

                    @Override
                    public void onNext(CommonErrorResponse response) {
                        AppUtils.logMe(ApiNames.PROFILE_UPDATE + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.PROFILE_UPDATE);
                    }
                });
    }


    public void getLiveStockDesign(int id,int offset) {
        AppUtils.logMe(ApiNames.LIVE_STOCK_DESIGN + " REQUEST", "");
        getServiceConnection().getLiveStockDesign(id,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockDesignRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK_DESIGN + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVE_STOCK_DESIGN);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVE_STOCK_DESIGN);
                        }
                    }

                    @Override
                    public void onNext(LiveStockDesignRes response) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK_DESIGN + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVE_STOCK_DESIGN);
                    }
                });

    }


    public void getpendingOrder(int custid,int companyid,int offset) {
        AppUtils.logMe(ApiNames.PENDING_ORDER + " REQUEST", "");
        getServiceConnection().getpendingorder(custid,companyid,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PendingOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.PENDING_ORDER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.PENDING_ORDER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.PENDING_ORDER);
                        }
                    }

                    @Override
                    public void onNext(PendingOrderResponse response) {
                        AppUtils.logMe(ApiNames.PENDING_ORDER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.PENDING_ORDER);
                    }
                });
    }


    public void getoldpendingOrder(int custid,int companyid,int offset) {
        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER + " REQUEST", "");
        getServiceConnection().getoldpendingorder(custid,companyid,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OldPendingOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLD_PENDING_ORDER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLD_PENDING_ORDER);
                        }
                    }

                    @Override
                    public void onNext(OldPendingOrderResponse response) {
                        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLD_PENDING_ORDER);
                    }
                });
    }

    public void getApprovedOrder(int custid,int companyid,int offset) {
        AppUtils.logMe(ApiNames.APPROVED_ORDER + " REQUEST", "");
        getServiceConnection().getapprovedorder(custid,companyid,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ApprovedOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.APPROVED_ORDER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.APPROVED_ORDER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.APPROVED_ORDER);
                        }
                    }

                    @Override
                    public void onNext(ApprovedOrderResponse response) {
                        AppUtils.logMe(ApiNames.APPROVED_ORDER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.APPROVED_ORDER);
                    }
                });
    }

    public void getColor() {
        AppUtils.logMe(ApiNames.GET_COLOR_DETAILS + " REQUEST", "");
        getServiceConnection().getColor()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ColorResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.GET_COLOR_DETAILS + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.GET_COLOR_DETAILS);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.GET_COLOR_DETAILS);
                        }
                    }

                    @Override
                    public void onNext(ColorResponse response) {
                        AppUtils.logMe(ApiNames.GET_COLOR_DETAILS + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.GET_COLOR_DETAILS);
                    }
                });
    }

    public void getSize() {
        AppUtils.logMe(ApiNames.GET_SIZE_DETAILS + " REQUEST", "");
        getServiceConnection().getSize()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SizeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.GET_SIZE_DETAILS + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.GET_SIZE_DETAILS);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.GET_SIZE_DETAILS);
                        }
                    }

                    @Override
                    public void onNext(SizeResponse response) {
                        AppUtils.logMe(ApiNames.GET_SIZE_DETAILS + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.GET_SIZE_DETAILS);
                    }
                });
    }

    public void getViewMoreDesign(int designcateid,int designid) {
        AppUtils.logMe(ApiNames.GET_MORE_DETAILS + " REQUEST", "");
        getServiceConnection().getViewMoreDesign(designcateid,designid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ViewMoreDesignResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.GET_MORE_DETAILS + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.GET_MORE_DETAILS);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.GET_MORE_DETAILS);
                        }
                    }

                    @Override
                    public void onNext(ViewMoreDesignResponse response) {
                        AppUtils.logMe(ApiNames.GET_MORE_DETAILS + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.GET_MORE_DETAILS);
                    }
                });

    }

    public void getViewMoreDesignCatalog(int designcateid,int designid) {
        AppUtils.logMe(ApiNames.GET_MORE_DETAILS_CATALOG + " REQUEST", "");
        getServiceConnection().getCatalogViewMoreDesign(designcateid,designid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ViewMoreDesignResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.GET_MORE_DETAILS_CATALOG + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.GET_MORE_DETAILS_CATALOG);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.GET_MORE_DETAILS_CATALOG);
                        }
                    }

                    @Override
                    public void onNext(ViewMoreDesignResponse response) {
                        AppUtils.logMe(ApiNames.GET_MORE_DETAILS_CATALOG + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.GET_MORE_DETAILS_CATALOG);
                    }
                });

    }


    public void getCatalogkDesign(int id,int offset) {
        AppUtils.logMe(ApiNames.CATALOG_DESIGN + " REQUEST", "");
        getServiceConnection().getCatalogDesign(id/*,offset*/)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockDesignRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.CATALOG_DESIGN + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.CATALOG_DESIGN);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.CATALOG_DESIGN);
                        }
                    }

                    @Override
                    public void onNext(LiveStockDesignRes response) {
                        AppUtils.logMe(ApiNames.CATALOG_DESIGN + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.CATALOG_DESIGN);
                    }
                });

    }

    public void getViewMoreDesignBySearch(String colorvalue,String sizevalue,int designcateid,int designid) {
        AppUtils.logMe(ApiNames.GET_SEARCH_MORE_DETAILS + " REQUEST", "");
        getServiceConnection().getLiveStockDesignBySearch(colorvalue,sizevalue,designcateid,designid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockDesignRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.GET_SEARCH_MORE_DETAILS + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.GET_SEARCH_MORE_DETAILS);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.GET_SEARCH_MORE_DETAILS);
                        }
                    }

                    @Override
                    public void onNext(LiveStockDesignRes response) {
                        AppUtils.logMe(ApiNames.GET_SEARCH_MORE_DETAILS + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.GET_SEARCH_MORE_DETAILS);
                    }
                });

    }

    public void getLiveStockOrder(int designcateid,int designid) {
        AppUtils.logMe(ApiNames.LIVE_STOCK_ORDER + " REQUEST", "");
        getServiceConnection().getLiveStockOrder(designcateid,designid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK_ORDER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVE_STOCK_ORDER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVE_STOCK_ORDER);
                        }
                    }

                    @Override
                    public void onNext(LiveStockOrderResponse response) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK_ORDER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVE_STOCK_ORDER);
                    }
                });

    }

    public void getOldStockOrder(int designcateid,int designid) {
        AppUtils.logMe(ApiNames.OLD_STOCK_ORDER + " REQUEST", "");
        getServiceConnection().getOldStockOrder(designcateid,designid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OldStockOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLD_STOCK_ORDER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLD_STOCK_ORDER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLD_STOCK_ORDER);
                        }
                    }

                    @Override
                    public void onNext(OldStockOrderResponse response) {
                        AppUtils.logMe(ApiNames.OLD_STOCK_ORDER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLD_STOCK_ORDER);
                    }
                });

    }


    public void getAddress(int custid) {
        AppUtils.logMe(ApiNames.LIVESTOCK_FILLADDRES + " REQUEST", "");
        getServiceConnection().getAddress(custid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AddressResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_FILLADDRES + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVESTOCK_FILLADDRES);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVESTOCK_FILLADDRES);
                        }
                    }

                    @Override
                    public void onNext(AddressResponse response) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_FILLADDRES + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVESTOCK_FILLADDRES);
                    }
                });

    }

    public void getRate(int custid) {
        AppUtils.logMe(ApiNames.LIVESTOCK_FILLRATE + " REQUEST", "");
        getServiceConnection().getRate(custid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RateResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_FILLRATE + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVESTOCK_FILLRATE);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVESTOCK_FILLRATE);
                        }
                    }

                    @Override
                    public void onNext(RateResponse response) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_FILLRATE + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVESTOCK_FILLRATE);
                    }
                });

    }

    public void getSaveLiveStock(String date,int custid,int addressid) {
        AppUtils.logMe(ApiNames.LIVESTOCK_SAVE + " REQUEST", "");
        getServiceConnection().getSaveLiveStock(date,custid,addressid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockSaveResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_SAVE + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVESTOCK_SAVE);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVESTOCK_SAVE);
                        }
                    }

                    @Override
                    public void onNext(LiveStockSaveResponse response) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_SAVE + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVESTOCK_SAVE);
                    }
                });

    }

    public void getSaveAllLiveStock(int STOID, double Rate, int CatID, int DesignID, int ColourID, int SizeID, int Qty, int Company_ID, final int position, final int designid) {
        AppUtils.logMe(ApiNames.LIVESTOCK_SAVEALL + " REQUEST", "");
        getServiceConnection().getSaveLiveStockAll( STOID,  Rate,  CatID,  DesignID,  ColourID,  SizeID,  Qty,  Company_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockSaveAllResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_SAVEALL + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVESTOCK_SAVEALL);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVESTOCK_SAVEALL);
                        }
                    }

                    @Override
                    public void onNext(LiveStockSaveAllResponse response) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_SAVEALL + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVESTOCK_SAVEALL,position,"");
                    }
                });

    }



    public void getOldAddress(int custid) {
        AppUtils.logMe(ApiNames.OLDSTOCK_FILLADDRES + " REQUEST", "");
        getServiceConnection().getOldAddress(custid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AddressResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_FILLADDRES + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLDSTOCK_FILLADDRES);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLDSTOCK_FILLADDRES);
                        }
                    }

                    @Override
                    public void onNext(AddressResponse response) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_FILLADDRES + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLDSTOCK_FILLADDRES);
                    }
                });

    }

    public void getOldRate(int custid) {
        AppUtils.logMe(ApiNames.OLDSTOCK_FILLRATE + " REQUEST", "");
        getServiceConnection().getOldRate(custid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RateResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_FILLRATE + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLDSTOCK_FILLRATE);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLDSTOCK_FILLRATE);
                        }
                    }

                    @Override
                    public void onNext(RateResponse response) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_FILLRATE + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLDSTOCK_FILLRATE);
                    }
                });

    }

    public void getSaveOldStock(String date,int custid,int addressid) {
        AppUtils.logMe(ApiNames.OLDSTOCK_SAVE + " REQUEST", "");
        getServiceConnection().getSaveOldStock(date,custid,addressid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockSaveResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_SAVE + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLDSTOCK_SAVE);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLDSTOCK_SAVE);
                        }
                    }

                    @Override
                    public void onNext(LiveStockSaveResponse response) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_SAVE + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLDSTOCK_SAVE);
                    }
                });

    }

    public void getSaveAllOldStock(int STOID, double Rate, int CatID, int DesignID, int ColourID, int SizeID, double weight,int Qty, int Company_ID, int olddays,final int position, final int designid) {
        AppUtils.logMe(ApiNames.OLDSTOCK_SAVEALL + " REQUEST", "");
        getServiceConnection().getSaveOldStockAll( STOID,  Rate,  CatID,  DesignID,  ColourID,  SizeID,weight,  Qty,  Company_ID,olddays)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockSaveAllResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_SAVEALL + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLDSTOCK_SAVEALL);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLDSTOCK_SAVEALL);
                        }
                    }

                    @Override
                    public void onNext(LiveStockSaveAllResponse response) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_SAVEALL + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLDSTOCK_SAVEALL,position,"");
                    }
                });

    }

    public void getLabourCharge(String id) {
        AppUtils.logMe(ApiNames.LABOURCHARGE_OTP + " REQUEST", "");
        getServiceConnection().getLabourChargeOtp(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LabourChargeOTPResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LABOURCHARGE_OTP + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LABOURCHARGE_OTP);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LABOURCHARGE_OTP);
                        }
                    }

                    @Override
                    public void onNext(LabourChargeOTPResponse response) {
                        AppUtils.logMe(ApiNames.LABOURCHARGE_OTP + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LABOURCHARGE_OTP);
                    }
                });
    }


    public void getPendingOrderDetails(int id) {
        AppUtils.logMe(ApiNames.PENDING_ORDER_DETAILS + " REQUEST", "");
        getServiceConnection().getpendingorderdetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PendingOrderDetails>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.PENDING_ORDER_DETAILS + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.PENDING_ORDER_DETAILS);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.PENDING_ORDER_DETAILS);
                        }
                    }

                    @Override
                    public void onNext(PendingOrderDetails response) {
                        AppUtils.logMe(ApiNames.PENDING_ORDER_DETAILS + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.PENDING_ORDER_DETAILS);
                    }
                });
    }
    public void getOldPendingOrderDetails(int id) {
        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER_DETAILS + " REQUEST", "");
        getServiceConnection().getoldpendingorderdetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OldPendingOrderDetails>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER_DETAILS + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLD_PENDING_ORDER_DETAILS);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLD_PENDING_ORDER_DETAILS);
                        }
                    }

                    @Override
                    public void onNext(OldPendingOrderDetails response) {
                        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER_DETAILS + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLD_PENDING_ORDER_DETAILS);
                    }
                });
    }
    public void getApprovedOrderDetails(int id) {
        AppUtils.logMe(ApiNames.APPROVED_ORDER_DETAILS + " REQUEST", "");
        getServiceConnection().getapprovedorderdetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ApprovedOrderDetails>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.APPROVED_ORDER_DETAILS + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.APPROVED_ORDER_DETAILS);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.APPROVED_ORDER_DETAILS);
                        }
                    }

                    @Override
                    public void onNext(ApprovedOrderDetails response) {
                        AppUtils.logMe(ApiNames.APPROVED_ORDER_DETAILS + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.APPROVED_ORDER_DETAILS);
                    }
                });
    }
    public void getChangePassword(int id,String offset) {
        AppUtils.logMe(ApiNames.CHANGE_PASSWORD + " REQUEST", "");
        getServiceConnection().getChangePassword(id,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CommonErrorResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.CHANGE_PASSWORD + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.CHANGE_PASSWORD);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.CHANGE_PASSWORD);
                        }
                    }

                    @Override
                    public void onNext(CommonErrorResponse response) {
                        AppUtils.logMe(ApiNames.CHANGE_PASSWORD + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.CHANGE_PASSWORD);
                    }
                });
    }

    public void getSearchLiveStockResponse(int offset,final String str) {
        AppUtils.logMe(ApiNames.LIVE_STOCK_SEARCH + " REQUEST", "");
        getServiceConnection().getSearchLiveStock(offset,str)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LiveStockRes>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK_SEARCH + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVE_STOCK_SEARCH);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVE_STOCK_SEARCH);
                        }
                    }

                    @Override
                    public void onNext(LiveStockRes response) {
                        AppUtils.logMe(ApiNames.LIVE_STOCK_SEARCH + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVE_STOCK_SEARCH,str);
                    }
                });
    }
    public void getDeleteOrder(int id) {
        AppUtils.logMe(ApiNames.DELETE_ORDER + " REQUEST", "");
        getServiceConnection().getDeleteOrder(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CommonErrorResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.DELETE_ORDER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.DELETE_ORDER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.DELETE_ORDER);
                        }
                    }

                    @Override
                    public void onNext(CommonErrorResponse response) {
                        AppUtils.logMe(ApiNames.DELETE_ORDER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.DELETE_ORDER);
                    }
                });
    }
    public void getMyCustomerResponse(int id,int offset) {
        AppUtils.logMe(ApiNames.MY_CUSTOMER + " REQUEST", "");
        getServiceConnection().getmycustomer(id,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MyCustomerResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.MY_CUSTOMER + " RESPONSE", e.toString());
                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.MY_CUSTOMER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.MY_CUSTOMER);
                        }
                    }

                    @Override
                    public void onNext(MyCustomerResponse response) {
                        AppUtils.logMe(ApiNames.MY_CUSTOMER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.MY_CUSTOMER);
                    }
                });
    }

    public void getpendingOrdersaleman(int custid,int companyid,int offset) {
        AppUtils.logMe(ApiNames.PENDING_ORDER_SALESMAN + " REQUEST", "");
        getServiceConnection().getpendingordersalesman(custid,companyid,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PendingOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.PENDING_ORDER_SALESMAN + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.PENDING_ORDER_SALESMAN);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.PENDING_ORDER_SALESMAN);
                        }
                    }

                    @Override
                    public void onNext(PendingOrderResponse response) {
                        AppUtils.logMe(ApiNames.PENDING_ORDER_SALESMAN + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.PENDING_ORDER_SALESMAN);
                    }
                });
    }


    public void getoldpendingOrdersaleman(int custid,int companyid,int offset) {
        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER_SALESMAN + " REQUEST", "");
        getServiceConnection().getoldpendingordersalesman(custid,companyid,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OldPendingOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER_SALESMAN + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLD_PENDING_ORDER_SALESMAN);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLD_PENDING_ORDER_SALESMAN);
                        }
                    }

                    @Override
                    public void onNext(OldPendingOrderResponse response) {
                        AppUtils.logMe(ApiNames.OLD_PENDING_ORDER_SALESMAN + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLD_PENDING_ORDER_SALESMAN);
                    }
                });
    }

    public void getApprovedOrdersaleman(int custid,int companyid,int offset) {
        AppUtils.logMe(ApiNames.APPROVED_ORDER_SALESMAN + " REQUEST", "");
        getServiceConnection().getapprovedordersalesman(custid,companyid,offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ApprovedOrderResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.APPROVED_ORDER_SALESMAN + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.APPROVED_ORDER_SALESMAN);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.APPROVED_ORDER_SALESMAN);
                        }
                    }

                    @Override
                    public void onNext(ApprovedOrderResponse response) {
                        AppUtils.logMe(ApiNames.APPROVED_ORDER_SALESMAN + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.APPROVED_ORDER_SALESMAN);
                    }
                });
    }
    public void getCustomerList(int custid,int companyid) {
        AppUtils.logMe(ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER + " REQUEST", "");
        getServiceConnection().getcustomerList(custid,companyid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CustomerListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER);
                        }
                    }

                    @Override
                    public void onNext(CustomerListResponse response) {
                        AppUtils.logMe(ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.LIVESTOCK_FILLSALESWISECUSTOMER);
                    }
                });

    }

    public void getCustomerListold(int custid,int companyid) {
        AppUtils.logMe(ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER + " REQUEST", "");
        getServiceConnection().getcustomerListold(custid,companyid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CustomerListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER + " RESPONSE", e.toString());

                        if (e instanceof HttpException) {
                            presenter.onError(getError(e), ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER);
                        } else {
                            presenter.onResponse(e.toString(), false, ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER);
                        }
                    }

                    @Override
                    public void onNext(CustomerListResponse response) {
                        AppUtils.logMe(ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER + " RESPONSE", new Gson().toJson(response));
                        presenter.onResponse(response, true, ApiNames.OLDSTOCK_FILLSALESWISECUSTOMER);
                    }
                });

    }

}