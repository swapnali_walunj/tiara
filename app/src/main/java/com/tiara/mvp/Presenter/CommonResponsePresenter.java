package com.tiara.mvp.Presenter;

public interface CommonResponsePresenter {

    void onResponse(Object responseJson, boolean isSuccess, String apiName);
    void onResponse(Object responseJson, boolean isSuccess, String apiName, String searchKey);
    void onResponse(Object responseJson, boolean isSuccess, String apiName, int position, String grpid);
}
