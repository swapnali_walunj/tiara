package com.tiara.mvp.Presenter;

import android.widget.EditText;

/**
 * Created by itrs-203 on 6/2/17.
 */

public interface SearchPresenter {

    void getSearchKeywordDetails(EditText edtSearch, String mUserId);
}
