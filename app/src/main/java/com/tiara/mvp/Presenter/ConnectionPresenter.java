package com.tiara.mvp.Presenter;

import com.zplesac.connectionbuddy.presenters.ConnectivityPresenter;

/**
 * Created by user on 24/3/17.
 */

public interface ConnectionPresenter extends ConnectivityPresenter {

    void init(boolean hasSavedInstanceState);
}