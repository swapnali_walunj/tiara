package com.tiara.mvp.Presenter;

import com.tiara.ui.model.Response.ProfileResponse;

public interface CommonPresenter extends CommonResponsePresenter {

    void onResponse(Object responseJson, boolean isSuccess, String apiName);

    void onError(Object responseJson, String apiName);

    void getLiveStock(int id, int offset,boolean isConnected);
    void getGST(boolean isConnected);

    void getProfileDetails(boolean isConnected,int id);

    void getLabourCharge(int id, int offset, boolean isConnected);

    void getCatelog(int id, int offset, boolean isConnected);

    void updateProfile(boolean isConnected,ProfileResponse.Datum profile);


    void getLiveStockDesign(int id, int offset,boolean isConnected);


    void getColorstock(boolean isConnected);
    void getSize(boolean isConnected);
    void getCatalogDesign(int id, int offset,boolean isConnected);
    void getViewMoreDesignCatalog(int id, int offset, boolean isConnected);
    void getViewMoreDesign(int id, int offset, boolean isConnected);
    void getViewMoreDesignBysearch(String colorvalue,String sizevalue,int id, int offset, boolean isConnected);


    void getLiveStockOrder(int id, int offset, boolean isConnected);
    void getOldStockOrder(int id, int offset, boolean isConnected);

    void getAddress(int custid,boolean isConnetced);
    void getRate(int custid,boolean isConnetced);

    void getSaveLiveStock(String purchasedate,int custid,int addressid, boolean isConnected);
     void getSaveAllLiveStock(int STOID, double Rate, int CatID, int DesignID, int ColourID, int SizeID, int Qty, int Company_ID, boolean isConnected,int position);


    void getOldAddress(int custid,boolean isConnetced);
    void getOldRate(int custid,boolean isConnetced);

    void getSaveOldStock(String purchasedate,int custid,int addressid, boolean isConnected);
    void getSaveAlloldStock(int STOID, double Rate, int CatID, int DesignID, int ColourID, int SizeID, double weight, int Qty, int Company_ID,int olddays, boolean isConnected, int position);



    void getPendingOrder(int custid,int companyid,int offset,boolean isConnected);
    void getOldPendingOrder(int custid,int companyid,int offset,boolean isConnected);
    void getApprovedOrder(int custid,int companyid,int offset,boolean isConnected);

    void getLabourChargeOtp(boolean isConnected,String id);
    void getPendingOrderDetails(boolean isConnected,int id);
    void getOldPendingOrderDetails(boolean isConnected,int id);
    void getApprovedOrderDetails(boolean isConnected,int id);

    void getChangePassword(int id, String offset, boolean isConnected);


    void getSearchLiveStock(int offset, String str, boolean isConnected);

    void getDeleteOrder(boolean isConnected,int id);

    void getMyCustomer(int id, int offset, boolean isConnected);


    void getPendingOrdersaleman(int custid,int companyid,int offset,boolean isConnected);
    void getOldPendingOrdersaleman(int custid,int companyid,int offset,boolean isConnected);
    void getApprovedOrdersaleman(int custid,int companyid,int offset,boolean isConnected);
    void getCustomerList(int custid,int companyid,boolean isConnected);
    void getCustomerListold(int custid,int companyid,boolean isConnected);
}
