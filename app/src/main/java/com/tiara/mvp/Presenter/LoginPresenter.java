package com.tiara.mvp.Presenter;

/**
 * Created by itrs-203 on 12/22/17.
 */

public interface LoginPresenter extends CommonResponsePresenter{

    void onResponse(Object responseJson, boolean isSuccess, String apiName);

    void onError(Object responseJson, String apiName);
    void getLoginSalesMan(boolean isConnected, String username, String password);
    void getLoginCustomer(boolean isConnected, String username, String password);
    boolean validateRegistration( String mobile, String password);

}
