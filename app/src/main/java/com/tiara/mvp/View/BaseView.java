package com.tiara.mvp.View;

/**
 * Created by user on 13/2/17.
 */
public interface BaseView {
    void showProgress();

    void hideProgress();

    void showToast(String Message);

    void showToast(int id);

    void showProgressDialog();

    void showCancelableProgressDialog();

    void hideProgressDialog();

    void onResponseSuccess(Object data, String apiName, String searchKey);
    void onResponseSuccess(Object data, String apiName);
    void onResponseSuccess(Object data, String apiName, int position, String grppostion);
    void onResponseFailure(String msg, String apiName, int position, String grppostion);

    void onResponseFailure(String msg);

    void onErrorResponse(Object data, String apiName);

    void switchVisibility(int type);

    void switchVisibility(int type, String msg);

}
