package com.tiara.mvp.View;

import com.zplesac.connectionbuddy.models.ConnectivityEvent;

/**
 * Created by Yuvraj on 24/3/17.
 */

public interface ConnectionView {

    void initViews(boolean isConnected);

    void onConnectionChangeEvent(ConnectivityEvent event);
}

