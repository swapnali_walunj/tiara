package com.tiara.mvp.View;

import android.support.v7.widget.Toolbar;

import java.util.List;

/**
 * Created by Yuvraj on 31/05/17.
 */

public interface SearchProcessView {

    void onSearchStarted(String search);
    void onSearchCompleted(List<Object> listCategories);
    void onMenuItemActionCollapse(Toolbar searchToolbar, Toolbar mainToolbar);

}
