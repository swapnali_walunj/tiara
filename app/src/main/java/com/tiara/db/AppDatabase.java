/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tiara.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;


import com.tiara.db.dao.CatalogOrderListingDao;
import com.tiara.db.dao.OrderDao;
import com.tiara.db.dao.OrderListingDao;
import com.tiara.db.enitity.OrderProductEntity;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.OldStockOrder;


@Database(entities = {OrderProductEntity.class,
        LiveStockOrder.class,
        OldStockOrder.class
}, version = 3, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "tiara-db";

    private static AppDatabase INSTANCE;


    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }

    public abstract OrderDao wishlistProductDao();
    public abstract OrderListingDao orderListingDao();
    public abstract CatalogOrderListingDao catalogOrderListingDao();


}
