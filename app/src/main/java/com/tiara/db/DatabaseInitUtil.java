/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tiara.db;


import android.os.AsyncTask;

import com.tiara.db.enitity.OrderProductEntity;
import com.tiara.ui.model.Response.LiveStockOrder;
import com.tiara.ui.model.Response.LiveStockOrderResponse;
import com.tiara.ui.model.Response.OldStockOrder;

import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * Generates dummy data and inserts them into the database
 */
public class DatabaseInitUtil {

    private static final String[] FIRST = new String[]{
            "Special edition", "New", "Cheap", "Quality", "Used"};
    private static final String[] SECOND = new String[]{
            "Three-headed Monkey", "Rubber Chicken", "Pint of Grog", "Monocle"};
    private static final String[] DESCRIPTION = new String[]{
            "is finally here", "is recommended by Stan S. Stanman",
            "is the best sold product on Mêlée Island", "is \uD83D\uDCAF", "is ❤️", "is fine"};
    private static final String[] COMMENTS = new String[]{
            "Comment 1", "Comment 2", "Comment 3", "Comment 4", "Comment 5", "Comment 6",
    };

    public static void addWishlistProductToCart(AppDatabase db, final OrderProductEntity wishlistProductEntity) {
        new addWishlistProductAsyncTask(db).execute(wishlistProductEntity);
    }


    public static void deleteAllTables(final AppDatabase db) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                db.clearAllTables();
            }
        }).start();
    }


    public static void deleteWishlistProductFromCart(AppDatabase db, final int product_id) {
        new deleteWishlistProductAsyncTask(db).execute(product_id);
    }

    private static void insertOrderProduct(AppDatabase db, OrderProductEntity wishlistProducts) {
        db.beginTransaction();
        try {
            db.wishlistProductDao().insert(wishlistProducts);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    private static void deleteWishlistItemsFromEntity(AppDatabase db, int product_id) {
        db.beginTransaction();
        try {
            db.wishlistProductDao().deleteProduct(product_id);
            db.endTransaction();
            db.setTransactionSuccessful();
        } finally {
        }
    }
    private static void deleteOrderItemsFromEntity(AppDatabase db, int product_id) {
        db.beginTransaction();
        try {
            db.orderListingDao().deleteProduct(product_id);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }




    private static class addWishlistProductAsyncTask extends AsyncTask<OrderProductEntity, Void, Void> {

        private AppDatabase db;

        addWishlistProductAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final OrderProductEntity... params) {
            insertAWishlistProduct(db, params[0]);
            return null;
        }

    }
    private static void insertAWishlistProduct(AppDatabase db, OrderProductEntity wishlistProducts) {
        db.beginTransaction();
        try {
            db.wishlistProductDao().insert(wishlistProducts);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }





    private static class deleteWishlistProductAsyncTask extends AsyncTask<Integer, Void, Void>{
    private AppDatabase db;

        deleteWishlistProductAsyncTask(AppDatabase appDatabase) {
        db = appDatabase;
    }

    @Override
    protected Void doInBackground(final Integer... params) {
        deleteWishlistItemsFromEntity(db, params[0]);
        return null;
    }

}

    private static class deleteOrderProductAsyncTask extends AsyncTask<Integer, Void, Void> {

        private AppDatabase db;

        deleteOrderProductAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            deleteOrderItemsFromEntity(db, params[0]);
            return null;
        }

    }


    public static void addOrderListAsyncTask(AppDatabase db, final List<LiveStockOrder> orderResponseList, boolean firstTime) {
        new addOrderListAsyncTask(db, firstTime).execute(orderResponseList);
    }

    //SiteListing
    private static class addOrderListAsyncTask extends AsyncTask<List<LiveStockOrder>, Void, Void> {

        private AppDatabase db;
        private boolean firstTime;

        addOrderListAsyncTask(AppDatabase appDatabase, boolean firstTime) {
            db = appDatabase;
            this.firstTime = firstTime;
        }

        @Override
        protected Void doInBackground(List<LiveStockOrder>... arrayLists) {
//            insertSiteList(db, arrayLists[0]);
            db.beginTransaction();
            try {
                if (firstTime) {
                    db.orderListingDao().deleteAllSites();
                }
                db.orderListingDao().insertAll(arrayLists[0]);
                db.setTransactionSuccessful();
            } catch (ConcurrentModificationException e) {
                e.printStackTrace();
            } finally {
                db.endTransaction();
            }
            return null;
        }
    }


    public static void deleteOrderProductFromCart(AppDatabase db, final int product_id) {
        new deleteOrderProductAsyncTask(db).execute(product_id);
    }


    private static class deleteAllOrderListAsyncTask extends AsyncTask<Integer, Void, Void> {

        private AppDatabase db;

        deleteAllOrderListAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            db.beginTransaction();
            try {
                db.orderListingDao().deleteAllSites();
                db.setTransactionSuccessful();
            } catch (ConcurrentModificationException e) {
                e.printStackTrace();
            } finally {
                db.endTransaction();
            }
            return null;
        }

    }
    public static void deleteAllLiveStockOrder(AppDatabase db) {
        new deleteAllOrderListAsyncTask(db).execute();
    }


    //Catalog
    private static class deleteAllCatalogOrderListAsyncTask extends AsyncTask<Integer, Void, Void> {

        private AppDatabase db;

        deleteAllCatalogOrderListAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            db.beginTransaction();
            try {
                db.catalogOrderListingDao().deleteAllSites();
                db.setTransactionSuccessful();
            } catch (ConcurrentModificationException e) {
                e.printStackTrace();
            } finally {
                db.endTransaction();
            }
            return null;
        }

    }
    public static void deleteAllCatalogStockOrder(AppDatabase db) {
        new deleteAllCatalogOrderListAsyncTask(db).execute();
    }


    public static void addCatalogOrderListAsyncTask(AppDatabase db, final List<OldStockOrder> orderResponseList, boolean firstTime) {
        new addCatalogOrderListAsyncTask(db, firstTime).execute(orderResponseList);
    }


    private static class addCatalogOrderListAsyncTask extends AsyncTask<List<OldStockOrder>, Void, Void> {

        private AppDatabase db;
        private boolean firstTime;

        addCatalogOrderListAsyncTask(AppDatabase appDatabase, boolean firstTime) {
            db = appDatabase;
            this.firstTime = firstTime;
        }

        @Override
        protected Void doInBackground(List<OldStockOrder>... arrayLists) {
//            insertSiteList(db, arrayLists[0]);
            db.beginTransaction();
            try {
                if (firstTime) {
                    db.orderListingDao().deleteAllSites();
                }
                db.catalogOrderListingDao().insertAll(arrayLists[0]);
                db.setTransactionSuccessful();
            } catch (ConcurrentModificationException e) {
                e.printStackTrace();
            } finally {
                db.endTransaction();
            }
            return null;
        }
    }

    private static class deleteCatalogOrderProductAsyncTask extends AsyncTask<Integer, Void, Void> {

        private AppDatabase db;

        deleteCatalogOrderProductAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            deleteCatalogOrderItemsFromEntity(db, params[0]);
            return null;
        }

    }
    public static void deleteCatalogOrderItemsFromEntity(AppDatabase db, int product_id) {
        db.beginTransaction();
        try {
            db.catalogOrderListingDao().deleteProduct(product_id);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }


    public static void deleteCatalogOrderProductFromCart(AppDatabase db, final int product_id) {
        new deleteCatalogOrderProductAsyncTask(db).execute(product_id);
    }

}
