package com.tiara.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class Converters {

    @TypeConverter
    public static String fromInteger(ArrayList<Integer> list) {
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Integer>>() {
        }.getType();
        String json = gson.toJson(list, listType);
        return json;
    }



    //Commons >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    @TypeConverter
    public static String fromArr(ArrayList<String> list) {
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        String json = gson.toJson(list, listType);
        return json;
    }

    @TypeConverter
    public static ArrayList<String> toStringArr(String value) {
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }


}
