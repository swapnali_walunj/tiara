/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tiara.db.enitity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.tiara.db.model.OrderProduct;



@Entity(tableName = "order_products")
public class OrderProductEntity implements OrderProduct, Parcelable {
    @PrimaryKey
    @NonNull private String design;
    private String color;
    private String size;
    private double avgweight;
    private int quantity;


    @Override
    public String getDesign() {
        return design;
    }

    public void setDesign(String design) {
        this.design = design;
    }

    @Override
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String getSize() {
        return size;
    }

    @Override
    public double getAvgweight() {
        return  avgweight;

    }


    public void setSize(String size) {
        this.size = size;
    }


    @Override
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static Creator<OrderProductEntity> getCREATOR() {
        return CREATOR;
    }

    public OrderProductEntity(String design, String color, String size, double avgweight, int quantity) {
        this.design = design;
        this.color = color;
        this.size = size;
        this.avgweight = avgweight;
        this.quantity = quantity;

    }







    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.design);
        dest.writeString(this.color);
        dest.writeString(this.size);
        dest.writeDouble(this.avgweight);
        dest.writeInt(this.quantity);

    }

    protected OrderProductEntity(Parcel in) {
        this.design = in.readString();
        this.color = in.readString();
        this.size = in.readString();
        this.avgweight = in.readDouble();
        this.quantity = in.readInt();

    }

    public static final Creator<OrderProductEntity> CREATOR = new Creator<OrderProductEntity>() {
        @Override
        public OrderProductEntity createFromParcel(Parcel source) {
            return new OrderProductEntity(source);
        }

        @Override
        public OrderProductEntity[] newArray(int size) {
            return new OrderProductEntity[size];
        }
    };



}
