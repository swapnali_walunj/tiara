package com.tiara.ui.helper;

/**
 * Created by itrs-203 on 11/14/17.
 */

public class NavDashBoardPagerList {

    private String DeviceType;
    private String DeviceQuantity;
    private int DeviceResImage;

    public NavDashBoardPagerList(String deviceType, String deviceQuantity, int deviceResImage) {
        DeviceType = deviceType;
        DeviceQuantity = deviceQuantity;
        DeviceResImage = deviceResImage;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public String getDeviceQuantity() {
        return DeviceQuantity;
    }

    public int getDeviceResImage() {
        return DeviceResImage;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public void setDeviceQuantity(String deviceQuantity) {
        DeviceQuantity = deviceQuantity;
    }

    public void setDeviceResImage(int deviceResImage) {
        DeviceResImage = deviceResImage;
    }
}
