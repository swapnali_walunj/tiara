package com.tiara.dependancy.module;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    public static final String CACHE_MANAGER = "cacheManager";
    public static final String RESOURCES = "resources";
    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Named("context")
    Context providesApplication() {
        return mApplication;
    }

    @Provides
    @Named(RESOURCES)
    Resources providesResources() {
        return mApplication.getResources();
    }
}
