package com.tiara.dependancy;


import com.tiara.dependancy.component.BaseServiceComponent;
import com.tiara.dependancy.scopes.ActivityScope;
import com.tiara.mvp.Model.MainBaseModel;
import com.tiara.ui.base.AppController;
import com.tiara.ui.base.BaseActivity;
import com.tiara.ui.fragment.BaseFragment;

import dagger.Component;

@ActivityScope
@Component(dependencies = {BaseServiceComponent.class})
public interface DependancyInjection {
    void inject(MainBaseModel baseActivity);
    void inject(BaseActivity baseActivity);
    void inject(AppController appController);
    void inject(BaseFragment baseFragment);
}
