package com.tiara.dependancy.component;

import com.tiara.api.NetworkService;
import com.tiara.dependancy.module.BaseModule;
import com.tiara.utils.PreferenceUtils;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = BaseModule.class)
public interface BaseServiceComponent {

    NetworkService getNetworkService();

    PreferenceUtils getPreferences();

    Retrofit getRetrofit();
}
